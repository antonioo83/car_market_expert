<?php

class LoggedException extends BaseException
{
    /**
     * @param $message
     * @param $model
     */
    public function log($message = '', $model = null)
    {
        if ($message == '') {
            $message = $this->getMessage();
        }

        if (! is_null($model)) {
            $message .= ', model = ' . print_r($model, true) . ' ';
        }

        if (Yii::app()->user->isGuest == false) {
            $message .= ', company_id=' . Yii::app()->user->getCompanyId() . ', user_id=' . Yii::app()->user->id . ' ';
        } else {
            $message .= ', user_id is Guest ';
        }

        Yii::log($message . 'Exception:' . $this->getMessage(), CLogger::LEVEL_ERROR);
    }

}