<?php

class LTransaction
{
    /**
     * Определяет была ли активна транзакция прежде.
     *
     * @var null
     */
    public $isActiveBeforeTransaction = null;

    /**
     * Объект транзакции.
     *
     * @var CDbTransaction
     */
    public $transaction = null;

    /**
     * Запуск транзакции.
     */
    public function begin()
    {
        $transaction = Yii::app()->getDb()->getCurrentTransaction();
        if (! is_null($transaction)) {
            $this->isActiveBeforeTransaction = true;
            $this->transaction =  $transaction;
        } else {
            $this->isActiveBeforeTransaction = false;
            $this->transaction = Yii::app()->getDb()->beginTransaction();
        }
    }

    /**
     * @throws CDbException
     */
    public function commit()
    {
        if ($this->isActiveBeforeTransaction == false) {
            $this->transaction->commit();
        }
    }

    /**
     * @throws CDbException
     */
    public function rollback()
    {
        if ($this->isActiveBeforeTransaction == false) {
            $this->transaction->rollback();
        }
    }

}