<?php

class LLogFilter extends CLogFilter
{
    // =array('category','category.*','some.category.tree.*');
    public $ignoreCategories = array('exception.CHttpException.404');

    /**
     * @param array $logs
     * @return array
     */
    public function filter(&$logs)
    {
        if (empty($logs)) {

            return $logs;
        }

        foreach($logs as $logKey => $log)
        {
            $logCategory = $log[2];
            foreach($this->ignoreCategories as $ignoreCategory)
            {
                if ($logCategory === $ignoreCategory) {
                    unset($logs[$logKey]);

                    continue;
                } else
                if (strpos($ignoreCategory,'.*') !== false)
                {
                    $ignoreCategory = str_replace('.*', '', $ignoreCategory) . '.';
                    if (strpos($logCategory . '.', $ignoreCategory) !== false) {
                        unset($logs[$logKey]);
                    }
                }
            }
        }

        $this->format($logs);

        return $logs;
    }

}