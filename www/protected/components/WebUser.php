<?php

class WebUser extends CWebUser
{
    public $hh = null;

    public $hrBoUser = null;

    public $hrBoVipUser = null;

    /**
     * @var boolean whether to enable cookie-based login. Defaults to false.
     */
    public $allowAutoLogin=true;
    /**
     * @var string|array the URL for login. If using array, the first element should be
     * the route to the login action, and the rest name-value pairs are GET parameters
     * to construct the login URL (e.g. array('/site/login')). If this property is null,
     * a 403 HTTP exception will be raised instead.
     * @see CController::createUrl
     */
    public $loginUrl=array('/user/login');
/*
    public function getRole()
    {
        return $this->getState('__role');
    }
*/
    public function getId()
    {
        return $this->getState('__id') ? $this->getState('__id') : 0;
    }

    protected function afterLogin($fromCookie)
	{
        parent::afterLogin($fromCookie);
	}

	protected function changeIdentity($id,$name,$states)
	{
		parent::changeIdentity($id,$name,$states);
	}

    /**
     * @param int $id
     * @return User
     */
    public function model($id=0) {
        return Yii::app()->getModule('user')->user($id);
    }

    public function user($id=0) {
        return $this->model($id);
    }

    public function getUserByName($username) {
        return Yii::app()->getModule('user')->getUserByName($username);
    }

    public function getAdmins() {
        return Yii::app()->getModule('user')->getAdmins();
    }

    public function isAdmin() {
        return Yii::app()->getModule('user')->isAdmin();
    }


    public function isRegistered()
    {
        return ( !$this->isGuest() && !$this->isAdmin() );
    }

}