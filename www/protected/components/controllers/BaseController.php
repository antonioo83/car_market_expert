<?php

class BaseController extends Controller
{
    /**
     * Отправляет json данные в заданном формате.
     *
     * @param $isSave
     * @param $errorMessage
     * @param array $params
     */
    protected function sendAjaxResponse($isSave, $errorMessage, $params = array())
    {
        header("Content-Type: application/json");
        $result = CJSON::encode(array(
            'isSave' => $isSave,
            'errorMessage' => $errorMessage,
            'params' => $params
        ));
        echo $result;
        Yii::app()->end();
    }

    /**
     * Отправляет json данные.
     *
     * @param array $params
     */
    protected function sendAjaxResponseByParams($params = array())
    {
        header("Content-Type: application/json");
        $result = CJSON::encode($params);
        echo $result;
        Yii::app()->end();
    }

}