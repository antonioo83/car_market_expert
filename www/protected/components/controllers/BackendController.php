<?php

class BackendController extends BaseController
{
    public $layout = '//layouts/forest_main';

    public $menu = array();

    public $breadcrumbs = array();

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

}