<?php
/**
 * Библиотека работы с датой и временем.
 */
class DateTimeLibrary
{
	const LIMIT_DEFAULT = 3;

    const DAY_VALUE = 86400;

    const YEAR_VALUE = 31104000;

    /**
     * Возвращает дату и время в заданном формате + количество лет.
     *
     * @param $value
     * @param string $outputFormat
     * @return string
     */
    public static function getDateTimeMinusValue($datetime, $value, $outputFormat = 'Y-m-d H:i:s', $type = 'Y')
    {
        $period = 'P';
        if (in_array($type, array('H', 'M', 'S'))) {
            $period = 'PT';
        }

        $interval = new DateInterval($period . $value . $type);

        $currentDate = new DateTime($datetime);
        $currentDate->sub($interval);

        return $currentDate->format($outputFormat);
    }

    /**
     * Возвращает дату и время в заданном формате + количество лет.
     *
     * @param $value
     * @param string $outputFormat
     * @return string
     */
    public static function getDateTimePlusValue($datetime, $value, $outputFormat = 'Y-m-d H:i:s', $type = 'Y')
    {
        $period = 'P';
        if (in_array($type, array('H', 'M', 'S'))) {
            $period = 'PT';
        }

        $interval = new DateInterval($period . $value . $type);

        $currentDate = new DateTime($datetime);
        $currentDate->add($interval);

        return $currentDate->format($outputFormat);
    }

	/**
	 * Возвращает текущее время.
	 *
	 * @return string
	 */
	public static function getCurrentDateTime($outputFormat = 'Y-m-d H:i:s')
	{
		$date = new DateTime('now');

		return $date->format($outputFormat);
	}

    /**
     * Возвращает текущее время.
     *
     * @return string
     */
    public static function getCurrentDate($outputFormat = 'Y-m-d')
    {
        $date = new DateTime('now');

        return $date->format($outputFormat);
    }

    /**
     * Возвращает дату в заданном формате.
     *
     * @param $datetime
     * @param $outputFormat
     * @return string
     */
    public static function getDate($datetime, $outputFormat = 'd.m.Y')
    {
        $date = DateTime::createFromFormat('Y-m-d', $datetime);
        if ($date == false) {

            return 0;
        }

        return strtr($date->format($outputFormat),  self::getRussianDateLocales());
    }

    /**
     * Возвращает дату в заданном формате.
     *
     * @param $datetime
     * @param $outputFormat
     * @return string
     */
    public static function getDateTime($datetime, $outputFormat = 'Y-m-d H:i:s')
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
        if ($date == false) {

            return 0;
        }

        return strtr($date->format($outputFormat),  self::getRussianDateLocales());
    }

    /**
     * Возвращает дату в заданном формате.
     *
     * @param $datetime
     * @param $outputFormat
     * @return string
     */
    public static function getDateByDateTime($datetime, $outputFormat = 'd.m.Y')
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
        if ($date == false) {

            return 0;
        }

        return strtr($date->format($outputFormat), self::getRussianDateLocales());
    }

    /**
     * Возвращает дату в заданном формате.
     *
     * @param $datetime
     * @param $outputFormat
     * @return string
     */
    public static function getDateByEuroDate($datetime, $outputFormat = 'Y-m-d')
    {
        $date = DateTime::createFromFormat('d-m-Y', $datetime);
        if ($date == false) {

            return 0;
        }

        return $date->format($outputFormat);
    }

	/**
	 * Определяет прошло ли заданное время.
	 *
	 * @param $dateTime
	 * @return bool
	 */
	public static function getPassedYear($dateTime)
	{
		if (empty($dateTime)) {
			
			return false;
		}


        $compareDatetime = DateTime::createFromFormat('Y-m-d', $dateTime);
        if (empty($compareDatetime)) {

           return false;
        }

        $currentDateTime = new DateTime("now");
        $interval = $currentDateTime->diff($compareDatetime);
        if ($interval->y > 150) {

            return 0;
        }

        return $interval->y;
    }

    /**
     * Определяет количество прошедших дней текущей даты от заданной.
     *
     * @param $dateTime
     * @return bool
     */
    public static function getPassedDay($dateTime)
    {
        if (empty($dateTime)) {

            return false;
        }

        $currentDateTime = new DateTime("now");
        $compareDatetime = DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
        $interval = $currentDateTime->diff($compareDatetime);

        return $interval->days;
    }

    /**
     * Определяет прошло ли заданное время.
     *
     * @param $dateTime
     * @return bool
     */
    public static function getPassedSecond($dateTime)
    {
        if (empty($dateTime)) {

            return false;
        }

        $currentDateTime = new DateTime("now");
        $compareDatetime = DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
        $timestamp = $currentDateTime->getTimestamp() - $compareDatetime->getTimestamp();

        return $timestamp;
    }

    /**
     * Определяет прошло ли заданное время.
     *
     * @param $dateTime
     * @return bool
     */
    public static function getPassedYearsAndMonthsBetweenT1andT2($dateTime1, $datetime2)
    {
        if (empty($dateTime1)) {

            return false;
        }


        $compareDatetime = DateTime::createFromFormat('Y-m-d', $dateTime1);
        if (empty($compareDatetime)) {

            return false;
        }

        $currentDateTime = DateTime::createFromFormat('Y-m-d', $datetime2);
        $interval = $currentDateTime->diff($compareDatetime);

        if (($interval->y > 0) && ($interval->m > 0)) {

            return $interval->y . ' года ' . $interval->m . ' месяцев';
        } else
        if (($interval->y == 0) && ($interval->m > 0)) {

            return $interval->m . ' месяцев';
        } else
        if (($interval->y >0) && ($interval->m > 0)) {

            return $interval->y . ' года ';
        }

        return '';
    }

    /**
     * @param $datetime
     * @return string
     */
    public static function getDatetimeStringByAtomDateTime($datetime)
    {
        $datetimeStr = DateTime::createFromFormat('Y-m-d\TH:i:sP', $datetime);
        if (empty($datetimeStr)) {

            return '';
        }

        return $datetimeStr->format('Y-m-d H:i:s');
    }

    /**
     * Выдает массив дат в заданном формате между датой 1 и датой 2.
     *
     * @param $startDate
     * @param $finishDate
     * @param string $outputFormat
     * @param int $deltaValue
     * @return array
     */
    public static function getDatesBetweenDate1AndDate2($startDate, $finishDate, $outputFormat = 'Y-m-d', $deltaValue = -1)
    {
        $dates = array();

        $startDate = DateTime::createFromFormat('Y-m-d', $startDate);
        if ($startDate == false) {

            return array();
        }

        $finishDate = DateTime::createFromFormat('Y-m-d', $finishDate);
        if ($finishDate == false) {

            return array();
        }

        if ($deltaValue == -1) {
            $deltaValue = self::getDeltaTimestampByDate1AndDate2($startDate, $finishDate);
        }

        $pointCount = self::getPointCount($startDate, $finishDate, $deltaValue);

        $dates[] = strtr($startDate->format($outputFormat),  self::getRussianDateLocales());
        for ($i = 0; $i < $pointCount; $i++) {
            $startDate->setTimestamp($startDate->getTimestamp() + $deltaValue);
            $dates[] = strtr($startDate->format($outputFormat),  self::getRussianDateLocales());
        }


        if (in_array($deltaValue, array(self::DAY_VALUE))) {
            $startDate->setTimestamp($startDate->getTimestamp() + $deltaValue);
            $dates[] = strtr($startDate->format($outputFormat),  self::getRussianDateLocales());
        }

        // Задает конечное значение = конечному из заданного интервала.
        if ($startDate->getTimestamp() != $finishDate->getTimestamp()) {
            $dates[count($dates) - 1] = strtr($finishDate->format($outputFormat),  self::getRussianDateLocales());
        }

        return $dates;
    }

    /**
     * Возвращает целое количество дельта диапазонов полученное из заданного диапазона.
     *
     * @param DateTime $startDate
     * @param DateTime $finishDate
     * @param $deltaValue
     * @return int
     */
    private static function getPointCount(DateTime $startDate, DateTime $finishDate, $deltaValue)
    {
        return (int) round(($finishDate->getTimestamp() - $startDate->getTimestamp()) / $deltaValue, 0, PHP_ROUND_HALF_DOWN);
    }

    /**
     * Возвращает временной диапазон в виде timestamp в зависимости от длины заданного интервала.
     *
     * @param $date1
     * @param $date2
     * @return int
     */
    public static function getDeltaTimestampByDate1AndDate2(DateTime $date1, DateTime $date2)
    {
        $deltaTimestamp = $date2->getTimestamp() - $date1->getTimestamp();
        if ($deltaTimestamp <= 0) {

            return 86400;
        }

        if ($deltaTimestamp > 86400*365*1) {

            return 86400*30*12;
        } else
        if ($deltaTimestamp > 86400*7*15) {

            return 86400*30;
        } else
        if ($deltaTimestamp > 86400*14) {

            return 86400*7;
        }

        return 86400;
    }

    /**
     * @return array
     */
    public static function getMonthInRussianLocals()
    {
        return array(
            '1' => 'Январь',
            '2' => 'Февраль',
            '3' => 'Март',
            '4' => 'Апрель',
            '5' => 'Май',
            '6' => 'Июнь',
            '7' => 'Июль',
            '8' => 'Август',
            '9' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        );
    }

    /**
     * Возвращает список месяцев в латинице.
     *
     * @return array
     */
    public static function getEnglishMonths()
    {
        return array(
            'январь' => 'January',
            'февраль' =>'February',
            'март' => 'March',
            'апрель' => 'April',
            'май' => 'May',
            'июнь' => 'June',
            'июль' => 'July',
            'август' => 'August',
            'сентябрь' => 'September',
            'октябрь' => 'October',
            'ноябрь' => 'November',
            'декабрь' => 'December',

            'января' => 'January',
            'февраля' =>'February',
            'марта' => 'March',
            'апреля' => 'April',
            'майя' => 'May',
            'июня' => 'June',
            'июля' => 'July',
            'августа' => 'August',
            'сентября' => 'September',
            'октября' => 'October',
            'ноября' => 'November',
            'декабря' => 'December',
        );
    }

    /**
     * @return array
     */
    private static function getRussianDateLocales()
    {
        return array(
            "am" => "дп",
            "pm" => "пп",
            "AM" => "ДП",
            "PM" => "ПП",
            "Monday" => "Понедельник",
            "Mon" => "Пн",
            "Tuesday" => "Вторник",
            "Tue" => "Вт",
            "Wednesday" => "Среда",
            "Wed" => "Ср",
            "Thursday" => "Четверг",
            "Thu" => "Чт",
            "Friday" => "Пятница",
            "Fri" => "Пт",
            "Saturday" => "Суббота",
            "Sat" => "Сб",
            "Sunday" => "Воскресенье",
            "Sun" => "Вс",
            "January" => "Января",
            "Jan" => "Янв",
            "February" => "Февраля",
            "Feb" => "Фев",
            "March" => "Марта",
            "Mar" => "Мар",
            "April" => "Апреля",
            "Apr" => "Апр",
            "May" => "Мая",
            "June" => "Июня",
            "Jun" => "Июн",
            "July" => "Июля",
            "Jul" => "Июл",
            "August" => "Августа",
            "Aug" => "Авг",
            "September" => "Сентября",
            "Sep" => "Сен",
            "October" => "Октября",
            "Oct" => "Окт",
            "November" => "Ноября",
            "Nov" => "Ноя",
            "December" => "Декабря",
            "Dec" => "Дек",
            "st" => "ое",
            "nd" => "ое",
            "rd" => "е",
            "th" => "ое"
        );
    }

} 