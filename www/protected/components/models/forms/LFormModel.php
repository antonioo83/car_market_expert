<?php

class LFormModel extends CFormModel
{

    /**
     *
     */
    public function getLastErrorMessage()
    {
        $lastErrorMessage = '';

        $errors = $this->getErrors();
        foreach ($errors as $key => $error) {
            $lastErrorMessage = $error[0];

            break;
        }

        return $lastErrorMessage;
    }
}