<?php

class DropDownActiveRecord extends LActiveRecord
{
    const DEFAULT_TYPE_ID = 1;

    const DEFAULT_WEIGHT = 1;

    const EDITABLE_ACTION = '/editable/save';

    const EDITABLE_PROTECTED_STATUS_ACTION = '/editable/saveProtectedStatus';

    public $show_title = '';

    public $max_weight = '';

    /**
     * Определяет нужно ли кешировать запрос при получения списка записей (для поведения).
     */
    public $isHoldToCache = true;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array(
            'typeBehavior' => array(
                'class' => 'DropDownBehavior',
            ),
        );
    }

    /**
     * @return DropDownBehavior
     */
    public static function bObject()
    {
        $model = new static();

        return $model->typeBehavior;
    }

    /**
     *
     */
    protected function beforeSave()
    {
        if (($this->weight == 999) || (empty($this->weight))) {
            $this->weight = $this->getMaxWeight() + 1;
        }

        return parent::beforeSave();
    }

    /**
     * @return bool
     */
    protected function beforeDelete()
    {
        if ($this->getCount() == 1) {
            $this->addError('title', 'Должен остаться хотя бы один элемент в списке!');

            return false;
        }

        return parent::beforeDelete();
    }

    /**
     *
     */
    private function getCount()
    {
        return static::model()->count();
    }

    /**
     *
     */
    private function getMaxWeight()
    {
        $criteria = new CDbCriteria;
        $criteria->select='MAX(weight) AS max_weight';
        $result = static::model()->find($criteria);

        return $result['max_weight'];
    }

    /**
     * @return array|CActiveRecord|mixed|null
     */
    public static function getAll($sortFieldName = 'title')
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'weight,' .$sortFieldName . ' ASC';

        return static::model()->findAll($criteria);
    }

    /**
     *
     */
    public static function getDefaultElement()
    {
        return static::model()->findByAttributes(array('weight' => self::DEFAULT_WEIGHT));
    }

    /**
     *
     */
    public static function getAllForEditable()
    {
        $editableElements = array();

        $elements = static::model()->getAll();
        foreach ($elements as $element) {
            $editableElements[] = array('value' => $element->id , 'text' => $element->title);
        }

        return $editableElements;
    }

    /**
     *
     */
    public static function getAllForHhEditable()
    {
        $editableElements = array();

        $elements = static::model()->getAll('name');
        foreach($elements as $element) {
            $editableElements[] = array('value' => $element->id , 'text' => $element->name);
        }

        return $editableElements;
    }

    /**
     * @return array
     */
    public static function getAllForSelect($fieldName = 'title')
    {
        $elements = CHtml::listData(self::getAll($fieldName), 'id', $fieldName);

        return $elements;
    }

}