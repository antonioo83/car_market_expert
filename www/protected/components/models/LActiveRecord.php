<?php

class LActiveRecord extends CActiveRecord
{
    const UNKNOWN_VALUE = -1;

    const MAX_ELEMENT_COUNT = 30;

    /**
     * Алиас таблицы.
     *
     * @var string
     */
    protected $alias = '';

    /**
     *
     */
    public function init()
    {
       $this->alias = $this->getTableAlias();

        parent::init();
    }

    /**
     * Возвращает текущий класс сущности.
     *
     * @return string
     */
    public function getClass()
    {
        return get_class($this);
    }

    /**
     * Записывает последнее сообщение об ошибке в лог.
     */
    public function writeLastErrorToLog()
    {
        Yii::log($this->getLastErrorMessage(), CLogger::LEVEL_ERROR);
    }

    /**
     * Возвращает последнее сообщение об ошибке у текущей модели.
     *
     * @return string
     */
    public function getLastErrorMessage()
    {
        $lastErrorMessage = '';

        $errors = $this->getErrors();
        foreach ($errors as $key => $error) {
            $lastErrorMessage = $error[0];

            break;
        }

        return $lastErrorMessage;
    }

    /**
     * @param string $className
     * @return static
     */
    public static function model($className=__CLASS__)
    {
        //REVIEW ! Anton: Than system have cycle login and logout than criteria does't clear.
        $model =  parent::model($className);
        if (! is_null($model)) {
            $model->setDbCriteria(null);
        }

        return $model;
    }

    /**
     * Возвращает alias таблицы.
     *
     * @return string
     */
    protected function getAlias()
    {
        if (empty($this->alias)) {
            $alias =  $this->getTableAlias(false, false) . '.';
        } else {
            $alias =  $this->alias;
        }

        return $alias;
    }

}