<?php

interface BaseValidatorInterface
{
    /**
     * Валидирует заданное значение.
     *
     * @param $value
     * @return mixed
     */
    public static function isValidate($value);

    /**
     * Возвращает сообщение об ошибке.
     *
     * @return mixed
     */
    public static function getErrorMessage();

}