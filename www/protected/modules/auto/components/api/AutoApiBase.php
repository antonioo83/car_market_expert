<?php

class AutoApiBase
{
    /**
     * @var string
     */
    private $url = '';

    /**
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Формирует заголовки для публикации, когда важно получить Location в ответе.
     */
    protected function getHeaders()
    {
        return array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36 OPR/39.0.2256.48',
            CURLOPT_COOKIESESSION => true,
            CURLOPT_COOKIEJAR => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_COOKIEFILE => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false

        );
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

}