<?php

interface AutoApiInterface
{
    /**
     * Возвращает Html - контент со всеми брендами.
     *
     * @return mixed
     */
    public function getBrandsHtmlContent();

    /**
     * Возвращает Html - контент по заданному url.
     *
     * @return mixed
     */
    public function getHtmlContentByUrl($url);

}