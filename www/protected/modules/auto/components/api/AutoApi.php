<?php

/**
 * Осуществляет взаимодействие (получение контента) с заданым сайтом.
 */
class AutoApi extends AutoApiBase implements AutoApiInterface
{
    /**
     * Возвращает Html - контент со всеми брендами.
     *
     * @return mixed
     */
    public function getBrandsHtmlContent()
    {
        return Yii::app()->curl->setHeaders($this->getHeaders())->get($this->getUrl(), array());
    }

    /**
     * Возвращает Html - контент по заданному url.
     *
     * @return mixed
     */
    public function getHtmlContentByUrl($url)
    {
        return Yii::app()->curl->setHeaders($this->getHeaders())->get($url, array());
    }

}