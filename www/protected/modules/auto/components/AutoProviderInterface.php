<?php

interface AutoProviderInterface
{
    /**
     * Загружает данные с сайта auto.ru непосредственно в БД.
     */
    public function initialize();

    /**
     * Возвращает идентификаторы которые были обновлены.
     *
     * @return array
     */
    public function getUpdatedModels();

}