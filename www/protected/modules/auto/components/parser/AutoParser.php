<?php

/**
 * Осуществляет парсинг контента и приведение его к объектно ориентированному виду.
 */
class AutoParser extends AutoBaseParser implements AutoParserInterface
{
    /**
     * Возвращает древовидный список объектов брендов и их моделей.
     *
     * @return AutoBrandEntityList
     * @throws LoggedException
     */
    public function getBrandEntityList()
    {
        $brandList = $this->getBrands();

        /** @var AutoBrandEntity $brandEntity */
        foreach($brandList->getItems() as $brandEntity)
        {
            $modelList = new AutoModelEntityList();
            $nodes = $this->getDomNodeList(
                $this->api->getHtmlContentByUrl(Yii::app()->getModule('auto')->siteUrl . $brandEntity->url),
                'mmm__item'
            );
            /** @var DOMNode $node */
            foreach ($nodes as $node)
            {
                $brandUrl = '';
                $brandTitle = '';
                if (($node->childNodes->length > 0) && ($node->childNodes->item(0)->hasAttribute('href'))) {
                    $brandUrl = $node->childNodes->item(0)->getAttribute('href');
                    $brandTitle = $node->childNodes->item(0)->nodeValue;
                }

                $countBrand = 0;
                if (($node->childNodes->length > 1)) {
                    $countBrand = $node->childNodes->item(1)->nodeValue;
                }

                $modelList->add(new AutoModelEntity($brandTitle, $brandUrl, $countBrand));
            }

            $brandEntity->setModelList($modelList);
        }

        return $brandList;
    }


    /**
     * Возвращает список брендов авто в виде списков объектов.
     *
     * @return AutoBrandEntityList
     */
    private function getBrands()
    {
        $brandList = new AutoBrandEntityList();

        $nodes = $this->getDomNodeList(
            $this->api->getBrandsHtmlContent(),
            'mmm__item'
        );

        /** @var DOMNode $node */
        foreach ($nodes as $node)
        {
            $brandUrl = '';
            $brandTitle = '';
            if (($node->childNodes->length > 0) && ($node->childNodes->item(0)->hasAttribute('href'))) {
                $brandUrl = $node->childNodes->item(0)->getAttribute('href');
                $brandTitle = $node->childNodes->item(0)->nodeValue;
            }

            $countBrand = 0;
            if (($node->childNodes->length > 1)) {
                $countBrand = $node->childNodes->item(1)->nodeValue;
            }

            $brandList->add(new AutoBrandEntity($brandTitle, $brandUrl, $countBrand));
        }

        return $brandList;
    }

}