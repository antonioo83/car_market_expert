<?php

class AutoBaseParser
{
    /**
     * @var AutoApiInterface
     */
    public $api = null;

    /**
     * AutoApiParser constructor.
     * @param AutoApiInterface $api
     */
    public function __construct(AutoApiInterface $api)
    {
        $this->api = $api;
    }

    /**
     * Возвращает DOM список из HTML - контента полученного по заданному классу.
     *
     * @param $content
     * @param $class
     * @return DOMNodeList
     * @throws LoggedException
     */
    protected function getDomNodeList($content, $class)
    {
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        if ($doc->loadHTML($content) == false) {
            throw new LoggedException('Не могу загрузить документ');
        }

        $finder = new DomXPath($doc);
        $classname = $class;

        return $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
    }

}