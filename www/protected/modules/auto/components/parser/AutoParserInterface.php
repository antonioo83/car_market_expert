<?php

interface AutoParserInterface
{
    /**
     * Возвращает древовидный список объектов брендов и их моделей.
     *
     * @return AutoBrandEntityList
     * @throws LoggedException
     */
    public function getBrandEntityList();
}