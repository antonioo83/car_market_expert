<?php

abstract class AutoEntityBase
{
    /**
     * @var null
     */
    public $id = null;

    /**
     * @var string
     */
    public $url = '';

    /**
     * Краткое наименование в Url.
     *
     * @var string
     */
    public $slug = '';

    /**
     * Наименование бренда.
     *
     * @var string
     */
    public $title = '';

    /**
     * Количество объявлений.
     *
     * @var int
     */
    public $count = 0;

    /**
     * AutoEntityBase constructor.
     * @param string $url
     * @param string $title
     * @param int $count
     */
    public function __construct($title, $url, $count = 0)
    {
        $this->title = $title;
        $this->url = $url;
        $this->count = $count;
        $this->slug = $this->getSlugByUrl($url);
    }

    /**
     * Возвращает slug из заданного url.
     *
     * @param $url
     * @return string
     */
    protected function getSlugByUrl($url)
    {
        $parts = explode('/', $url);
        if (count($parts) > 2) {

            return $parts[3];
        }

        return '';
    }

}