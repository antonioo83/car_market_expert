<?php

class AutoModelEntityList
{
    /**
     * @var AutoModelEntity[]
     */
    private $items = array();

    /**
     * @param AutoModelEntity $item
     */
    public function add(AutoModelEntity $item)
    {
        $this->items[] = $item;
    }

    /**
     * @param $index
     * @return AutoModelEntity|null
     */
    public function getByIndex($index)
    {
        return isset($this->items[$index]) ? $this->items[$index] : null;
    }

    /**
     * @param $index
     */
    public function remove($index)
    {
        if (array_key_exists($index, $this->items)) {
            unset($this->items[$index]);
        }
    }

    /**
     * @return AutoModelEntity[]
     */
    public function getItems() {

        return $this->items;
    }

}