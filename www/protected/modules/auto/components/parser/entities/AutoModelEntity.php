<?php

class AutoModelEntity extends AutoEntityBase
{
    /**
     * AutoEntityBase constructor.
     * @param string $url
     * @param string $title
     * @param int $count
     */
    public function __construct($title, $url, $count = 0)
    {
        parent::__construct($title, $url, $count);
    }

    /**
     * Возвращает slug из заданного url.
     *
     * @param $url
     * @return string
     */
    protected function getSlugByUrl($url)
    {
        $parts = explode('/', $url);
        if (count($parts) > 3) {

            return $parts[4];
        }

        return '';
    }

}