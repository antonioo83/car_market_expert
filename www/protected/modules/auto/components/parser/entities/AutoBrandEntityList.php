<?php

class AutoBrandEntityList
{
    /**
     * @var AutoBrandEntity[]
     */
    private $items = array();

    /**
     * @param AutoBrandEntity $item
     */
    public function add(AutoBrandEntity $item)
    {
        $this->items[] = $item;
    }

    /**
     * @param $index
     * @return AutoBrandEntity|null
     */
    public function getByIndex($index)
    {
        return isset($this->items[$index]) ? $this->items[$index] : null;
    }

    /**
     * @param $index
     */
    public function remove($index)
    {
        if (array_key_exists($index, $this->items)) {
            unset($this->items[$index]);
        }
    }

    /**
     * @return AutoBrandEntity[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Осуществляет поиск по slug.
     *
     * @param $slug
     * @return AutoBrandEntity|null
     */
    public function getBySlug($slug)
    {
        /** @var AutoBrandEntity $brand */
        foreach ($this->getItems() as $brand) {
            if ($brand->slug == $slug) {

                return $brand;
            }
        }

        return null;
    }

    /**
     * Осуществляет поиск по бренду и модели.
     *
     * @param $brandSlug
     * @param $modelSlug
     * @return AutoModelEntity|null
     */
    public function getModelByBrandSlugAndModelSlug($brandSlug, $modelSlug)
    {
        $brand = $this->getBySlug($brandSlug);
        if (! is_null($brand)) {
            $model = $brand->getModelBySlug($modelSlug);
            if (! is_null($model)) {

                return $model;
            }
        }

        return null;
    }

}