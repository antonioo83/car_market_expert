<?php

class AutoBrandEntity extends AutoEntityBase
{
    /**
     * @var AutoModelEntityList
     */
    private $modelList = null;

    /**
     * @return AutoModelEntity[]
     */
    public function getModels()
    {
        return $this->modelList->getItems();
    }

    /**
     * @return AutoModelEntityList
     */
    public function getModelList()
    {
        return $this->modelList;
    }

    /**
     * @param AutoModelEntityList $modelList
     */
    public function setModelList($modelList)
    {
        $this->modelList = $modelList;
    }

    /**
     * @param $slug
     * @return AutoBrandEntity|null
     */
    public function getModelBySlug($slug)
    {
        /** @var AutoBrandEntity $brand */
        foreach ($this->getModelList()->getItems() as $model) {
            if ($model->slug == $slug) {

                return $model;
            }
        }

        return null;
    }

}