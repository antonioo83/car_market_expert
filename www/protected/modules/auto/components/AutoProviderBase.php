<?php

class AutoProviderBase
{
    /**
     * @var AutoParserInterface
     */
    protected $parser = null;

    /**
     * @var AutoBrandEntityList
     */
    protected $brandList = null;

    /**
     * @param AutoParserInterface $parser
     */
    public function __construct(AutoParserInterface $parser)
    {
        $this->parser = $parser;
        $this->brandList = null;
    }

    /**
     * @return AutoBrandEntityList|null
     */
    protected function getBrandList()
    {
        if (is_null($this->brandList)) {
            $this->brandList = $this->parser->getBrandEntityList();
        }

        return $this->brandList;
    }

}