<?php

/**
 * Осуществляет работу с БД: создание новых брендов и моделей, обновление данных.
 */
class AutoProvider extends AutoProviderBase implements AutoProviderInterface
{
    /**
     * Загружает данные с сайта auto.ru непосредственно в БД.
     */
    public function initialize()
    {
        $result = false;

        //REVIEW ! Anton: Проверка на дубликаты.

        $brand = Brand::model()->find();
        if (is_null($brand))
        {
            /** @var AutoBrandEntity $brandEntity */
            foreach ($this->getBrandList()->getItems() as $brandEntity)
            {
                $brand = new Brand();
                $brand->title = $brandEntity->title;
                $brand->slug = $brandEntity->slug;
                $brand->count = $brandEntity->count;
                $brand->url = $brandEntity->url;
                if ($brand->save() == false) {
                    $brand->writeLastErrorToLog();
                } else {
                    $modelsPackage = array();
                    /** @var AutoModelEntity $modelEntity */
                    foreach ($brandEntity->getModelList()->getItems() as $modelEntity) {
                        $model = new CarModel();
                        $model->brand_id = $brand->id;
                        $model->title = $modelEntity->title;
                        $model->slug = $modelEntity->slug;
                        $model->count = $modelEntity->count;
                        $model->url = $modelEntity->url;
                        if ($model->save() == false) {
                            $model->writeLastErrorToLog();
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Возвращает список моделей у которых был обнавлен параметр каунт.
     *
     * @return CarModel[]
     */
    public function getUpdatedModels()
    {
        $updatedModels = array();

        //REVIEW ! Anton: проверить на наличие новых моделей.

        $brands = Brand::model()->findAll();
        /** @var Brand $brand */
        foreach ($brands as $brand)
        {
            $brandEntity = $this->getBrandList()->getBySlug($brand->slug);
            if (! is_null($brandEntity)) {
                $brandEntity->id = $brand->id;
            }

            /** @var  CarModel $model */
            foreach ($brand->models as $model)
            {
                $modelEntity = $this->getBrandList()->getModelByBrandSlugAndModelSlug($brand->slug, $model->slug);
                if (! is_null($modelEntity))
                {
                    $modelEntity->id = $model->id;
                    if ($modelEntity->count != $model->count)
                    {
                        $model->count = $modelEntity->count;
                        if ($model->save() == false) {
                            $model->writeLastErrorToLog();
                        }

                        $updatedModels[] = $model;
                    }
                }
            }
        }

        return $updatedModels;
    }

}