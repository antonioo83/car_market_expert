<?php

class AutoModule extends CWebModule
{
    /**
     * @var string
     */
    public $siteUrl = '';

    /**
     * @var string
     */
    public $region = 'rossiya';

    /**
     *
     */
	public function init()
	{
		$this->setImport(array(
			'auto.models.*',
			'auto.components.*',
            'auto.components.api.*',
            'auto.components.parser.*',
            'auto.components.parser.entities.*',
            'auto.components.interfaces.*',
		));
	}

    /**
     * @param CController $controller
     * @param CAction $action
     * @return bool
     */
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->siteUrl . '/' . $this->region . '/';
    }

}
