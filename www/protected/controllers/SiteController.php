<?php

class SiteController extends FrontendController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $moduleAuto = Yii::app()->getModule('auto');

        $api = new AutoApi($moduleAuto->getUrl());
        $provider = new AutoProvider(new AutoParser($api));

        $provider->initialize();
        $modelIds = $provider->getUpdatedModels();

		$this->render('index', array(
            'modelIds' => $modelIds
        ));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{

	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{

	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{

	}
}