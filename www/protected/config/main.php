<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

return CMap::mergeArray(
    // CConsole application doesn't handle "theme" property
    // and as the result throws the undefined property exception.
    // So do this trick ...
    (php_sapi_name() !== 'cli' ? array('theme' => 'personium') : null)
, array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'PERSONIUM',
    'language'=>'ru',
    'timeZone' => 'Europe/Moscow',
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.models.forms.*',
        'application.models.types.*',

        'application.components.*',
        'application.components.interfaces.*',
        'application.components.controllers.*',
        'application.components.models.*',
        'application.components.models.forms.*',
        'application.components.validators.*',
        'application.components.exceptions.*',
        'application.components.behaviors.*',
        'application.components.providers.*',
        'application.components.converters.*',
        'application.components.widgets.*',
        'application.components.services.*',
    ),
));