<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'PERSONIUM',
    'language'=>'ru',
    'timeZone' => 'Europe/Moscow',
    //  'theme' => 'personium',
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.models.forms.*',
        'application.models.types.*',

        'application.components.*',
        'application.components.interfaces.*',
        'application.components.controllers.*',
        'application.components.models.*',
        'application.components.models.forms.*',
        'application.components.validators.*',
        'application.components.exceptions.*',
        'application.components.behaviors.*',
        'application.components.providers.*',
        'application.components.converters.*',
        'application.components.widgets.*',
        'application.components.services.*',

        'application.extensions.yiisortablemodel.behaviors.*',
    ),
);