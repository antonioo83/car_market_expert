<?php
return CMap::mergeArray(
    require_once(dirname(__FILE__) . '/db.php'),
    array(
        'preload' => array(
            'log',
            'debug',
        ),
        'components' => array(
            'cache'=>array(
                'class'=>'CFileCache',
            ),

            'bootstrap' => array(
                'class' => 'bootstrap.components.Bootstrap',
            ),
            'user' => array(
                'class' => 'WebUser',
                // 'allowAutoLogin' => true,
                //'loginUrl' => array('login/index'),
            ),
            'urlManager' => array(
                'showScriptName' => false,
                'urlFormat' => 'path',
                'rules' => array(
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ),

            ),
            'errorHandler' => array(
                'errorAction' => 'site/error',
            ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error',
                    ),
                ),
            ),
            'curl' => array(
                'class' => 'application.extensions.curl.Curl'
            ),
        ),
        'modules' => require(dirname(__FILE__) . '/modules.php'),
        'params' => require(dirname(__FILE__) . '/params.php'),
    ),
    require_once(dirname(__FILE__) . '/../main_console.php')
);