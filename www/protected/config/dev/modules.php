<?php
return array(
    'gii' => array(
        'class' => 'system.gii.GiiModule',
        'password' => '123',
        'ipFilters' => array('*'),
        'generatorPaths' => array(
            'bootstrap.gii',
        ),
    ),
    'auto' => array(
        'siteUrl' => 'http://auto.ru',
        'region' => 'rossiya'
    )
);