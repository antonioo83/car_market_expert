<?php
return array(
    'components' => array(
        'db' => array(
            'tablePrefix' => 'cme_',
            'class'=>'system.db.CDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=car_market_expert',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'NEXTON',
            'charset' => 'utf8',
            'enableProfiling'=>true,
        ),
    )
);