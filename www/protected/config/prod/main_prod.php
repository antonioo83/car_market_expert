<?php
return CMap::mergeArray(
    require_once(dirname(__FILE__) . '/db.php'),
    array(
        'defaultController' => 'site/index',
        'preload' => array(
            'debug',
        ),
        'components' => array(
            'cache'=>array(
                'class'=>'CDbCache',
            ),

            'bootstrap' => array(
                'class' => 'bootstrap.components.Bootstrap',
            ),
            'user' => array(
                'class' => 'WebUser',
                // 'allowAutoLogin' => true,
                //'loginUrl' => array('login/index'),
            ),
            'urlManager' => array(
                'showScriptName' => false,
                'urlFormat' => 'path',
                'rules' => array(
                    'gii' => 'gii',
                    'gii/<controller:\w+>' => 'gii/<controller>',
                    'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ),
            ),
            'errorHandler' => array(
                'errorAction' => 'site/error',
            ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error',
                        'filter' => array(
                            'class'=>'LLogFilter',
                            'ignoreCategories' => array(
                                'exception.CHttpException.404',
                            ),
                        ),
                    ),
                ),
            ),
            ($_SERVER['HTTP_HOST'] != 'personium.local') ? '' : 'debug' => array('class' => 'application.extensions.yii2-debug.Yii2Debug'),
            'curl' => array(
                'class' => 'application.extensions.curl.Curl',
                'options' => array(
                    CURLOPT_USERAGENT => 'Personium_dev/1.0a (tech@personium.ru)',
                )
            ),
        ),
        'modules' => require(dirname(__FILE__) . '/modules.php'),
        'params' => require(dirname(__FILE__) . '/params.php'),
    ),
    require_once(dirname(__FILE__) . '/../main.php')
);