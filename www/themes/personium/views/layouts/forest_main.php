<!doctype html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
    <meta charset="utf-8">
    <title>Personium</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/favicon.ico" rel="icon" type="image/x-icon" />

    <link href="/themes/forest/css/bootstrap3.min.css" rel="stylesheet"/>
<!--    <link href="/themes/forest/css/bootstrap-theme3.min.css" rel="stylesheet"/>-->
    <link href="/themes/forest/plugins/form-xeditable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <link href="/themes/forest/fonts/glyphicons/css/glyphicons.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/themes/forest/css/styles.css">
    <link rel="stylesheet" href="/themes/forest/css/personium-style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <link href="<?php echo '/themes/forest/' . 'fonts/font-awesome/less/variables.less'; ?>" rel="stylesheet/less" media="all">
    <link href="<?php echo '/themes/forest/' . 'less/styles.less'; ?>" rel="stylesheet/less" media="all">
    <link href="<?php echo '/themes/forest/' . 'plugins/form-datepicker/css/datepicker.css'; ?>" rel="stylesheet" media="all">
    <link href="<?php echo '/themes/forest/' . 'plugins/form-datepicker/less/datepicker.less'; ?>" rel="stylesheet/less" media="all">
    <link href="<?php echo '/themes/forest/' . 'plugins/form-daterangepicker/daterangepicker-bs3.css'; ?>" rel="stylesheet" media="all">
<!--    <link href="--><?php //echo '/themes/forest/' . 'plugins/select2/select2.css'; ?><!--" rel="stylesheet" media="all">-->
    <link rel="stylesheet" type="text/css" href="/themes/forest/plugins/tooltipster/css/tooltipster.css" />
    <link href="/themes/forest/css/ladda.scss" rel="stylesheet"/>
    <link href="/themes/forest/css/ladda-theme.scss" rel="stylesheet"/>
    <link href="/themes/forest/plugins/pines-notify/jquery.pnotify.default.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet" />

    <!--[if lt IE 9]>
    <link rel="staylesheet" href="assets/css/ie8.css">
    <script type="text/javascript" src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl . 'themes/forest/' . 'plugins/charts-flot/excanvas.min.js'; ?>"></script>
    <![endif]-->

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>

    <script src="/js/jquery-ui-i18n.min.js"></script>


    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'js/less.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'js/bootstrap.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'js/enquire.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'js/jquery.cookie.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'js/jquery.nicescroll.min.js'; ?>"></script>

    <!-- Edit Area -->
    <script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>

    <!-- Online edit -->
    <script type="text/javascript" src="/themes/forest/plugins/form-inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'plugins/form-daterangepicker/moment.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'plugins/form-daterangepicker/daterangepicker.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'plugins/form-xeditable/bootstrap3-editable/js/bootstrap-editable.min.js'; ?>"></script>

    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'plugins/form-datepicker/js/bootstrap-datepicker.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo '/themes/forest/' . 'plugins/form-datepicker/js/locales/bootstrap-datepicker.ru.js'; ?>"></script>
<!--    <script type="text/javascript" src="/themes/forest/plugins/select2/select2.min.js"></script>-->
    <script type="text/javascript" src="/themes/forest/js/spin.js"></script>
    <script type="text/javascript" src="/themes/forest/js/ladda.js"></script>
    <script type="text/javascript" src="/themes/forest/plugins/pines-notify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="/themes/forest/plugins/tooltipster/js/jquery.tooltipster.min.js"></script>

    <!-- Personium action -->
    <script type="text/javascript" src="<?php echo '/js/personium_action.js'; ?>"></script>

    <!-- Search menu -->
    <script type='text/javascript' src="/themes/forest/js/application.js"></script>

    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/select2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/i18n/ru.js"></script>

    <!-- ad -->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <link rel="stylesheet" href="/themes/forest/plugins/form-toggle/toggles.css">
    <link rel="stylesheet" href="/themes/forest/plugins/form-toggle/toggles-light.css">
    <script type='text/javascript' src='/themes/forest/plugins/form-toggle/toggle.min.js'></script>

</head>
<body class="horizontal-nav ">
    <?php $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts._header'); ?>

    <nav class="navbar navbar-default yamm" role="navigation" style="<?php echo $navbarStyle; ?>">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse large-icons-nav" id="horizontal-navbar"></div>
    </nav>
    <div id="page-container">
        <div id="page-content">
            <div id="wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <? echo $content ?>
                        </div>
                    </div>
                </div> <!-- container -->
            </div> <!-- #wrap -->
        </div> <!-- page-content -->

        <?php echo $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts.' . '_footer'); ?>
    </div> <!-- page-container -->
</body>