<header class="navbar navbar-inverse navbar-fixed-top" role="banner">
    <a href="/user/logout/logout" class="logout-btn"><i class="fa fa-sign-out"></i></a>

    <div class="navbar-header pull-left" style="width: 500px">
        <a class="navbar-personium-brand" target="_blank" href="http://jobify.ru" style="width:140px"><img height="20px" src="/img/jobify_white.png" alt="" class="logo-main"/></a>
        <div style="padding-left: 20px; height: 20px; padding-top: 12px; color: white">
             <strong>
                 <a style="color: white; text-decoration: underline" target="_blank" href="https://chrome.google.com/webstore/detail/personium/lgnffcocmdcdddmbjclojbcbnlcjdblo?hl=ru">Personium для Google Chrome</a>
                 <a style="color: white; padding-left: 5px;" target="_blank" href="http://personium.ru/click">
                     <i class="fa fa-question-circle"></i>
                 </a>
             </strong>
        </div>
    </div>
</header>