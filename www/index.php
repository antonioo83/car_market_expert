<?php
error_reporting(E_ALL);
defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

$yii=dirname(__FILE__).'../../yii/yii.php';

$config=dirname(__FILE__).'/protected/config/dev/main_dev.php';

// remove the following line when in production mode
// defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);
Yii::createWebApplication($config)->run();
