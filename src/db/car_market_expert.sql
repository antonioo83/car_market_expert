/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 10.1.13-MariaDB : Database - car_market_expert
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `cme_brand` */

DROP TABLE IF EXISTS `cme_brand`;

CREATE TABLE `cme_brand` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `url` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=793 DEFAULT CHARSET=utf8;

/*Data for the table `cme_brand` */

insert  into `cme_brand`(`id`,`title`,`slug`,`count`,`url`) values 
(529,'ВАЗ (Lada)','vaz',49922,'/rossiya/cars/vaz/all/?listing=listing'),
(530,'Audi','audi',12583,'/rossiya/cars/audi/all/?listing=listing'),
(531,'Bentley','bentley',253,'/rossiya/cars/bentley/all/?listing=listing'),
(532,'BMW','bmw',15759,'/rossiya/cars/bmw/all/?listing=listing'),
(533,'Chery','chery',1779,'/rossiya/cars/chery/all/?listing=listing'),
(534,'Chevrolet','chevrolet',12204,'/rossiya/cars/chevrolet/all/?listing=listing'),
(535,'Citroen','citroen',3307,'/rossiya/cars/citroen/all/?listing=listing'),
(536,'Daewoo','daewoo',6078,'/rossiya/cars/daewoo/all/?listing=listing'),
(537,'Ferrari','ferrari',76,'/rossiya/cars/ferrari/all/?listing=listing'),
(538,'Ford','ford',16079,'/rossiya/cars/ford/all/?listing=listing'),
(539,'Honda','honda',5138,'/rossiya/cars/honda/all/?listing=listing'),
(540,'Hyundai','hyundai',17927,'/rossiya/cars/hyundai/all/?listing=listing'),
(541,'Infiniti','infiniti',1988,'/rossiya/cars/infiniti/all/?listing=listing'),
(542,'Kia','kia',14524,'/rossiya/cars/kia/all/?listing=listing'),
(543,'Lamborghini','lamborghini',24,'/rossiya/cars/lamborghini/all/?listing=listing'),
(544,'Land Rover','land_rover',4588,'/rossiya/cars/land_rover/all/?listing=listing'),
(545,'Lexus','lexus',4189,'/rossiya/cars/lexus/all/?listing=listing'),
(546,'Mazda','mazda',7658,'/rossiya/cars/mazda/all/?listing=listing'),
(547,'Mercedes-Benz','mercedes',18163,'/rossiya/cars/mercedes/all/?listing=listing'),
(548,'MINI','mini',669,'/rossiya/cars/mini/all/?listing=listing'),
(549,'Mitsubishi','mitsubishi',10679,'/rossiya/cars/mitsubishi/all/?listing=listing'),
(550,'Nissan','nissan',15725,'/rossiya/cars/nissan/all/?listing=listing'),
(551,'Opel','opel',11062,'/rossiya/cars/opel/all/?listing=listing'),
(552,'Peugeot','peugeot',5026,'/rossiya/cars/peugeot/all/?listing=listing'),
(553,'Renault','renault',12900,'/rossiya/cars/renault/all/?listing=listing'),
(554,'Rolls-Royce','rolls_royce',94,'/rossiya/cars/rolls_royce/all/?listing=listing'),
(555,'Skoda','skoda',7936,'/rossiya/cars/skoda/all/?listing=listing'),
(556,'SsangYong','ssang_yong',1907,'/rossiya/cars/ssang_yong/all/?listing=listing'),
(557,'Subaru','subaru',3022,'/rossiya/cars/subaru/all/?listing=listing'),
(558,'Suzuki','suzuki',2806,'/rossiya/cars/suzuki/all/?listing=listing'),
(559,'Tesla','tesla',41,'/rossiya/cars/tesla/all/?listing=listing'),
(560,'Toyota','toyota',19072,'/rossiya/cars/toyota/all/?listing=listing'),
(561,'Volkswagen','volkswagen',20600,'/rossiya/cars/volkswagen/all/?listing=listing'),
(562,'Volvo','volvo',4089,'/rossiya/cars/volvo/all/?listing=listing'),
(563,'ГАЗ','gaz',2854,'/rossiya/cars/gaz/all/?listing=listing'),
(564,'УАЗ','uaz',2993,'/rossiya/cars/uaz/all/?listing=listing'),
(565,'AC','ac',2,'/rossiya/cars/ac/all/?listing=listing'),
(566,'Acura','acura',158,'/rossiya/cars/acura/all/?listing=listing'),
(567,'Adler','adler',1,'/rossiya/cars/adler/all/?listing=listing'),
(568,'Alfa Romeo','alfa_romeo',255,'/rossiya/cars/alfa_romeo/all/?listing=listing'),
(569,'Alpina','alpina',10,'/rossiya/cars/alpina/all/?listing=listing'),
(570,'Alpine','alpine',0,'/rossiya/cars/alpine/all/?listing=listing'),
(571,'AM General','am_general',0,'/rossiya/cars/am_general/all/?listing=listing'),
(572,'AMC','amc',0,'/rossiya/cars/amc/all/?listing=listing'),
(573,'Ariel','ariel',0,'/rossiya/cars/ariel/all/?listing=listing'),
(574,'Aro','aro',1,'/rossiya/cars/aro/all/?listing=listing'),
(575,'Asia','asia',3,'/rossiya/cars/asia/all/?listing=listing'),
(576,'Aston Martin','aston_martin',38,'/rossiya/cars/aston_martin/all/?listing=listing'),
(578,'Austin','austin',0,'/rossiya/cars/austin/all/?listing=listing'),
(579,'Autobianchi','autobianchi',0,'/rossiya/cars/autobianchi/all/?listing=listing'),
(580,'Baltijas Dzips','baltijas_dzips',0,'/rossiya/cars/baltijas_dzips/all/?listing=listing'),
(581,'Batmobile','batmobile',1,'/rossiya/cars/batmobile/all/?listing=listing'),
(582,'Beijing','beijing',0,'/rossiya/cars/beijing/all/?listing=listing'),
(584,'Bertone','bertone',1,'/rossiya/cars/bertone/all/?listing=listing'),
(585,'Bitter','bitter',0,'/rossiya/cars/bitter/all/?listing=listing'),
(587,'Borgward','borgward',1,'/rossiya/cars/borgward/all/?listing=listing'),
(588,'Brabus','brabus',2,'/rossiya/cars/brabus/all/?listing=listing'),
(589,'Brilliance','brilliance',40,'/rossiya/cars/brilliance/all/?listing=listing'),
(590,'Bristol','bristol',0,'/rossiya/cars/bristol/all/?listing=listing'),
(591,'Bufori','bufori',0,'/rossiya/cars/bufori/all/?listing=listing'),
(592,'Bugatti','bugatti',2,'/rossiya/cars/bugatti/all/?listing=listing'),
(593,'Buick','buick',20,'/rossiya/cars/buick/all/?listing=listing'),
(594,'BYD','byd',119,'/rossiya/cars/byd/all/?listing=listing'),
(595,'Byvin','byvin',0,'/rossiya/cars/byvin/all/?listing=listing'),
(596,'Cadillac','cadillac',799,'/rossiya/cars/cadillac/all/?listing=listing'),
(597,'Callaway','callaway',0,'/rossiya/cars/callaway/all/?listing=listing'),
(598,'Carbodies','carbodies',0,'/rossiya/cars/carbodies/all/?listing=listing'),
(599,'Caterham','caterham',0,'/rossiya/cars/caterham/all/?listing=listing'),
(600,'Changan','changan',28,'/rossiya/cars/changan/all/?listing=listing'),
(601,'ChangFeng','changfeng',1,'/rossiya/cars/changfeng/all/?listing=listing'),
(604,'Chrysler','chrysler',904,'/rossiya/cars/chrysler/all/?listing=listing'),
(606,'Cizeta','cizeta',0,'/rossiya/cars/cizeta/all/?listing=listing'),
(607,'Coggiola','coggiola',0,'/rossiya/cars/coggiola/all/?listing=listing'),
(608,'Dacia','dacia',23,'/rossiya/cars/dacia/all/?listing=listing'),
(609,'Dadi','dadi',2,'/rossiya/cars/dadi/all/?listing=listing'),
(611,'Daihatsu','daihatsu',185,'/rossiya/cars/daihatsu/all/?listing=listing'),
(612,'Daimler','daimler',4,'/rossiya/cars/daimler/all/?listing=listing'),
(613,'Datsun','datsun',769,'/rossiya/cars/datsun/all/?listing=listing'),
(614,'De Tomaso','de_tomaso',1,'/rossiya/cars/de_tomaso/all/?listing=listing'),
(615,'DeLorean','delorean',1,'/rossiya/cars/delorean/all/?listing=listing'),
(616,'Derways','derways',26,'/rossiya/cars/derways/all/?listing=listing'),
(617,'DeSoto','desoto',1,'/rossiya/cars/desoto/all/?listing=listing'),
(618,'Dodge','dodge',1328,'/rossiya/cars/dodge/all/?listing=listing'),
(619,'DongFeng','dongfeng',24,'/rossiya/cars/dongfeng/all/?listing=listing'),
(620,'Doninvest','doninvest',10,'/rossiya/cars/doninvest/all/?listing=listing'),
(621,'Donkervoort','donkervoort',0,'/rossiya/cars/donkervoort/all/?listing=listing'),
(622,'DS','ds',0,'/rossiya/cars/ds/all/?listing=listing'),
(623,'E-Car','e_car',0,'/rossiya/cars/e_car/all/?listing=listing'),
(624,'Eagle','eagle',3,'/rossiya/cars/eagle/all/?listing=listing'),
(625,'Eagle Cars','eagle_cars',0,'/rossiya/cars/eagle_cars/all/?listing=listing'),
(626,'Ecomotors','ecomotors',0,'/rossiya/cars/ecomotors/all/?listing=listing'),
(627,'FAW','faw',129,'/rossiya/cars/faw/all/?listing=listing'),
(629,'Fiat','fiat',951,'/rossiya/cars/fiat/all/?listing=listing'),
(630,'Fisker','fisker',1,'/rossiya/cars/fisker/all/?listing=listing'),
(632,'Foton','foton',6,'/rossiya/cars/foton/all/?listing=listing'),
(633,'FSO','fso',0,'/rossiya/cars/fso/all/?listing=listing'),
(634,'Fuqi','fuqi',0,'/rossiya/cars/fuqi/all/?listing=listing'),
(635,'Geely','geely',728,'/rossiya/cars/geely/all/?listing=listing'),
(636,'Geo','geo',2,'/rossiya/cars/geo/all/?listing=listing'),
(637,'GMC','gmc',128,'/rossiya/cars/gmc/all/?listing=listing'),
(638,'Gonow','gonow',0,'/rossiya/cars/gonow/all/?listing=listing'),
(639,'Gordon','gordon',0,'/rossiya/cars/gordon/all/?listing=listing'),
(640,'Great Wall','great_wall',1010,'/rossiya/cars/great_wall/all/?listing=listing'),
(641,'Hafei','hafei',27,'/rossiya/cars/hafei/all/?listing=listing'),
(642,'Haima','haima',59,'/rossiya/cars/haima/all/?listing=listing'),
(643,'Haval','haval',26,'/rossiya/cars/haval/all/?listing=listing'),
(644,'Hawtai','hawtai',5,'/rossiya/cars/hawtai/all/?listing=listing'),
(645,'Hindustan','hindustan',0,'/rossiya/cars/hindustan/all/?listing=listing'),
(646,'Holden','holden',0,'/rossiya/cars/holden/all/?listing=listing'),
(648,'HuangHai','huanghai',2,'/rossiya/cars/huanghai/all/?listing=listing'),
(649,'Hudson','hudson',1,'/rossiya/cars/hudson/all/?listing=listing'),
(650,'Hummer','hummer',217,'/rossiya/cars/hummer/all/?listing=listing'),
(653,'Innocenti','innocenti',0,'/rossiya/cars/innocenti/all/?listing=listing'),
(654,'Invicta','invicta',0,'/rossiya/cars/invicta/all/?listing=listing'),
(655,'Iran Khodro','iran_khodro',93,'/rossiya/cars/iran_khodro/all/?listing=listing'),
(656,'Isdera','isdera',0,'/rossiya/cars/isdera/all/?listing=listing'),
(657,'Isuzu','isuzu',83,'/rossiya/cars/isuzu/all/?listing=listing'),
(658,'JAC','jac',26,'/rossiya/cars/jac/all/?listing=listing'),
(659,'Jaguar','jaguar',1020,'/rossiya/cars/jaguar/all/?listing=listing'),
(660,'Jeep','jeep',1371,'/rossiya/cars/jeep/all/?listing=listing'),
(661,'Jensen','jensen',0,'/rossiya/cars/jensen/all/?listing=listing'),
(662,'JMC','jmc',14,'/rossiya/cars/jmc/all/?listing=listing'),
(664,'Koenigsegg','koenigsegg',0,'/rossiya/cars/koenigsegg/all/?listing=listing'),
(665,'KTM','ktm',0,'/rossiya/cars/ktm/all/?listing=listing'),
(667,'Lancia','lancia',40,'/rossiya/cars/lancia/all/?listing=listing'),
(669,'Landwind','landwind',1,'/rossiya/cars/landwind/all/?listing=listing'),
(671,'Liebao Motor','liebao',0,'/rossiya/cars/liebao/all/?listing=listing'),
(672,'Lifan','lifan',1122,'/rossiya/cars/lifan/all/?listing=listing'),
(673,'Lincoln','lincoln',200,'/rossiya/cars/lincoln/all/?listing=listing'),
(674,'Lotus','lotus',5,'/rossiya/cars/lotus/all/?listing=listing'),
(675,'LTI','lti',1,'/rossiya/cars/lti/all/?listing=listing'),
(676,'Luxgen','luxgen',17,'/rossiya/cars/luxgen/all/?listing=listing'),
(677,'Mahindra','mahindra',0,'/rossiya/cars/mahindra/all/?listing=listing'),
(678,'Marcos','marcos',1,'/rossiya/cars/marcos/all/?listing=listing'),
(679,'Marlin','marlin',0,'/rossiya/cars/marlin/all/?listing=listing'),
(680,'Marussia','marussia',0,'/rossiya/cars/marussia/all/?listing=listing'),
(681,'Maruti','maruti',0,'/rossiya/cars/maruti/all/?listing=listing'),
(682,'Maserati','maserati',88,'/rossiya/cars/maserati/all/?listing=listing'),
(683,'Maybach','maybach',20,'/rossiya/cars/maybach/all/?listing=listing'),
(685,'McLaren','mclaren',1,'/rossiya/cars/mclaren/all/?listing=listing'),
(686,'Mega','mega',0,'/rossiya/cars/mega/all/?listing=listing'),
(688,'Mercury','mercury',29,'/rossiya/cars/mercury/all/?listing=listing'),
(689,'Metrocab','metrocab',1,'/rossiya/cars/metrocab/all/?listing=listing'),
(690,'MG','mg',18,'/rossiya/cars/mg/all/?listing=listing'),
(691,'Microcar','microcar',0,'/rossiya/cars/microcar/all/?listing=listing'),
(692,'Minelli','minelli',0,'/rossiya/cars/minelli/all/?listing=listing'),
(695,'Mitsuoka','mitsuoka',2,'/rossiya/cars/mitsuoka/all/?listing=listing'),
(696,'Morgan','morgan',2,'/rossiya/cars/morgan/all/?listing=listing'),
(697,'Morris','morris',1,'/rossiya/cars/morris/all/?listing=listing'),
(699,'Noble','noble',0,'/rossiya/cars/noble/all/?listing=listing'),
(700,'Oldsmobile','oldsmobile',10,'/rossiya/cars/oldsmobile/all/?listing=listing'),
(702,'Osca','osca',0,'/rossiya/cars/osca/all/?listing=listing'),
(703,'Packard','packard',0,'/rossiya/cars/packard/all/?listing=listing'),
(704,'Pagani','pagani',0,'/rossiya/cars/pagani/all/?listing=listing'),
(705,'Panoz','panoz',0,'/rossiya/cars/panoz/all/?listing=listing'),
(706,'Perodua','perodua',0,'/rossiya/cars/perodua/all/?listing=listing'),
(708,'PGO','pgo',2,'/rossiya/cars/pgo/all/?listing=listing'),
(709,'Piaggio','piaggio',0,'/rossiya/cars/piaggio/all/?listing=listing'),
(710,'Plymouth','plymouth',19,'/rossiya/cars/plymouth/all/?listing=listing'),
(711,'Pontiac','pontiac',125,'/rossiya/cars/pontiac/all/?listing=listing'),
(712,'Porsche','porsche',1774,'/rossiya/cars/porsche/all/?listing=listing'),
(713,'Premier','premier',0,'/rossiya/cars/premier/all/?listing=listing'),
(714,'Proton','proton',6,'/rossiya/cars/proton/all/?listing=listing'),
(715,'PUCH','puch',7,'/rossiya/cars/puch/all/?listing=listing'),
(716,'Puma','puma',0,'/rossiya/cars/puma/all/?listing=listing'),
(717,'Qoros','qoros',0,'/rossiya/cars/qoros/all/?listing=listing'),
(718,'Qvale','qvale',0,'/rossiya/cars/qvale/all/?listing=listing'),
(719,'Ravon','ravon',146,'/rossiya/cars/ravon/all/?listing=listing'),
(720,'Reliant','reliant',0,'/rossiya/cars/reliant/all/?listing=listing'),
(721,'Renaissance','renaissance_cars',0,'/rossiya/cars/renaissance_cars/all/?listing=listing'),
(723,'Renault Samsung','samsung',0,'/rossiya/cars/samsung/all/?listing=listing'),
(724,'Rezvani','rezvani',0,'/rossiya/cars/rezvani/all/?listing=listing'),
(725,'Rimac','rimac',0,'/rossiya/cars/rimac/all/?listing=listing'),
(727,'Ronart','ronart',0,'/rossiya/cars/ronart/all/?listing=listing'),
(728,'Rover','rover',206,'/rossiya/cars/rover/all/?listing=listing'),
(729,'Saab','saab',371,'/rossiya/cars/saab/all/?listing=listing'),
(730,'Saleen','saleen',0,'/rossiya/cars/saleen/all/?listing=listing'),
(731,'Santana','santana',0,'/rossiya/cars/santana/all/?listing=listing'),
(732,'Saturn','saturn',30,'/rossiya/cars/saturn/all/?listing=listing'),
(733,'Scion','scion',26,'/rossiya/cars/scion/all/?listing=listing'),
(734,'SEAT','seat',277,'/rossiya/cars/seat/all/?listing=listing'),
(735,'Shanghai Maple','shanghai_maple',1,'/rossiya/cars/shanghai_maple/all/?listing=listing'),
(736,'ShuangHuan','shuanghuan',2,'/rossiya/cars/shuanghuan/all/?listing=listing'),
(738,'Smart','smart',230,'/rossiya/cars/smart/all/?listing=listing'),
(739,'Soueast','soueast',0,'/rossiya/cars/soueast/all/?listing=listing'),
(740,'Spectre','spectre',0,'/rossiya/cars/spectre/all/?listing=listing'),
(741,'Spyker','spyker',1,'/rossiya/cars/spyker/all/?listing=listing'),
(745,'Talbot','talbot',1,'/rossiya/cars/talbot/all/?listing=listing'),
(746,'TATA','tata',3,'/rossiya/cars/tata/all/?listing=listing'),
(747,'Tatra','tatra',4,'/rossiya/cars/tatra/all/?listing=listing'),
(748,'Tazzari','tazzari',0,'/rossiya/cars/tazzari/all/?listing=listing'),
(750,'Tianma','tianma',2,'/rossiya/cars/tianma/all/?listing=listing'),
(751,'Tianye','tianye',9,'/rossiya/cars/tianye/all/?listing=listing'),
(752,'Tofas','tofas',0,'/rossiya/cars/tofas/all/?listing=listing'),
(754,'Trabant','trabant',0,'/rossiya/cars/trabant/all/?listing=listing'),
(755,'Tramontana','tramontana',0,'/rossiya/cars/tramontana/all/?listing=listing'),
(756,'Triumph','triumph',1,'/rossiya/cars/triumph/all/?listing=listing'),
(757,'TVR','tvr',0,'/rossiya/cars/tvr/all/?listing=listing'),
(758,'Ultima','ultima',1,'/rossiya/cars/ultima/all/?listing=listing'),
(759,'Vauxhall','vauxhall',0,'/rossiya/cars/vauxhall/all/?listing=listing'),
(760,'Vector','vector',0,'/rossiya/cars/vector/all/?listing=listing'),
(761,'Venturi','venturi',0,'/rossiya/cars/venturi/all/?listing=listing'),
(764,'Vortex','vortex',308,'/rossiya/cars/vortex/all/?listing=listing'),
(765,'W Motors','w_motors',0,'/rossiya/cars/w_motors/all/?listing=listing'),
(766,'Wartburg','wartburg',0,'/rossiya/cars/wartburg/all/?listing=listing'),
(767,'Westfield','westfield',0,'/rossiya/cars/westfield/all/?listing=listing'),
(768,'Wiesmann','wiesmann',2,'/rossiya/cars/wiesmann/all/?listing=listing'),
(769,'Willys','willis',4,'/rossiya/cars/willis/all/?listing=listing'),
(770,'Xin Kai','xinkai',6,'/rossiya/cars/xinkai/all/?listing=listing'),
(771,'Zastava','zastava',0,'/rossiya/cars/zastava/all/?listing=listing'),
(772,'Zenos','zenos',0,'/rossiya/cars/zenos/all/?listing=listing'),
(773,'Zenvo','zenvo',0,'/rossiya/cars/zenvo/all/?listing=listing'),
(774,'Zotye','zotye',13,'/rossiya/cars/zotye/all/?listing=listing'),
(775,'ZX','zx',15,'/rossiya/cars/zx/all/?listing=listing'),
(776,'Автокам','avtokam',1,'/rossiya/cars/avtokam/all/?listing=listing'),
(777,'Бронто','bronto',7,'/rossiya/cars/bronto/all/?listing=listing'),
(780,'ЗАЗ','zaz',790,'/rossiya/cars/zaz/all/?listing=listing'),
(781,'ЗИЛ','zil',10,'/rossiya/cars/zil/all/?listing=listing'),
(782,'ЗиС','zis',1,'/rossiya/cars/zis/all/?listing=listing'),
(783,'ИЖ','ig',460,'/rossiya/cars/ig/all/?listing=listing'),
(784,'Канонир','kanonir',0,'/rossiya/cars/kanonir/all/?listing=listing'),
(785,'Комбат','kombat',2,'/rossiya/cars/kombat/all/?listing=listing'),
(786,'ЛуАЗ','luaz',102,'/rossiya/cars/luaz/all/?listing=listing'),
(787,'Москвич','moscvich',495,'/rossiya/cars/moscvich/all/?listing=listing'),
(788,'СМЗ','smz',10,'/rossiya/cars/smz/all/?listing=listing'),
(789,'ТагАЗ','tagaz',202,'/rossiya/cars/tagaz/all/?listing=listing'),
(791,'Эксклюзив','exclusive',38,'/rossiya/cars/exclusive/all/?listing=listing'),
(792,'Ё-мобиль','e_mobil',1,'/rossiya/cars/e_mobil/all/?listing=listing');

/*Table structure for table `cme_model` */

DROP TABLE IF EXISTS `cme_model`;

CREATE TABLE `cme_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `url` varchar(1000) NOT NULL,
  `brand_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cme_model_1` (`brand_id`),
  CONSTRAINT `FK_cme_model_1` FOREIGN KEY (`brand_id`) REFERENCES `cme_brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8807 DEFAULT CHARSET=utf8;

/*Data for the table `cme_model` */

insert  into `cme_model`(`id`,`title`,`slug`,`count`,`url`,`brand_id`) values 
(5280,'2107','2107',3568,'/rossiya/cars/vaz/2107/all/?listing=listing',529),
(5281,'2109','2109',2940,'/rossiya/cars/vaz/2109/all/?listing=listing',529),
(5282,'21099','21099',2622,'/rossiya/cars/vaz/21099/all/?listing=listing',529),
(5283,'2110','2110',4571,'/rossiya/cars/vaz/2110/all/?listing=listing',529),
(5284,'2112','2112',3318,'/rossiya/cars/vaz/2112/all/?listing=listing',529),
(5285,'2114','2114',5117,'/rossiya/cars/vaz/2114/all/?listing=listing',529),
(5286,'2115','2115',3314,'/rossiya/cars/vaz/2115/all/?listing=listing',529),
(5287,'2121 (4x4)','2121',3566,'/rossiya/cars/vaz/2121/all/?listing=listing',529),
(5288,'Granta','granta',2751,'/rossiya/cars/vaz/granta/all/?listing=listing',529),
(5289,'Kalina','kalina',3560,'/rossiya/cars/vaz/kalina/all/?listing=listing',529),
(5290,'Priora','2170',4666,'/rossiya/cars/vaz/2170/all/?listing=listing',529),
(5291,'1111 Ока','1111',584,'/rossiya/cars/vaz/1111/all/?listing=listing',529),
(5292,'2101','2101',511,'/rossiya/cars/vaz/2101/all/?listing=listing',529),
(5293,'2102','2102',61,'/rossiya/cars/vaz/2102/all/?listing=listing',529),
(5294,'2103','2103',108,'/rossiya/cars/vaz/2103/all/?listing=listing',529),
(5295,'2104','2104',873,'/rossiya/cars/vaz/2104/all/?listing=listing',529),
(5296,'2105','2105',1320,'/rossiya/cars/vaz/2105/all/?listing=listing',529),
(5297,'2106','2106',1541,'/rossiya/cars/vaz/2106/all/?listing=listing',529),
(5299,'2108','2108',1065,'/rossiya/cars/vaz/2108/all/?listing=listing',529),
(5303,'2111','2111',955,'/rossiya/cars/vaz/2111/all/?listing=listing',529),
(5305,'2113','2113',911,'/rossiya/cars/vaz/2113/all/?listing=listing',529),
(5308,'2120 Надежда','2120',33,'/rossiya/cars/vaz/2120/all/?listing=listing',529),
(5310,'2123','2123',50,'/rossiya/cars/vaz/2123/all/?listing=listing',529),
(5311,'2129','2129',2,'/rossiya/cars/vaz/2129/all/?listing=listing',529),
(5312,'2131 (4x4)','2131_4x4',639,'/rossiya/cars/vaz/2131_4x4/all/?listing=listing',529),
(5313,'2328','2328',0,'/rossiya/cars/vaz/2328/all/?listing=listing',529),
(5314,'2329','2329',29,'/rossiya/cars/vaz/2329/all/?listing=listing',529),
(5315,'EL Lada','ellada',1,'/rossiya/cars/vaz/ellada/all/?listing=listing',529),
(5318,'Largus','largus',582,'/rossiya/cars/vaz/largus/all/?listing=listing',529),
(5320,'Revolution','revolution',0,'/rossiya/cars/vaz/revolution/all/?listing=listing',529),
(5321,'Vesta','vesta',331,'/rossiya/cars/vaz/vesta/all/?listing=listing',529),
(5322,'XRAY','xray',243,'/rossiya/cars/vaz/xray/all/?listing=listing',529),
(5323,'100','100',717,'/rossiya/cars/audi/100/all/?listing=listing',530),
(5324,'80','80',882,'/rossiya/cars/audi/80/all/?listing=listing',530),
(5325,'A3','a3',803,'/rossiya/cars/audi/a3/all/?listing=listing',530),
(5326,'A4','a4',2264,'/rossiya/cars/audi/a4/all/?listing=listing',530),
(5327,'A5','a5',709,'/rossiya/cars/audi/a5/all/?listing=listing',530),
(5328,'A6','a6',2754,'/rossiya/cars/audi/a6/all/?listing=listing',530),
(5329,'A6 allroad','allroad',345,'/rossiya/cars/audi/allroad/all/?listing=listing',530),
(5330,'A8','a8',694,'/rossiya/cars/audi/a8/all/?listing=listing',530),
(5331,'Q3','q3',457,'/rossiya/cars/audi/q3/all/?listing=listing',530),
(5332,'Q5','q5',778,'/rossiya/cars/audi/q5/all/?listing=listing',530),
(5333,'Q7','q7',1052,'/rossiya/cars/audi/q7/all/?listing=listing',530),
(5335,'200','200',24,'/rossiya/cars/audi/200/all/?listing=listing',530),
(5336,'50','50',0,'/rossiya/cars/audi/50/all/?listing=listing',530),
(5338,'90','90',27,'/rossiya/cars/audi/90/all/?listing=listing',530),
(5339,'920','920',0,'/rossiya/cars/audi/920/all/?listing=listing',530),
(5340,'A1','a1',130,'/rossiya/cars/audi/a1/all/?listing=listing',530),
(5341,'A2','a2',16,'/rossiya/cars/audi/a2/all/?listing=listing',530),
(5344,'A4 allroad','a4_allroad',91,'/rossiya/cars/audi/a4_allroad/all/?listing=listing',530),
(5348,'A7','a7',260,'/rossiya/cars/audi/a7/all/?listing=listing',530),
(5350,'Cabriolet','cabriolet',6,'/rossiya/cars/audi/cabriolet/all/?listing=listing',530),
(5351,'Coupe','coupe',8,'/rossiya/cars/audi/coupe/all/?listing=listing',530),
(5352,'NSU RO 80','nsu_ro_80',0,'/rossiya/cars/audi/nsu_ro_80/all/?listing=listing',530),
(5353,'Q2','q2',0,'/rossiya/cars/audi/q2/all/?listing=listing',530),
(5357,'quattro','quattro',2,'/rossiya/cars/audi/quattro/all/?listing=listing',530),
(5358,'R8','r8',29,'/rossiya/cars/audi/r8/all/?listing=listing',530),
(5359,'RS Q3','rsq3',13,'/rossiya/cars/audi/rsq3/all/?listing=listing',530),
(5360,'RS2','rs2',0,'/rossiya/cars/audi/rs2/all/?listing=listing',530),
(5361,'RS3','rs3',4,'/rossiya/cars/audi/rs3/all/?listing=listing',530),
(5362,'RS4','rs4',12,'/rossiya/cars/audi/rs4/all/?listing=listing',530),
(5363,'RS5','rs5',10,'/rossiya/cars/audi/rs5/all/?listing=listing',530),
(5364,'RS6','rs6',29,'/rossiya/cars/audi/rs6/all/?listing=listing',530),
(5365,'RS7','rs7',26,'/rossiya/cars/audi/rs7/all/?listing=listing',530),
(5366,'S1','s1',0,'/rossiya/cars/audi/s1/all/?listing=listing',530),
(5367,'S2','s2',4,'/rossiya/cars/audi/s2/all/?listing=listing',530),
(5368,'S3','s3',14,'/rossiya/cars/audi/s3/all/?listing=listing',530),
(5369,'S4','s4',17,'/rossiya/cars/audi/s4/all/?listing=listing',530),
(5370,'S5','s5',33,'/rossiya/cars/audi/s5/all/?listing=listing',530),
(5371,'S6','s6',29,'/rossiya/cars/audi/s6/all/?listing=listing',530),
(5372,'S7','s7',2,'/rossiya/cars/audi/s7/all/?listing=listing',530),
(5373,'S8','s8',78,'/rossiya/cars/audi/s8/all/?listing=listing',530),
(5374,'SQ5','sq5',16,'/rossiya/cars/audi/sq5/all/?listing=listing',530),
(5375,'SQ7','sq7',0,'/rossiya/cars/audi/sq7/all/?listing=listing',530),
(5376,'TT','tt',217,'/rossiya/cars/audi/tt/all/?listing=listing',530),
(5377,'TT RS','tt_rs',3,'/rossiya/cars/audi/tt_rs/all/?listing=listing',530),
(5378,'TTS','tts',22,'/rossiya/cars/audi/tts/all/?listing=listing',530),
(5379,'Typ R','typ_r',0,'/rossiya/cars/audi/typ_r/all/?listing=listing',530),
(5380,'V8','v8',6,'/rossiya/cars/audi/v8/all/?listing=listing',530),
(5381,'Arnage','arnage',14,'/rossiya/cars/bentley/arnage/all/?listing=listing',531),
(5382,'Azure','azure',1,'/rossiya/cars/bentley/azure/all/?listing=listing',531),
(5383,'Bentayga','bentayga',17,'/rossiya/cars/bentley/bentayga/all/?listing=listing',531),
(5384,'Brooklands','brooklands',4,'/rossiya/cars/bentley/brooklands/all/?listing=listing',531),
(5385,'Continental','continental',2,'/rossiya/cars/bentley/continental/all/?listing=listing',531),
(5386,'Continental Flying Spur','continental_flying_spur',30,'/rossiya/cars/bentley/continental_flying_spur/all/?listing=listing',531),
(5387,'Continental GT','continental_gt',139,'/rossiya/cars/bentley/continental_gt/all/?listing=listing',531),
(5388,'Flying Spur','flying_spur',19,'/rossiya/cars/bentley/flying_spur/all/?listing=listing',531),
(5389,'Mulsanne','mulsanne',23,'/rossiya/cars/bentley/mulsanne/all/?listing=listing',531),
(5390,'S','s',2,'/rossiya/cars/bentley/s/all/?listing=listing',531),
(5391,'Turbo R','turbo_r',4,'/rossiya/cars/bentley/turbo_r/all/?listing=listing',531),
(5399,'Eight','eight',0,'/rossiya/cars/bentley/eight/all/?listing=listing',531),
(5401,'Mark VI','mark_vi',0,'/rossiya/cars/bentley/mark_vi/all/?listing=listing',531),
(5403,'R Type','r_type',0,'/rossiya/cars/bentley/r_type/all/?listing=listing',531),
(5405,'T-Series','t_series',0,'/rossiya/cars/bentley/t_series/all/?listing=listing',531),
(5407,'1er','1er',721,'/rossiya/cars/bmw/1er/all/?listing=listing',532),
(5408,'3er','3er',3263,'/rossiya/cars/bmw/3er/all/?listing=listing',532),
(5409,'4er','4',120,'/rossiya/cars/bmw/4/all/?listing=listing',532),
(5410,'5er','5er',4007,'/rossiya/cars/bmw/5er/all/?listing=listing',532),
(5411,'6er','6er',356,'/rossiya/cars/bmw/6er/all/?listing=listing',532),
(5412,'7er','7er',1188,'/rossiya/cars/bmw/7er/all/?listing=listing',532),
(5413,'X1','x1',559,'/rossiya/cars/bmw/x1/all/?listing=listing',532),
(5414,'X3','x3',1105,'/rossiya/cars/bmw/x3/all/?listing=listing',532),
(5415,'X5','x5',2718,'/rossiya/cars/bmw/x5/all/?listing=listing',532),
(5416,'X6','x6',1133,'/rossiya/cars/bmw/x6/all/?listing=listing',532),
(5417,'X6 M','x6_m',170,'/rossiya/cars/bmw/x6_m/all/?listing=listing',532),
(5418,'02 (E10)','02',0,'/rossiya/cars/bmw/02/all/?listing=listing',532),
(5420,'1M','m1',5,'/rossiya/cars/bmw/m1/all/?listing=listing',532),
(5421,'2000 C/CS','2000_c_cs',0,'/rossiya/cars/bmw/2000_c_cs/all/?listing=listing',532),
(5422,'2er','2er',12,'/rossiya/cars/bmw/2er/all/?listing=listing',532),
(5423,'2er Active Tourer','2activetourer',0,'/rossiya/cars/bmw/2activetourer/all/?listing=listing',532),
(5424,'2er Grand Tourer','2grandtourer',0,'/rossiya/cars/bmw/2grandtourer/all/?listing=listing',532),
(5425,'3/15','3_15',0,'/rossiya/cars/bmw/3_15/all/?listing=listing',532),
(5426,'315','315',0,'/rossiya/cars/bmw/315/all/?listing=listing',532),
(5427,'3200','3200',0,'/rossiya/cars/bmw/3200/all/?listing=listing',532),
(5428,'321','321',2,'/rossiya/cars/bmw/321/all/?listing=listing',532),
(5429,'326','326',0,'/rossiya/cars/bmw/326/all/?listing=listing',532),
(5430,'327','327',0,'/rossiya/cars/bmw/327/all/?listing=listing',532),
(5431,'340','340',0,'/rossiya/cars/bmw/340/all/?listing=listing',532),
(5434,'501','501',0,'/rossiya/cars/bmw/501/all/?listing=listing',532),
(5435,'502','502',0,'/rossiya/cars/bmw/502/all/?listing=listing',532),
(5436,'503','503',0,'/rossiya/cars/bmw/503/all/?listing=listing',532),
(5437,'507','507',0,'/rossiya/cars/bmw/507/all/?listing=listing',532),
(5439,'600','600',0,'/rossiya/cars/bmw/600/all/?listing=listing',532),
(5441,'700','700',0,'/rossiya/cars/bmw/700/all/?listing=listing',532),
(5443,'8er','8er',7,'/rossiya/cars/bmw/8er/all/?listing=listing',532),
(5444,'E3','e3',0,'/rossiya/cars/bmw/e3/all/?listing=listing',532),
(5445,'E9','e9',0,'/rossiya/cars/bmw/e9/all/?listing=listing',532),
(5446,'i3','i3',0,'/rossiya/cars/bmw/i3/all/?listing=listing',532),
(5447,'i8','i8',4,'/rossiya/cars/bmw/i8/all/?listing=listing',532),
(5448,'M2','m2',1,'/rossiya/cars/bmw/m2/all/?listing=listing',532),
(5449,'M3','m3',56,'/rossiya/cars/bmw/m3/all/?listing=listing',532),
(5450,'M4','m4',13,'/rossiya/cars/bmw/m4/all/?listing=listing',532),
(5451,'M5','m5',73,'/rossiya/cars/bmw/m5/all/?listing=listing',532),
(5452,'M6','m6',32,'/rossiya/cars/bmw/m6/all/?listing=listing',532),
(5453,'New Class','new_class',0,'/rossiya/cars/bmw/new_class/all/?listing=listing',532),
(5456,'X4','x4',73,'/rossiya/cars/bmw/x4/all/?listing=listing',532),
(5458,'X5 M','x5_m',82,'/rossiya/cars/bmw/x5_m/all/?listing=listing',532),
(5461,'Z1','z1',1,'/rossiya/cars/bmw/z1/all/?listing=listing',532),
(5462,'Z3','z3',10,'/rossiya/cars/bmw/z3/all/?listing=listing',532),
(5463,'Z3 M','z3m',2,'/rossiya/cars/bmw/z3m/all/?listing=listing',532),
(5464,'Z4','z4',82,'/rossiya/cars/bmw/z4/all/?listing=listing',532),
(5465,'Z4 M','z4_m',2,'/rossiya/cars/bmw/z4_m/all/?listing=listing',532),
(5466,'Z8','z8',1,'/rossiya/cars/bmw/z8/all/?listing=listing',532),
(5467,'Amulet (A15)','amulet',304,'/rossiya/cars/chery/amulet/all/?listing=listing',533),
(5468,'Bonus (A13)','bonus',113,'/rossiya/cars/chery/bonus/all/?listing=listing',533),
(5469,'Fora (A21)','fora',194,'/rossiya/cars/chery/fora/all/?listing=listing',533),
(5470,'IndiS (S18D)','indis',104,'/rossiya/cars/chery/indis/all/?listing=listing',533),
(5471,'Kimo (A1)','kimo',102,'/rossiya/cars/chery/kimo/all/?listing=listing',533),
(5472,'M11 (A3)','m11',81,'/rossiya/cars/chery/m11/all/?listing=listing',533),
(5473,'QQ6 (S21)','qq6',75,'/rossiya/cars/chery/qq6/all/?listing=listing',533),
(5474,'Sweet (QQ)','qq',44,'/rossiya/cars/chery/qq/all/?listing=listing',533),
(5475,'Tiggo (T11)','tiggo',618,'/rossiya/cars/chery/tiggo/all/?listing=listing',533),
(5476,'Tiggo 5','tiggo_5',44,'/rossiya/cars/chery/tiggo_5/all/?listing=listing',533),
(5477,'Very (A13)','very',69,'/rossiya/cars/chery/very/all/?listing=listing',533),
(5479,'Arrizo 7','arrizo7',7,'/rossiya/cars/chery/arrizo7/all/?listing=listing',533),
(5481,'Bonus 3 (E3/A19)','bonus_3',13,'/rossiya/cars/chery/bonus_3/all/?listing=listing',533),
(5482,'CrossEastar (B14)','cross_eastar',29,'/rossiya/cars/chery/cross_eastar/all/?listing=listing',533),
(5487,'Oriental Son (B11)','oriental_son',1,'/rossiya/cars/chery/oriental_son/all/?listing=listing',533),
(5493,'Aveo','aveo',1645,'/rossiya/cars/chevrolet/aveo/all/?listing=listing',534),
(5494,'Captiva','captiva',472,'/rossiya/cars/chevrolet/captiva/all/?listing=listing',534),
(5495,'Cobalt','cobalt',168,'/rossiya/cars/chevrolet/cobalt/all/?listing=listing',534),
(5496,'Cruze','cruze',2029,'/rossiya/cars/chevrolet/cruze/all/?listing=listing',534),
(5497,'Epica','epica',275,'/rossiya/cars/chevrolet/epica/all/?listing=listing',534),
(5498,'Lacetti','lacetti',1893,'/rossiya/cars/chevrolet/lacetti/all/?listing=listing',534),
(5499,'Lanos','lanos',1090,'/rossiya/cars/chevrolet/lanos/all/?listing=listing',534),
(5500,'Niva','niva',3002,'/rossiya/cars/chevrolet/niva/all/?listing=listing',534),
(5501,'Spark','spark',303,'/rossiya/cars/chevrolet/spark/all/?listing=listing',534),
(5502,'Tahoe','tahoe',237,'/rossiya/cars/chevrolet/tahoe/all/?listing=listing',534),
(5503,'TrailBlazer','trailblazer',167,'/rossiya/cars/chevrolet/trailblazer/all/?listing=listing',534),
(5504,'Alero','alero',1,'/rossiya/cars/chevrolet/alero/all/?listing=listing',534),
(5505,'Astra','astra',0,'/rossiya/cars/chevrolet/astra/all/?listing=listing',534),
(5506,'Astro','astro',8,'/rossiya/cars/chevrolet/astro/all/?listing=listing',534),
(5507,'Avalanche','avalanche',11,'/rossiya/cars/chevrolet/avalanche/all/?listing=listing',534),
(5509,'Bel Air','bel_air',1,'/rossiya/cars/chevrolet/bel_air/all/?listing=listing',534),
(5510,'Beretta','beretta',1,'/rossiya/cars/chevrolet/beretta/all/?listing=listing',534),
(5511,'Blazer','blazer',91,'/rossiya/cars/chevrolet/blazer/all/?listing=listing',534),
(5512,'Blazer K5','k5_blazer',2,'/rossiya/cars/chevrolet/k5_blazer/all/?listing=listing',534),
(5513,'Bolt','bolt',0,'/rossiya/cars/chevrolet/bolt/all/?listing=listing',534),
(5514,'C-10','c_10',1,'/rossiya/cars/chevrolet/c_10/all/?listing=listing',534),
(5515,'C/K','ck',0,'/rossiya/cars/chevrolet/ck/all/?listing=listing',534),
(5516,'Camaro','camaro',136,'/rossiya/cars/chevrolet/camaro/all/?listing=listing',534),
(5517,'Caprice','caprice',19,'/rossiya/cars/chevrolet/caprice/all/?listing=listing',534),
(5519,'Cavalier','cavalier',7,'/rossiya/cars/chevrolet/cavalier/all/?listing=listing',534),
(5520,'Celebrity','celebrity',0,'/rossiya/cars/chevrolet/celebrity/all/?listing=listing',534),
(5521,'Celta','celta',0,'/rossiya/cars/chevrolet/celta/all/?listing=listing',534),
(5522,'Chevelle','chevelle',0,'/rossiya/cars/chevrolet/chevelle/all/?listing=listing',534),
(5523,'Chevette','chevette',2,'/rossiya/cars/chevrolet/chevette/all/?listing=listing',534),
(5524,'Citation','citation',0,'/rossiya/cars/chevrolet/citation/all/?listing=listing',534),
(5525,'Classic','classic',0,'/rossiya/cars/chevrolet/classic/all/?listing=listing',534),
(5527,'Colorado','colorado',6,'/rossiya/cars/chevrolet/colorado/all/?listing=listing',534),
(5528,'Corsa','corsa',0,'/rossiya/cars/chevrolet/corsa/all/?listing=listing',534),
(5529,'Corsica','corsica',2,'/rossiya/cars/chevrolet/corsica/all/?listing=listing',534),
(5530,'Corvette','corvette',48,'/rossiya/cars/chevrolet/corvette/all/?listing=listing',534),
(5532,'Cruze (HR)','cruze_hr',2,'/rossiya/cars/chevrolet/cruze_hr/all/?listing=listing',534),
(5533,'Deluxe','deluxe',0,'/rossiya/cars/chevrolet/deluxe/all/?listing=listing',534),
(5534,'El Camino','el_camino',1,'/rossiya/cars/chevrolet/el_camino/all/?listing=listing',534),
(5536,'Equinox','equinox',8,'/rossiya/cars/chevrolet/equinox/all/?listing=listing',534),
(5537,'Evanda','evanda',36,'/rossiya/cars/chevrolet/evanda/all/?listing=listing',534),
(5538,'Express','express',91,'/rossiya/cars/chevrolet/express/all/?listing=listing',534),
(5539,'Fleetmaster','fleetmaster',0,'/rossiya/cars/chevrolet/fleetmaster/all/?listing=listing',534),
(5540,'HHR','hhr',4,'/rossiya/cars/chevrolet/hhr/all/?listing=listing',534),
(5541,'Impala','impala',4,'/rossiya/cars/chevrolet/impala/all/?listing=listing',534),
(5542,'Kalos','kalos',2,'/rossiya/cars/chevrolet/kalos/all/?listing=listing',534),
(5545,'Lumina','lumina',9,'/rossiya/cars/chevrolet/lumina/all/?listing=listing',534),
(5546,'Lumina APV','lumina_apv',9,'/rossiya/cars/chevrolet/lumina_apv/all/?listing=listing',534),
(5547,'LUV D-MAX','luv_dmax',0,'/rossiya/cars/chevrolet/luv_dmax/all/?listing=listing',534),
(5548,'Malibu','malibu',15,'/rossiya/cars/chevrolet/malibu/all/?listing=listing',534),
(5549,'Master','master',0,'/rossiya/cars/chevrolet/master/all/?listing=listing',534),
(5550,'Metro','metro',7,'/rossiya/cars/chevrolet/metro/all/?listing=listing',534),
(5551,'Monte Carlo','monte_carlo',0,'/rossiya/cars/chevrolet/monte_carlo/all/?listing=listing',534),
(5552,'Monza','monza',0,'/rossiya/cars/chevrolet/monza/all/?listing=listing',534),
(5553,'MW','mw',1,'/rossiya/cars/chevrolet/mw/all/?listing=listing',534),
(5555,'Nubira','nubira',0,'/rossiya/cars/chevrolet/nubira/all/?listing=listing',534),
(5556,'Omega','omega',0,'/rossiya/cars/chevrolet/omega/all/?listing=listing',534),
(5557,'Orlando','orlando',154,'/rossiya/cars/chevrolet/orlando/all/?listing=listing',534),
(5558,'Prizm','prizm',2,'/rossiya/cars/chevrolet/prizm/all/?listing=listing',534),
(5559,'Rezzo','rezzo',62,'/rossiya/cars/chevrolet/rezzo/all/?listing=listing',534),
(5560,'S-10 Pickup','s_10',1,'/rossiya/cars/chevrolet/s_10/all/?listing=listing',534),
(5561,'Sail','sail',0,'/rossiya/cars/chevrolet/sail/all/?listing=listing',534),
(5562,'Silverado','silverado',32,'/rossiya/cars/chevrolet/silverado/all/?listing=listing',534),
(5563,'Sonic','sonic',0,'/rossiya/cars/chevrolet/sonic/all/?listing=listing',534),
(5565,'Special DeLuxe','special_deluxe',0,'/rossiya/cars/chevrolet/special_deluxe/all/?listing=listing',534),
(5566,'SS','ss',3,'/rossiya/cars/chevrolet/ss/all/?listing=listing',534),
(5567,'SSR','ssr',3,'/rossiya/cars/chevrolet/ssr/all/?listing=listing',534),
(5568,'Starcraft','starcraft',0,'/rossiya/cars/chevrolet/starcraft/all/?listing=listing',534),
(5569,'Suburban','suburban',33,'/rossiya/cars/chevrolet/suburban/all/?listing=listing',534),
(5571,'Tavera','tavera',0,'/rossiya/cars/chevrolet/tavera/all/?listing=listing',534),
(5572,'Tracker','tracker',46,'/rossiya/cars/chevrolet/tracker/all/?listing=listing',534),
(5574,'Trans Sport','transsport',0,'/rossiya/cars/chevrolet/transsport/all/?listing=listing',534),
(5575,'Traverse','traverse',0,'/rossiya/cars/chevrolet/traverse/all/?listing=listing',534),
(5576,'Uplander','uplander',0,'/rossiya/cars/chevrolet/uplander/all/?listing=listing',534),
(5577,'Van','van',13,'/rossiya/cars/chevrolet/van/all/?listing=listing',534),
(5578,'Vectra','vectra',0,'/rossiya/cars/chevrolet/vectra/all/?listing=listing',534),
(5579,'Venture','venture',1,'/rossiya/cars/chevrolet/venture/all/?listing=listing',534),
(5580,'Viva','viva',43,'/rossiya/cars/chevrolet/viva/all/?listing=listing',534),
(5581,'Volt','volt',4,'/rossiya/cars/chevrolet/volt/all/?listing=listing',534),
(5582,'Zafira','zafira',0,'/rossiya/cars/chevrolet/zafira/all/?listing=listing',534),
(5583,'Berlingo','berlingo',377,'/rossiya/cars/citroen/berlingo/all/?listing=listing',535),
(5584,'C-Crosser','c-crosser',102,'/rossiya/cars/citroen/c-crosser/all/?listing=listing',535),
(5585,'C-Elysee','c-elysee',71,'/rossiya/cars/citroen/c-elysee/all/?listing=listing',535),
(5586,'C3','c3',244,'/rossiya/cars/citroen/c3/all/?listing=listing',535),
(5587,'C3 Picasso','c3_picasso',99,'/rossiya/cars/citroen/c3_picasso/all/?listing=listing',535),
(5588,'C4','c4',1331,'/rossiya/cars/citroen/c4/all/?listing=listing',535),
(5589,'C4 Picasso','c4_picasso',201,'/rossiya/cars/citroen/c4_picasso/all/?listing=listing',535),
(5590,'C5','c5',486,'/rossiya/cars/citroen/c5/all/?listing=listing',535),
(5591,'DS4','ds4',71,'/rossiya/cars/citroen/ds4/all/?listing=listing',535),
(5592,'Xsara','xsara',39,'/rossiya/cars/citroen/xsara/all/?listing=listing',535),
(5593,'Xsara Picasso','xsara_picasso',69,'/rossiya/cars/citroen/xsara_picasso/all/?listing=listing',535),
(5594,'2 CV','2cv',0,'/rossiya/cars/citroen/2cv/all/?listing=listing',535),
(5595,'AMI','ami',0,'/rossiya/cars/citroen/ami/all/?listing=listing',535),
(5596,'AX','ax',1,'/rossiya/cars/citroen/ax/all/?listing=listing',535),
(5598,'BX','bx',7,'/rossiya/cars/citroen/bx/all/?listing=listing',535),
(5601,'C-ZERO','c_zero',0,'/rossiya/cars/citroen/c_zero/all/?listing=listing',535),
(5602,'C1','c1',37,'/rossiya/cars/citroen/c1/all/?listing=listing',535),
(5603,'C2','c2',36,'/rossiya/cars/citroen/c2/all/?listing=listing',535),
(5607,'C4 Aircross','c4_aircross',24,'/rossiya/cars/citroen/c4_aircross/all/?listing=listing',535),
(5608,'C4 Cactus','c4_cactus',0,'/rossiya/cars/citroen/c4_cactus/all/?listing=listing',535),
(5611,'C6','c6',7,'/rossiya/cars/citroen/c6/all/?listing=listing',535),
(5612,'C8','c8',4,'/rossiya/cars/citroen/c8/all/?listing=listing',535),
(5613,'CX','cx',0,'/rossiya/cars/citroen/cx/all/?listing=listing',535),
(5614,'DS','ds',1,'/rossiya/cars/citroen/ds/all/?listing=listing',535),
(5615,'DS3','ds3',29,'/rossiya/cars/citroen/ds3/all/?listing=listing',535),
(5617,'DS5','ds5',33,'/rossiya/cars/citroen/ds5/all/?listing=listing',535),
(5618,'Dyane','dyane',0,'/rossiya/cars/citroen/dyane/all/?listing=listing',535),
(5619,'E-Mehari','e-mehari',0,'/rossiya/cars/citroen/e-mehari/all/?listing=listing',535),
(5620,'Evasion','evasion',2,'/rossiya/cars/citroen/evasion/all/?listing=listing',535),
(5621,'GS','gs',0,'/rossiya/cars/citroen/gs/all/?listing=listing',535),
(5622,'Jumpy','jumpy',33,'/rossiya/cars/citroen/jumpy/all/?listing=listing',535),
(5623,'LN','ln',0,'/rossiya/cars/citroen/ln/all/?listing=listing',535),
(5624,'Saxo','saxo',2,'/rossiya/cars/citroen/saxo/all/?listing=listing',535),
(5625,'SM','sm',0,'/rossiya/cars/citroen/sm/all/?listing=listing',535),
(5626,'Spacetourer','spacetourer',0,'/rossiya/cars/citroen/spacetourer/all/?listing=listing',535),
(5627,'Traction Avant','traction_avant',2,'/rossiya/cars/citroen/traction_avant/all/?listing=listing',535),
(5628,'Visa','visa',0,'/rossiya/cars/citroen/visa/all/?listing=listing',535),
(5629,'Xantia','xantia',14,'/rossiya/cars/citroen/xantia/all/?listing=listing',535),
(5630,'XM','xm',5,'/rossiya/cars/citroen/xm/all/?listing=listing',535),
(5633,'ZX','zx',1,'/rossiya/cars/citroen/zx/all/?listing=listing',535),
(5634,'Damas','damas',11,'/rossiya/cars/daewoo/damas/all/?listing=listing',536),
(5635,'Espero','espero',145,'/rossiya/cars/daewoo/espero/all/?listing=listing',536),
(5636,'Gentra','gentra',149,'/rossiya/cars/daewoo/gentra/all/?listing=listing',536),
(5638,'Leganza','leganza',82,'/rossiya/cars/daewoo/leganza/all/?listing=listing',536),
(5639,'Magnus','magnus',10,'/rossiya/cars/daewoo/magnus/all/?listing=listing',536),
(5640,'Matiz','matiz',2300,'/rossiya/cars/daewoo/matiz/all/?listing=listing',536),
(5641,'Nexia','nexia',3217,'/rossiya/cars/daewoo/nexia/all/?listing=listing',536),
(5643,'Sens','sens',19,'/rossiya/cars/daewoo/sens/all/?listing=listing',536),
(5644,'Tico','tico',12,'/rossiya/cars/daewoo/tico/all/?listing=listing',536),
(5645,'Arcadia','arcadia',0,'/rossiya/cars/daewoo/arcadia/all/?listing=listing',536),
(5646,'Chairman','chairman',0,'/rossiya/cars/daewoo/chairman/all/?listing=listing',536),
(5650,'G2X','g2x',0,'/rossiya/cars/daewoo/g2x/all/?listing=listing',536),
(5653,'Korando','korando',0,'/rossiya/cars/daewoo/korando/all/?listing=listing',536),
(5657,'LeMans','le_mans',0,'/rossiya/cars/daewoo/le_mans/all/?listing=listing',536),
(5660,'Musso','musso',0,'/rossiya/cars/daewoo/musso/all/?listing=listing',536),
(5663,'Prince','prince',2,'/rossiya/cars/daewoo/prince/all/?listing=listing',536),
(5664,'Racer','racer',1,'/rossiya/cars/daewoo/racer/all/?listing=listing',536),
(5667,'Tacuma','tacuma',4,'/rossiya/cars/daewoo/tacuma/all/?listing=listing',536),
(5669,'Tosca','tosca',0,'/rossiya/cars/daewoo/tosca/all/?listing=listing',536),
(5670,'Winstorm','windstorm',4,'/rossiya/cars/daewoo/windstorm/all/?listing=listing',536),
(5671,'208/308','208_308',1,'/rossiya/cars/ferrari/208_308/all/?listing=listing',537),
(5672,'360','360_modena',4,'/rossiya/cars/ferrari/360_modena/all/?listing=listing',537),
(5673,'458','458_italia',16,'/rossiya/cars/ferrari/458_italia/all/?listing=listing',537),
(5674,'488','488',6,'/rossiya/cars/ferrari/488/all/?listing=listing',537),
(5675,'599','599',7,'/rossiya/cars/ferrari/599/all/?listing=listing',537),
(5676,'612','612_scaglietti',3,'/rossiya/cars/ferrari/612_scaglietti/all/?listing=listing',537),
(5677,'California','california',16,'/rossiya/cars/ferrari/california/all/?listing=listing',537),
(5678,'F12berlinetta','f12berlinetta',4,'/rossiya/cars/ferrari/f12berlinetta/all/?listing=listing',537),
(5679,'F430','f430',8,'/rossiya/cars/ferrari/f430/all/?listing=listing',537),
(5680,'FF','ff',10,'/rossiya/cars/ferrari/ff/all/?listing=listing',537),
(5681,'Testarossa','testarossa',1,'/rossiya/cars/ferrari/testarossa/all/?listing=listing',537),
(5683,'328','328',0,'/rossiya/cars/ferrari/328/all/?listing=listing',537),
(5684,'348','348',0,'/rossiya/cars/ferrari/348/all/?listing=listing',537),
(5686,'400','400',0,'/rossiya/cars/ferrari/400/all/?listing=listing',537),
(5687,'412','412',0,'/rossiya/cars/ferrari/412/all/?listing=listing',537),
(5688,'456','456',0,'/rossiya/cars/ferrari/456/all/?listing=listing',537),
(5691,'512 BB','512_bb',0,'/rossiya/cars/ferrari/512_bb/all/?listing=listing',537),
(5692,'512 M','512m',0,'/rossiya/cars/ferrari/512m/all/?listing=listing',537),
(5693,'512 TR','512_tr',0,'/rossiya/cars/ferrari/512_tr/all/?listing=listing',537),
(5694,'550','550',0,'/rossiya/cars/ferrari/550/all/?listing=listing',537),
(5695,'575M','575_maranello',0,'/rossiya/cars/ferrari/575_maranello/all/?listing=listing',537),
(5699,'Enzo','enzo',0,'/rossiya/cars/ferrari/enzo/all/?listing=listing',537),
(5701,'F355','f355',0,'/rossiya/cars/ferrari/f355/all/?listing=listing',537),
(5702,'F40','f40',0,'/rossiya/cars/ferrari/f40/all/?listing=listing',537),
(5704,'F50','f50',0,'/rossiya/cars/ferrari/f50/all/?listing=listing',537),
(5706,'FXX K','fxx_k',0,'/rossiya/cars/ferrari/fxx_k/all/?listing=listing',537),
(5707,'GTC4Lusso','gtc4lusso',0,'/rossiya/cars/ferrari/gtc4lusso/all/?listing=listing',537),
(5708,'LaFerrari','laferrari',0,'/rossiya/cars/ferrari/laferrari/all/?listing=listing',537),
(5709,'Mondial','mondial',0,'/rossiya/cars/ferrari/mondial/all/?listing=listing',537),
(5711,'C-MAX','c_max',352,'/rossiya/cars/ford/c_max/all/?listing=listing',538),
(5712,'EcoSport','ecosport',161,'/rossiya/cars/ford/ecosport/all/?listing=listing',538),
(5713,'Escape','escape',213,'/rossiya/cars/ford/escape/all/?listing=listing',538),
(5714,'Explorer','explorer',422,'/rossiya/cars/ford/explorer/all/?listing=listing',538),
(5715,'Fiesta','fiesta',932,'/rossiya/cars/ford/fiesta/all/?listing=listing',538),
(5716,'Focus','focus',8499,'/rossiya/cars/ford/focus/all/?listing=listing',538),
(5717,'Fusion','fusion',736,'/rossiya/cars/ford/fusion/all/?listing=listing',538),
(5718,'Kuga','kuga',579,'/rossiya/cars/ford/kuga/all/?listing=listing',538),
(5719,'Mondeo','mondeo',2387,'/rossiya/cars/ford/mondeo/all/?listing=listing',538),
(5720,'Mustang','mustang',202,'/rossiya/cars/ford/mustang/all/?listing=listing',538),
(5721,'Tourneo Connect','tourneo_connect',158,'/rossiya/cars/ford/tourneo_connect/all/?listing=listing',538),
(5722,'Aerostar','aerostar',10,'/rossiya/cars/ford/aerostar/all/?listing=listing',538),
(5723,'Aspire','aspire',0,'/rossiya/cars/ford/aspire/all/?listing=listing',538),
(5724,'B-MAX','b_max',1,'/rossiya/cars/ford/b_max/all/?listing=listing',538),
(5725,'Bronco','bronco',6,'/rossiya/cars/ford/bronco/all/?listing=listing',538),
(5726,'Bronco-II','bronco_ii',2,'/rossiya/cars/ford/bronco_ii/all/?listing=listing',538),
(5728,'Capri','capri',1,'/rossiya/cars/ford/capri/all/?listing=listing',538),
(5729,'Consul','consul',0,'/rossiya/cars/ford/consul/all/?listing=listing',538),
(5730,'Contour','contour',9,'/rossiya/cars/ford/contour/all/?listing=listing',538),
(5731,'Cougar','cougar',8,'/rossiya/cars/ford/cougar/all/?listing=listing',538),
(5732,'Crown Victoria','crown_victoria',10,'/rossiya/cars/ford/crown_victoria/all/?listing=listing',538),
(5733,'Custom','custom',1,'/rossiya/cars/ford/custom/all/?listing=listing',538),
(5734,'Econoline','econoline',21,'/rossiya/cars/ford/econoline/all/?listing=listing',538),
(5736,'Edge','edge',23,'/rossiya/cars/ford/edge/all/?listing=listing',538),
(5738,'Escort','escort',125,'/rossiya/cars/ford/escort/all/?listing=listing',538),
(5739,'Escort (North America)','escort_na',2,'/rossiya/cars/ford/escort_na/all/?listing=listing',538),
(5740,'Everest','everest',0,'/rossiya/cars/ford/everest/all/?listing=listing',538),
(5741,'Excursion','excursion',13,'/rossiya/cars/ford/excursion/all/?listing=listing',538),
(5742,'Expedition','expedition',39,'/rossiya/cars/ford/expedition/all/?listing=listing',538),
(5744,'Explorer Sport Trac','explorer_sport_trac',3,'/rossiya/cars/ford/explorer_sport_trac/all/?listing=listing',538),
(5745,'F-150','f_150',132,'/rossiya/cars/ford/f_150/all/?listing=listing',538),
(5746,'Fairlane','fairlane',0,'/rossiya/cars/ford/fairlane/all/?listing=listing',538),
(5747,'Fairmont','fairmont',0,'/rossiya/cars/ford/fairmont/all/?listing=listing',538),
(5748,'Festiva','festiva',2,'/rossiya/cars/ford/festiva/all/?listing=listing',538),
(5750,'Fiesta ST','fiesta_st',7,'/rossiya/cars/ford/fiesta_st/all/?listing=listing',538),
(5751,'Five Hundred','five_hundred',0,'/rossiya/cars/ford/five_hundred/all/?listing=listing',538),
(5752,'Flex','flex',1,'/rossiya/cars/ford/flex/all/?listing=listing',538),
(5754,'Focus (North America)','focus_na',51,'/rossiya/cars/ford/focus_na/all/?listing=listing',538),
(5755,'Focus RS','focus_rs',5,'/rossiya/cars/ford/focus_rs/all/?listing=listing',538),
(5756,'Focus ST','focus_st',56,'/rossiya/cars/ford/focus_st/all/?listing=listing',538),
(5757,'Freestar','freestar',0,'/rossiya/cars/ford/freestar/all/?listing=listing',538),
(5758,'Freestyle','freestyle',5,'/rossiya/cars/ford/freestyle/all/?listing=listing',538),
(5760,'Fusion (North America)','fusion_na',1,'/rossiya/cars/ford/fusion_na/all/?listing=listing',538),
(5761,'Galaxie','galaxie',1,'/rossiya/cars/ford/galaxie/all/?listing=listing',538),
(5762,'Galaxy','galaxy',153,'/rossiya/cars/ford/galaxy/all/?listing=listing',538),
(5763,'GPA','gpa',0,'/rossiya/cars/ford/gpa/all/?listing=listing',538),
(5764,'Granada','granada',12,'/rossiya/cars/ford/granada/all/?listing=listing',538),
(5765,'Granada (North America)','granada_na',1,'/rossiya/cars/ford/granada_na/all/?listing=listing',538),
(5766,'GT','gt',0,'/rossiya/cars/ford/gt/all/?listing=listing',538),
(5767,'GT40','gt40',0,'/rossiya/cars/ford/gt40/all/?listing=listing',538),
(5768,'Ixion','ixion',2,'/rossiya/cars/ford/ixion/all/?listing=listing',538),
(5769,'KA','ka',41,'/rossiya/cars/ford/ka/all/?listing=listing',538),
(5771,'Laser','laser',2,'/rossiya/cars/ford/laser/all/?listing=listing',538),
(5772,'LTD Crown Victoria','ltd_crown_victoria',2,'/rossiya/cars/ford/ltd_crown_victoria/all/?listing=listing',538),
(5773,'M151','m151',0,'/rossiya/cars/ford/m151/all/?listing=listing',538),
(5774,'Maverick','maverick',111,'/rossiya/cars/ford/maverick/all/?listing=listing',538),
(5775,'Model A','model_a',1,'/rossiya/cars/ford/model_a/all/?listing=listing',538),
(5776,'Model T','model_t',0,'/rossiya/cars/ford/model_t/all/?listing=listing',538),
(5778,'Mondeo ST','mondeo_st',9,'/rossiya/cars/ford/mondeo_st/all/?listing=listing',538),
(5780,'Orion','orion',7,'/rossiya/cars/ford/orion/all/?listing=listing',538),
(5781,'Probe','probe',17,'/rossiya/cars/ford/probe/all/?listing=listing',538),
(5782,'Puma','puma',13,'/rossiya/cars/ford/puma/all/?listing=listing',538),
(5783,'Ranchero','ranchero',1,'/rossiya/cars/ford/ranchero/all/?listing=listing',538),
(5784,'Ranger','ranger',116,'/rossiya/cars/ford/ranger/all/?listing=listing',538),
(5785,'Ranger (North America)','ranger_na',2,'/rossiya/cars/ford/ranger_na/all/?listing=listing',538),
(5786,'S-MAX','s_max',146,'/rossiya/cars/ford/s_max/all/?listing=listing',538),
(5787,'Scorpio','scorpio',111,'/rossiya/cars/ford/scorpio/all/?listing=listing',538),
(5788,'Sierra','sierra',98,'/rossiya/cars/ford/sierra/all/?listing=listing',538),
(5789,'Spectron','spectron',0,'/rossiya/cars/ford/spectron/all/?listing=listing',538),
(5790,'Taunus','taunus',9,'/rossiya/cars/ford/taunus/all/?listing=listing',538),
(5791,'Taurus','taurus',66,'/rossiya/cars/ford/taurus/all/?listing=listing',538),
(5792,'Taurus X','taurus_x',0,'/rossiya/cars/ford/taurus_x/all/?listing=listing',538),
(5793,'Telstar','telstar',0,'/rossiya/cars/ford/telstar/all/?listing=listing',538),
(5794,'Tempo','tempo',1,'/rossiya/cars/ford/tempo/all/?listing=listing',538),
(5795,'Thunderbird','thunderbird',8,'/rossiya/cars/ford/thunderbird/all/?listing=listing',538),
(5796,'Torino','torino',0,'/rossiya/cars/ford/torino/all/?listing=listing',538),
(5798,'Tourneo Courier','tourneo_courier',0,'/rossiya/cars/ford/tourneo_courier/all/?listing=listing',538),
(5799,'Tourneo Custom','tourneo_custom',15,'/rossiya/cars/ford/tourneo_custom/all/?listing=listing',538),
(5800,'Windstar','windstar',5,'/rossiya/cars/ford/windstar/all/?listing=listing',538),
(5801,'Zephyr','zephyr',0,'/rossiya/cars/ford/zephyr/all/?listing=listing',538),
(5802,'Accord','accord',1213,'/rossiya/cars/honda/accord/all/?listing=listing',539),
(5803,'CR-V','cr_v',1085,'/rossiya/cars/honda/cr_v/all/?listing=listing',539),
(5804,'Civic','civic',1379,'/rossiya/cars/honda/civic/all/?listing=listing',539),
(5805,'Fit','fit',161,'/rossiya/cars/honda/fit/all/?listing=listing',539),
(5806,'HR-V','hr_v',195,'/rossiya/cars/honda/hr_v/all/?listing=listing',539),
(5807,'Jazz','jazz',140,'/rossiya/cars/honda/jazz/all/?listing=listing',539),
(5808,'Odyssey','odyssey',67,'/rossiya/cars/honda/odyssey/all/?listing=listing',539),
(5809,'Pilot','pilot',92,'/rossiya/cars/honda/pilot/all/?listing=listing',539),
(5810,'Prelude','prelude',65,'/rossiya/cars/honda/prelude/all/?listing=listing',539),
(5811,'Stepwgn','stepwagon',59,'/rossiya/cars/honda/stepwagon/all/?listing=listing',539),
(5812,'Stream','stream',55,'/rossiya/cars/honda/stream/all/?listing=listing',539),
(5814,'Acty','acty',5,'/rossiya/cars/honda/acty/all/?listing=listing',539),
(5815,'Airwave','airwave',21,'/rossiya/cars/honda/airwave/all/?listing=listing',539),
(5816,'Ascot','ascot',8,'/rossiya/cars/honda/ascot/all/?listing=listing',539),
(5817,'Ascot Innova','ascot_innova',1,'/rossiya/cars/honda/ascot_innova/all/?listing=listing',539),
(5818,'Avancier','avancier',7,'/rossiya/cars/honda/avancier/all/?listing=listing',539),
(5819,'Beat','beat',0,'/rossiya/cars/honda/beat/all/?listing=listing',539),
(5820,'Capa','capa',15,'/rossiya/cars/honda/capa/all/?listing=listing',539),
(5821,'City','city',17,'/rossiya/cars/honda/city/all/?listing=listing',539),
(5823,'Civic Ferio','civic_ferio',36,'/rossiya/cars/honda/civic_ferio/all/?listing=listing',539),
(5824,'Civic Type R','civic_type_r',32,'/rossiya/cars/honda/civic_type_r/all/?listing=listing',539),
(5825,'Concerto','concerto',1,'/rossiya/cars/honda/concerto/all/?listing=listing',539),
(5827,'CR-X','cr_x',8,'/rossiya/cars/honda/cr_x/all/?listing=listing',539),
(5828,'CR-Z','cr_z',5,'/rossiya/cars/honda/cr_z/all/?listing=listing',539),
(5829,'Crossroad','crossroad',2,'/rossiya/cars/honda/crossroad/all/?listing=listing',539),
(5830,'Crosstour','crosstour',39,'/rossiya/cars/honda/crosstour/all/?listing=listing',539),
(5831,'Domani','domani',14,'/rossiya/cars/honda/domani/all/?listing=listing',539),
(5832,'Edix','edix',4,'/rossiya/cars/honda/edix/all/?listing=listing',539),
(5833,'Element','element',25,'/rossiya/cars/honda/element/all/?listing=listing',539),
(5834,'Elysion','elysion',5,'/rossiya/cars/honda/elysion/all/?listing=listing',539),
(5835,'FCX Clarity','fcx_clarity',0,'/rossiya/cars/honda/fcx_clarity/all/?listing=listing',539),
(5837,'Fit Aria','fit_aria',9,'/rossiya/cars/honda/fit_aria/all/?listing=listing',539),
(5838,'FR-V','fr_v',4,'/rossiya/cars/honda/fr_v/all/?listing=listing',539),
(5839,'Freed','freed',24,'/rossiya/cars/honda/freed/all/?listing=listing',539),
(5841,'Insight','insight',35,'/rossiya/cars/honda/insight/all/?listing=listing',539),
(5842,'Inspire','inspire',24,'/rossiya/cars/honda/inspire/all/?listing=listing',539),
(5843,'Integra','integra',40,'/rossiya/cars/honda/integra/all/?listing=listing',539),
(5844,'Integra SJ','integra_sj',0,'/rossiya/cars/honda/integra_sj/all/?listing=listing',539),
(5846,'Legend','legend',50,'/rossiya/cars/honda/legend/all/?listing=listing',539),
(5847,'Life','life',9,'/rossiya/cars/honda/life/all/?listing=listing',539),
(5848,'Logo','logo',28,'/rossiya/cars/honda/logo/all/?listing=listing',539),
(5849,'MDX','mdx',0,'/rossiya/cars/honda/mdx/all/?listing=listing',539),
(5850,'Mobilio','mobilio',28,'/rossiya/cars/honda/mobilio/all/?listing=listing',539),
(5851,'NSX','nsx',1,'/rossiya/cars/honda/nsx/all/?listing=listing',539),
(5853,'Odyssey (North America)','odyssey_na',6,'/rossiya/cars/honda/odyssey_na/all/?listing=listing',539),
(5854,'Orthia','orthia',16,'/rossiya/cars/honda/orthia/all/?listing=listing',539),
(5855,'Partner','partner',20,'/rossiya/cars/honda/partner/all/?listing=listing',539),
(5856,'Passport','passport',1,'/rossiya/cars/honda/passport/all/?listing=listing',539),
(5859,'Quint','quint',0,'/rossiya/cars/honda/quint/all/?listing=listing',539),
(5860,'Rafaga','rafaga',5,'/rossiya/cars/honda/rafaga/all/?listing=listing',539),
(5861,'Ridgeline','ridgeline',36,'/rossiya/cars/honda/ridgeline/all/?listing=listing',539),
(5862,'S-MX','s_mx',6,'/rossiya/cars/honda/s_mx/all/?listing=listing',539),
(5863,'S2000','s2000',3,'/rossiya/cars/honda/s2000/all/?listing=listing',539),
(5864,'S660','s660',0,'/rossiya/cars/honda/s660/all/?listing=listing',539),
(5865,'Saber','saber',5,'/rossiya/cars/honda/saber/all/?listing=listing',539),
(5866,'Shuttle','shuttle',10,'/rossiya/cars/honda/shuttle/all/?listing=listing',539),
(5869,'That\'S','thats',0,'/rossiya/cars/honda/thats/all/?listing=listing',539),
(5870,'Today','today',0,'/rossiya/cars/honda/today/all/?listing=listing',539),
(5871,'Torneo','torneo',8,'/rossiya/cars/honda/torneo/all/?listing=listing',539),
(5872,'Vamos','vamos',3,'/rossiya/cars/honda/vamos/all/?listing=listing',539),
(5873,'Vezel','vezel',1,'/rossiya/cars/honda/vezel/all/?listing=listing',539),
(5874,'Vigor','vigor',4,'/rossiya/cars/honda/vigor/all/?listing=listing',539),
(5875,'Z','z',3,'/rossiya/cars/honda/z/all/?listing=listing',539),
(5876,'Zest','zest',3,'/rossiya/cars/honda/zest/all/?listing=listing',539),
(5877,'Accent','accent',1435,'/rossiya/cars/hyundai/accent/all/?listing=listing',540),
(5878,'Elantra','elantra',1321,'/rossiya/cars/hyundai/elantra/all/?listing=listing',540),
(5879,'Getz','getz',995,'/rossiya/cars/hyundai/getz/all/?listing=listing',540),
(5880,'Santa Fe','santa_fe',1439,'/rossiya/cars/hyundai/santa_fe/all/?listing=listing',540),
(5881,'Solaris','solaris',7171,'/rossiya/cars/hyundai/solaris/all/?listing=listing',540),
(5882,'Sonata','sonata',947,'/rossiya/cars/hyundai/sonata/all/?listing=listing',540),
(5883,'Starex (H-1)','h_1_starex',806,'/rossiya/cars/hyundai/h_1_starex/all/?listing=listing',540),
(5884,'Tucson','tucson',812,'/rossiya/cars/hyundai/tucson/all/?listing=listing',540),
(5885,'i30','i30',442,'/rossiya/cars/hyundai/i30/all/?listing=listing',540),
(5886,'i40','i40',429,'/rossiya/cars/hyundai/i40/all/?listing=listing',540),
(5887,'ix35','ix35',755,'/rossiya/cars/hyundai/ix35/all/?listing=listing',540),
(5889,'Aslan','aslan',0,'/rossiya/cars/hyundai/aslan/all/?listing=listing',540),
(5890,'Atos','atos',6,'/rossiya/cars/hyundai/atos/all/?listing=listing',540),
(5891,'Avante','avante',59,'/rossiya/cars/hyundai/avante/all/?listing=listing',540),
(5892,'Centennial','centennial',1,'/rossiya/cars/hyundai/centennial/all/?listing=listing',540),
(5894,'Creta','creta',84,'/rossiya/cars/hyundai/creta/all/?listing=listing',540),
(5895,'Dynasty','dynasty',0,'/rossiya/cars/hyundai/dynasty/all/?listing=listing',540),
(5897,'Entourage','entourage',0,'/rossiya/cars/hyundai/entourage/all/?listing=listing',540),
(5898,'Equus','equus',152,'/rossiya/cars/hyundai/equus/all/?listing=listing',540),
(5899,'Excel','excel',2,'/rossiya/cars/hyundai/excel/all/?listing=listing',540),
(5900,'Galloper','galloper',48,'/rossiya/cars/hyundai/galloper/all/?listing=listing',540),
(5901,'Genesis','genesis',130,'/rossiya/cars/hyundai/genesis/all/?listing=listing',540),
(5902,'Genesis Coupe','genesis_coupe',29,'/rossiya/cars/hyundai/genesis_coupe/all/?listing=listing',540),
(5904,'Grace','grace',12,'/rossiya/cars/hyundai/grace/all/?listing=listing',540),
(5905,'Grandeur','grandeur',87,'/rossiya/cars/hyundai/grandeur/all/?listing=listing',540),
(5906,'i10','i10',6,'/rossiya/cars/hyundai/i10/all/?listing=listing',540),
(5907,'i20','i20',74,'/rossiya/cars/hyundai/i20/all/?listing=listing',540),
(5910,'ix20','ix20',1,'/rossiya/cars/hyundai/ix20/all/?listing=listing',540),
(5912,'ix55','ix55',61,'/rossiya/cars/hyundai/ix55/all/?listing=listing',540),
(5913,'Lantra','lantra',46,'/rossiya/cars/hyundai/lantra/all/?listing=listing',540),
(5914,'Lavita','lavita',5,'/rossiya/cars/hyundai/lavita/all/?listing=listing',540),
(5915,'Marcia','marcia',0,'/rossiya/cars/hyundai/marcia/all/?listing=listing',540),
(5916,'Matrix','matrix',188,'/rossiya/cars/hyundai/matrix/all/?listing=listing',540),
(5917,'Maxcruz','maxcruz',0,'/rossiya/cars/hyundai/maxcruz/all/?listing=listing',540),
(5918,'Pony','pony',13,'/rossiya/cars/hyundai/pony/all/?listing=listing',540),
(5920,'Santamo','santamo',4,'/rossiya/cars/hyundai/santamo/all/?listing=listing',540),
(5921,'Scoupe','scoupe',2,'/rossiya/cars/hyundai/scoupe/all/?listing=listing',540),
(5925,'Stellar','stellar',1,'/rossiya/cars/hyundai/stellar/all/?listing=listing',540),
(5926,'Terracan','terracan',79,'/rossiya/cars/hyundai/terracan/all/?listing=listing',540),
(5927,'Tiburon','tiburon',68,'/rossiya/cars/hyundai/tiburon/all/?listing=listing',540),
(5928,'Trajet','trajet',39,'/rossiya/cars/hyundai/trajet/all/?listing=listing',540),
(5930,'Tuscani','tuscani',8,'/rossiya/cars/hyundai/tuscani/all/?listing=listing',540),
(5931,'Veloster','veloster',43,'/rossiya/cars/hyundai/veloster/all/?listing=listing',540),
(5932,'Veracruz','veracruz',2,'/rossiya/cars/hyundai/veracruz/all/?listing=listing',540),
(5933,'Verna','verna',53,'/rossiya/cars/hyundai/verna/all/?listing=listing',540),
(5934,'XG','xg',14,'/rossiya/cars/hyundai/xg/all/?listing=listing',540),
(5935,'EX','ex',119,'/rossiya/cars/infiniti/ex/all/?listing=listing',541),
(5936,'FX','fx',778,'/rossiya/cars/infiniti/fx/all/?listing=listing',541),
(5937,'G','g35',260,'/rossiya/cars/infiniti/g35/all/?listing=listing',541),
(5938,'M','m',183,'/rossiya/cars/infiniti/m/all/?listing=listing',541),
(5939,'Q50','q50',76,'/rossiya/cars/infiniti/q50/all/?listing=listing',541),
(5940,'Q70','q70',27,'/rossiya/cars/infiniti/q70/all/?listing=listing',541),
(5941,'QX50','qx50',52,'/rossiya/cars/infiniti/qx50/all/?listing=listing',541),
(5942,'QX56','qx56',208,'/rossiya/cars/infiniti/qx56/all/?listing=listing',541),
(5943,'QX60','qx60',39,'/rossiya/cars/infiniti/qx60/all/?listing=listing',541),
(5944,'QX70','qx70',134,'/rossiya/cars/infiniti/qx70/all/?listing=listing',541),
(5945,'QX80','qx80',63,'/rossiya/cars/infiniti/qx80/all/?listing=listing',541),
(5949,'I','i',6,'/rossiya/cars/infiniti/i/all/?listing=listing',541),
(5950,'J','j30',2,'/rossiya/cars/infiniti/j30/all/?listing=listing',541),
(5951,'JX','jx',26,'/rossiya/cars/infiniti/jx/all/?listing=listing',541),
(5953,'Q','q',3,'/rossiya/cars/infiniti/q/all/?listing=listing',541),
(5954,'Q30','q30',0,'/rossiya/cars/infiniti/q30/all/?listing=listing',541),
(5955,'Q40','q40',0,'/rossiya/cars/infiniti/q40/all/?listing=listing',541),
(5957,'Q60','q60',0,'/rossiya/cars/infiniti/q60/all/?listing=listing',541),
(5959,'QX30','qx30',0,'/rossiya/cars/infiniti/qx30/all/?listing=listing',541),
(5960,'QX4','qx4',12,'/rossiya/cars/infiniti/qx4/all/?listing=listing',541),
(5966,'Cee\'d','ceed',2406,'/rossiya/cars/kia/ceed/all/?listing=listing',542),
(5967,'Cerato','cerato',1073,'/rossiya/cars/kia/cerato/all/?listing=listing',542),
(5968,'Magentis','magentis',186,'/rossiya/cars/kia/magentis/all/?listing=listing',542),
(5969,'Optima','optima',314,'/rossiya/cars/kia/optima/all/?listing=listing',542),
(5970,'Picanto','picanto',406,'/rossiya/cars/kia/picanto/all/?listing=listing',542),
(5971,'Rio','rio',4308,'/rossiya/cars/kia/rio/all/?listing=listing',542),
(5972,'Sorento','sorento',1303,'/rossiya/cars/kia/sorento/all/?listing=listing',542),
(5973,'Soul','soul',363,'/rossiya/cars/kia/soul/all/?listing=listing',542),
(5974,'Spectra','spectra',788,'/rossiya/cars/kia/spectra/all/?listing=listing',542),
(5975,'Sportage','sportage',2470,'/rossiya/cars/kia/sportage/all/?listing=listing',542),
(5976,'Venga','venga',194,'/rossiya/cars/kia/venga/all/?listing=listing',542),
(5977,'Avella','avella',23,'/rossiya/cars/kia/avella/all/?listing=listing',542),
(5978,'Cadenza','cadenza',2,'/rossiya/cars/kia/cadenza/all/?listing=listing',542),
(5979,'Capital','capital',0,'/rossiya/cars/kia/capital/all/?listing=listing',542),
(5980,'Carens','carens',105,'/rossiya/cars/kia/carens/all/?listing=listing',542),
(5981,'Carnival','carnival',96,'/rossiya/cars/kia/carnival/all/?listing=listing',542),
(5983,'Cee\'d GT','ceed_gt',14,'/rossiya/cars/kia/ceed_gt/all/?listing=listing',542),
(5985,'Clarus','clarus',35,'/rossiya/cars/kia/clarus/all/?listing=listing',542),
(5986,'Concord','concord',0,'/rossiya/cars/kia/concord/all/?listing=listing',542),
(5987,'Elan','elan',0,'/rossiya/cars/kia/elan/all/?listing=listing',542),
(5988,'Enterprise','enterprise',0,'/rossiya/cars/kia/enterprise/all/?listing=listing',542),
(5989,'Joice','joice',9,'/rossiya/cars/kia/joice/all/?listing=listing',542),
(5991,'Mohave (Borrego)','mohaves',152,'/rossiya/cars/kia/mohaves/all/?listing=listing',542),
(5992,'Niro','niro',0,'/rossiya/cars/kia/niro/all/?listing=listing',542),
(5993,'Opirus','opirus',44,'/rossiya/cars/kia/opirus/all/?listing=listing',542),
(5996,'Potentia','potentia',0,'/rossiya/cars/kia/potentia/all/?listing=listing',542),
(5997,'Pride','pride',5,'/rossiya/cars/kia/pride/all/?listing=listing',542),
(5998,'Quoris','quoris',84,'/rossiya/cars/kia/quoris/all/?listing=listing',542),
(5999,'Ray','ray',0,'/rossiya/cars/kia/ray/all/?listing=listing',542),
(6000,'Retona','retona',10,'/rossiya/cars/kia/retona/all/?listing=listing',542),
(6002,'Sedona','sedona',1,'/rossiya/cars/kia/sedona/all/?listing=listing',542),
(6003,'Sephia','sephia',43,'/rossiya/cars/kia/sephia/all/?listing=listing',542),
(6004,'Shuma','shuma',90,'/rossiya/cars/kia/shuma/all/?listing=listing',542),
(6010,'Visto','visto',0,'/rossiya/cars/kia/visto/all/?listing=listing',542),
(6011,'X-Trek','x_trek',0,'/rossiya/cars/kia/x_trek/all/?listing=listing',542),
(6012,'Aventador','aventador',4,'/rossiya/cars/lamborghini/aventador/all/?listing=listing',543),
(6013,'Gallardo','gallardo',11,'/rossiya/cars/lamborghini/gallardo/all/?listing=listing',543),
(6014,'Huracán','huracan',7,'/rossiya/cars/lamborghini/huracan/all/?listing=listing',543),
(6015,'Jalpa','jalpa',0,'/rossiya/cars/lamborghini/jalpa/all/?listing=listing',543),
(6016,'LM001','lm001',0,'/rossiya/cars/lamborghini/lm001/all/?listing=listing',543),
(6017,'LM002','lm002',0,'/rossiya/cars/lamborghini/lm002/all/?listing=listing',543),
(6018,'Miura','miura',0,'/rossiya/cars/lamborghini/miura/all/?listing=listing',543),
(6019,'Murcielago','murcielago',1,'/rossiya/cars/lamborghini/murcielago/all/?listing=listing',543),
(6020,'Reventon','reventon',0,'/rossiya/cars/lamborghini/reventon/all/?listing=listing',543),
(6021,'Sesto Elemento','sesto_elemento',0,'/rossiya/cars/lamborghini/sesto_elemento/all/?listing=listing',543),
(6022,'Veneno','veneno',1,'/rossiya/cars/lamborghini/veneno/all/?listing=listing',543),
(6023,'350/400 GT','350_400_gt',0,'/rossiya/cars/lamborghini/350_400_gt/all/?listing=listing',543),
(6025,'Centanario','centanario',0,'/rossiya/cars/lamborghini/centanario/all/?listing=listing',543),
(6026,'Countach','countach',0,'/rossiya/cars/lamborghini/countach/all/?listing=listing',543),
(6027,'Diablo','diablo',0,'/rossiya/cars/lamborghini/diablo/all/?listing=listing',543),
(6028,'Espada','espada',0,'/rossiya/cars/lamborghini/espada/all/?listing=listing',543),
(6031,'Islero','islero',0,'/rossiya/cars/lamborghini/islero/all/?listing=listing',543),
(6033,'Jarama','jarama',0,'/rossiya/cars/lamborghini/jarama/all/?listing=listing',543),
(6040,'Silhouette','silhouette',0,'/rossiya/cars/lamborghini/silhouette/all/?listing=listing',543),
(6041,'Urraco','urraco',0,'/rossiya/cars/lamborghini/urraco/all/?listing=listing',543),
(6043,'Defender','defender',116,'/rossiya/cars/land_rover/defender/all/?listing=listing',544),
(6044,'Discovery','discovery',850,'/rossiya/cars/land_rover/discovery/all/?listing=listing',544),
(6045,'Discovery Sport','discovery_sport',227,'/rossiya/cars/land_rover/discovery_sport/all/?listing=listing',544),
(6046,'Freelander','freelander',732,'/rossiya/cars/land_rover/freelander/all/?listing=listing',544),
(6047,'Range Rover','range_rover',1157,'/rossiya/cars/land_rover/range_rover/all/?listing=listing',544),
(6048,'Range Rover Evoque','evoque',490,'/rossiya/cars/land_rover/evoque/all/?listing=listing',544),
(6049,'Range Rover Sport','range_rover_sport',1016,'/rossiya/cars/land_rover/range_rover_sport/all/?listing=listing',544),
(6050,'Series I','series_i',0,'/rossiya/cars/land_rover/series_i/all/?listing=listing',544),
(6051,'Series II','series_ii',0,'/rossiya/cars/land_rover/series_ii/all/?listing=listing',544),
(6052,'Series III','series_iii',0,'/rossiya/cars/land_rover/series_iii/all/?listing=listing',544),
(6053,'CT','ct',21,'/rossiya/cars/lexus/ct/all/?listing=listing',545),
(6054,'ES','es',381,'/rossiya/cars/lexus/es/all/?listing=listing',545),
(6056,'GX','gx',256,'/rossiya/cars/lexus/gx/all/?listing=listing',545),
(6057,'IS','is',281,'/rossiya/cars/lexus/is/all/?listing=listing',545),
(6058,'IS F','is_f',7,'/rossiya/cars/lexus/is_f/all/?listing=listing',545),
(6059,'LS','ls',232,'/rossiya/cars/lexus/ls/all/?listing=listing',545),
(6060,'LX','lx',772,'/rossiya/cars/lexus/lx/all/?listing=listing',545),
(6061,'NX','nx',497,'/rossiya/cars/lexus/nx/all/?listing=listing',545),
(6062,'RX','rx',1271,'/rossiya/cars/lexus/rx/all/?listing=listing',545),
(6063,'SC','sc',20,'/rossiya/cars/lexus/sc/all/?listing=listing',545),
(6067,'GS F','gs_f',2,'/rossiya/cars/lexus/gs_f/all/?listing=listing',545),
(6069,'HS','hs',6,'/rossiya/cars/lexus/hs/all/?listing=listing',545),
(6072,'LC','lc',0,'/rossiya/cars/lexus/lc/all/?listing=listing',545),
(6073,'LFA','lfa',0,'/rossiya/cars/lexus/lfa/all/?listing=listing',545),
(6077,'RC','rc',3,'/rossiya/cars/lexus/rc/all/?listing=listing',545),
(6078,'RC F','rc_f',2,'/rossiya/cars/lexus/rc_f/all/?listing=listing',545),
(6081,'3','3',1944,'/rossiya/cars/mazda/3/all/?listing=listing',546),
(6082,'323','323',242,'/rossiya/cars/mazda/323/all/?listing=listing',546),
(6083,'6','6',2116,'/rossiya/cars/mazda/6/all/?listing=listing',546),
(6084,'626','626',344,'/rossiya/cars/mazda/626/all/?listing=listing',546),
(6085,'CX-5','cx_5',804,'/rossiya/cars/mazda/cx_5/all/?listing=listing',546),
(6086,'CX-7','cx_7',750,'/rossiya/cars/mazda/cx_7/all/?listing=listing',546),
(6087,'Demio','demio',174,'/rossiya/cars/mazda/demio/all/?listing=listing',546),
(6088,'Familia','familia',115,'/rossiya/cars/mazda/familia/all/?listing=listing',546),
(6089,'MPV','mpv',138,'/rossiya/cars/mazda/mpv/all/?listing=listing',546),
(6090,'RX-8','rx_8',98,'/rossiya/cars/mazda/rx_8/all/?listing=listing',546),
(6091,'Tribute','tribute',108,'/rossiya/cars/mazda/tribute/all/?listing=listing',546),
(6092,'1000','1000',0,'/rossiya/cars/mazda/1000/all/?listing=listing',546),
(6093,'121','121',12,'/rossiya/cars/mazda/121/all/?listing=listing',546),
(6094,'1300','1300',0,'/rossiya/cars/mazda/1300/all/?listing=listing',546),
(6095,'2','2',64,'/rossiya/cars/mazda/2/all/?listing=listing',546),
(6097,'3 MPS','3mps',63,'/rossiya/cars/mazda/3mps/all/?listing=listing',546),
(6099,'5','5',78,'/rossiya/cars/mazda/5/all/?listing=listing',546),
(6101,'6 MPS','6_mps',35,'/rossiya/cars/mazda/6_mps/all/?listing=listing',546),
(6102,'616','616',0,'/rossiya/cars/mazda/616/all/?listing=listing',546),
(6104,'818','818',0,'/rossiya/cars/mazda/818/all/?listing=listing',546),
(6105,'929','929',11,'/rossiya/cars/mazda/929/all/?listing=listing',546),
(6106,'Atenza','atenza',19,'/rossiya/cars/mazda/atenza/all/?listing=listing',546),
(6107,'Axela','axela',21,'/rossiya/cars/mazda/axela/all/?listing=listing',546),
(6108,'AZ-1','autozam_az1',0,'/rossiya/cars/mazda/autozam_az1/all/?listing=listing',546),
(6109,'AZ-Offroad','az_offroad',0,'/rossiya/cars/mazda/az_offroad/all/?listing=listing',546),
(6110,'AZ-Wagon','az_wagon',2,'/rossiya/cars/mazda/az_wagon/all/?listing=listing',546),
(6111,'B-series','b_series',20,'/rossiya/cars/mazda/b_series/all/?listing=listing',546),
(6112,'Biante','biante',7,'/rossiya/cars/mazda/biante/all/?listing=listing',546),
(6113,'Bongo','bongo',47,'/rossiya/cars/mazda/bongo/all/?listing=listing',546),
(6114,'Bongo Friendee','bongo_friendee',26,'/rossiya/cars/mazda/bongo_friendee/all/?listing=listing',546),
(6115,'BT-50','bt_50',62,'/rossiya/cars/mazda/bt_50/all/?listing=listing',546),
(6116,'Capella','capella',56,'/rossiya/cars/mazda/capella/all/?listing=listing',546),
(6117,'Carol','carol',2,'/rossiya/cars/mazda/carol/all/?listing=listing',546),
(6118,'Cosmo','cosmo',0,'/rossiya/cars/mazda/cosmo/all/?listing=listing',546),
(6119,'Cronos','cronos',1,'/rossiya/cars/mazda/cronos/all/?listing=listing',546),
(6120,'CX-3','cx_3',0,'/rossiya/cars/mazda/cx_3/all/?listing=listing',546),
(6123,'CX-9','cx_9',78,'/rossiya/cars/mazda/cx_9/all/?listing=listing',546),
(6125,'Efini MS-6','efini_ms_6',0,'/rossiya/cars/mazda/efini_ms_6/all/?listing=listing',546),
(6126,'Efini MS-8','efini_ms_8',1,'/rossiya/cars/mazda/efini_ms_8/all/?listing=listing',546),
(6127,'Efini MS-9','efini_ms_9',0,'/rossiya/cars/mazda/efini_ms_9/all/?listing=listing',546),
(6128,'Eunos 100','eunos_100',1,'/rossiya/cars/mazda/eunos_100/all/?listing=listing',546),
(6129,'Eunos 300','eunos_300',0,'/rossiya/cars/mazda/eunos_300/all/?listing=listing',546),
(6130,'Eunos 500','eunos_500',2,'/rossiya/cars/mazda/eunos_500/all/?listing=listing',546),
(6131,'Eunos 800','eunos_800',0,'/rossiya/cars/mazda/eunos_800/all/?listing=listing',546),
(6132,'Eunos Cosmo','eunos_cosmo',0,'/rossiya/cars/mazda/eunos_cosmo/all/?listing=listing',546),
(6134,'Lantis','lantis',0,'/rossiya/cars/mazda/lantis/all/?listing=listing',546),
(6135,'Laputa','laputa',0,'/rossiya/cars/mazda/laputa/all/?listing=listing',546),
(6136,'Luce','luce',2,'/rossiya/cars/mazda/luce/all/?listing=listing',546),
(6137,'Millenia','millenia',24,'/rossiya/cars/mazda/millenia/all/?listing=listing',546),
(6139,'MX-3','mx_3',25,'/rossiya/cars/mazda/mx_3/all/?listing=listing',546),
(6140,'MX-5','mx_5',21,'/rossiya/cars/mazda/mx_5/all/?listing=listing',546),
(6141,'MX-6','mx_6',8,'/rossiya/cars/mazda/mx_6/all/?listing=listing',546),
(6142,'Navajo','navajo',0,'/rossiya/cars/mazda/navajo/all/?listing=listing',546),
(6143,'Persona','persona',1,'/rossiya/cars/mazda/persona/all/?listing=listing',546),
(6144,'Premacy','premacy',47,'/rossiya/cars/mazda/premacy/all/?listing=listing',546),
(6145,'Proceed Levante','proceed_levante',1,'/rossiya/cars/mazda/proceed_levante/all/?listing=listing',546),
(6146,'Proceed Marvie','proceed_marvie',3,'/rossiya/cars/mazda/proceed_marvie/all/?listing=listing',546),
(6147,'Protege','protege',24,'/rossiya/cars/mazda/protege/all/?listing=listing',546),
(6148,'Revue','revue',0,'/rossiya/cars/mazda/revue/all/?listing=listing',546),
(6149,'Roadster','roadster',2,'/rossiya/cars/mazda/roadster/all/?listing=listing',546),
(6150,'RX-7','rx_7',6,'/rossiya/cars/mazda/rx_7/all/?listing=listing',546),
(6152,'Scrum','scrum',0,'/rossiya/cars/mazda/scrum/all/?listing=listing',546),
(6153,'Sentia','sentia',3,'/rossiya/cars/mazda/sentia/all/?listing=listing',546),
(6154,'Spiano','spiano',1,'/rossiya/cars/mazda/spiano/all/?listing=listing',546),
(6156,'Verisa','verisa',11,'/rossiya/cars/mazda/verisa/all/?listing=listing',546),
(6157,'Xedos 6','xedos_6',35,'/rossiya/cars/mazda/xedos_6/all/?listing=listing',546),
(6158,'Xedos 9','xedos_9',24,'/rossiya/cars/mazda/xedos_9/all/?listing=listing',546),
(6159,'A-klasse','a_klasse',460,'/rossiya/cars/mercedes/a_klasse/all/?listing=listing',547),
(6160,'C-klasse','c_klasse',2681,'/rossiya/cars/mercedes/c_klasse/all/?listing=listing',547),
(6161,'CLS-klasse','cls_klasse',407,'/rossiya/cars/mercedes/cls_klasse/all/?listing=listing',547),
(6162,'E-klasse','e_klasse',3781,'/rossiya/cars/mercedes/e_klasse/all/?listing=listing',547),
(6163,'G-klasse','g_klasse',528,'/rossiya/cars/mercedes/g_klasse/all/?listing=listing',547),
(6164,'GL-klasse','gl_klasse',1090,'/rossiya/cars/mercedes/gl_klasse/all/?listing=listing',547),
(6165,'GLK-klasse','glk_klasse',536,'/rossiya/cars/mercedes/glk_klasse/all/?listing=listing',547),
(6166,'GLS-klasse','gls_klasse',367,'/rossiya/cars/mercedes/gls_klasse/all/?listing=listing',547),
(6167,'M-klasse','m_klasse',1511,'/rossiya/cars/mercedes/m_klasse/all/?listing=listing',547),
(6168,'S-klasse','s_klasse',2210,'/rossiya/cars/mercedes/s_klasse/all/?listing=listing',547),
(6169,'Vito','vito',364,'/rossiya/cars/mercedes/vito/all/?listing=listing',547),
(6170,'190 (W201)','w201',162,'/rossiya/cars/mercedes/w201/all/?listing=listing',547),
(6171,'190 SL','190_sl',0,'/rossiya/cars/mercedes/190_sl/all/?listing=listing',547),
(6173,'A-klasse AMG','a_klasse_amg',13,'/rossiya/cars/mercedes/a_klasse_amg/all/?listing=listing',547),
(6174,'AMG GLC','glc_klasse_amg',0,'/rossiya/cars/mercedes/glc_klasse_amg/all/?listing=listing',547),
(6175,'AMG GLE','gle_klasse_amg',15,'/rossiya/cars/mercedes/gle_klasse_amg/all/?listing=listing',547),
(6176,'AMG GLE Coupe','gle_klasse_coupe_amg',27,'/rossiya/cars/mercedes/gle_klasse_coupe_amg/all/?listing=listing',547),
(6177,'AMG GT','amg_gt',39,'/rossiya/cars/mercedes/amg_gt/all/?listing=listing',547),
(6178,'B-klasse','b_klasse',217,'/rossiya/cars/mercedes/b_klasse/all/?listing=listing',547),
(6180,'C-klasse AMG','c_klasse_amg',31,'/rossiya/cars/mercedes/c_klasse_amg/all/?listing=listing',547),
(6181,'Citan','citan',4,'/rossiya/cars/mercedes/citan/all/?listing=listing',547),
(6182,'CL-klasse','cl_klasse',176,'/rossiya/cars/mercedes/cl_klasse/all/?listing=listing',547),
(6183,'CL-klasse AMG','cl_klasse_amg',55,'/rossiya/cars/mercedes/cl_klasse_amg/all/?listing=listing',547),
(6184,'CLA-klasse','cla_klasse',237,'/rossiya/cars/mercedes/cla_klasse/all/?listing=listing',547),
(6185,'CLA-klasse AMG','cla_klasse_amg',14,'/rossiya/cars/mercedes/cla_klasse_amg/all/?listing=listing',547),
(6186,'CLC-klasse','clc_klasse',24,'/rossiya/cars/mercedes/clc_klasse/all/?listing=listing',547),
(6187,'CLK-klasse','clk_klasse',195,'/rossiya/cars/mercedes/clk_klasse/all/?listing=listing',547),
(6188,'CLK-klasse AMG','clk_klasse_amg',3,'/rossiya/cars/mercedes/clk_klasse_amg/all/?listing=listing',547),
(6190,'CLS-klasse AMG','cls_klasse_amg',28,'/rossiya/cars/mercedes/cls_klasse_amg/all/?listing=listing',547),
(6192,'E-klasse AMG','e_klasse_amg',51,'/rossiya/cars/mercedes/e_klasse_amg/all/?listing=listing',547),
(6194,'G-klasse AMG','g_klasse_amg',243,'/rossiya/cars/mercedes/g_klasse_amg/all/?listing=listing',547),
(6195,'G-klasse AMG 6x6','g_klasse_amg_6x6',1,'/rossiya/cars/mercedes/g_klasse_amg_6x6/all/?listing=listing',547),
(6197,'GL-klasse AMG','gl_klasse_amg',58,'/rossiya/cars/mercedes/gl_klasse_amg/all/?listing=listing',547),
(6198,'GLA-klasse','gla_class',253,'/rossiya/cars/mercedes/gla_class/all/?listing=listing',547),
(6199,'GLA-klasse AMG','gla_class_amg',15,'/rossiya/cars/mercedes/gla_class_amg/all/?listing=listing',547),
(6200,'GLC','glc_klasse',237,'/rossiya/cars/mercedes/glc_klasse/all/?listing=listing',547),
(6201,'GLC Coupe','glc_coupe',15,'/rossiya/cars/mercedes/glc_coupe/all/?listing=listing',547),
(6202,'GLE','gle_klasse',262,'/rossiya/cars/mercedes/gle_klasse/all/?listing=listing',547),
(6203,'GLE Coupe','gle_klasse_coupe',154,'/rossiya/cars/mercedes/gle_klasse_coupe/all/?listing=listing',547),
(6206,'GLS-klasse AMG','gls_klasse_amg',51,'/rossiya/cars/mercedes/gls_klasse_amg/all/?listing=listing',547),
(6208,'M-klasse AMG','m_klasse_amg',105,'/rossiya/cars/mercedes/m_klasse_amg/all/?listing=listing',547),
(6209,'Maybach S-klasse','s_class_maybach',152,'/rossiya/cars/mercedes/s_class_maybach/all/?listing=listing',547),
(6210,'R-klasse','r_klasse',111,'/rossiya/cars/mercedes/r_klasse/all/?listing=listing',547),
(6211,'R-klasse AMG','r_klasse_amg',0,'/rossiya/cars/mercedes/r_klasse_amg/all/?listing=listing',547),
(6213,'S-klasse AMG','s_klasse_amg',189,'/rossiya/cars/mercedes/s_klasse_amg/all/?listing=listing',547),
(6214,'SL-klasse','sl_klasse',132,'/rossiya/cars/mercedes/sl_klasse/all/?listing=listing',547),
(6215,'SL-klasse AMG','sl_klasse_amg',38,'/rossiya/cars/mercedes/sl_klasse_amg/all/?listing=listing',547),
(6216,'SLC-klasse','slc_klasse',5,'/rossiya/cars/mercedes/slc_klasse/all/?listing=listing',547),
(6217,'SLC-klasse AMG','slc_klasse_amg',0,'/rossiya/cars/mercedes/slc_klasse_amg/all/?listing=listing',547),
(6218,'SLK-klasse','slk_klasse',143,'/rossiya/cars/mercedes/slk_klasse/all/?listing=listing',547),
(6219,'SLK-klasse AMG','slk_klasse_amg',3,'/rossiya/cars/mercedes/slk_klasse_amg/all/?listing=listing',547),
(6220,'SLR McLaren','slr_klasse',13,'/rossiya/cars/mercedes/slr_klasse/all/?listing=listing',547),
(6221,'SLS AMG','sls_amg',3,'/rossiya/cars/mercedes/sls_amg/all/?listing=listing',547),
(6222,'V-klasse','v_klasse',139,'/rossiya/cars/mercedes/v_klasse/all/?listing=listing',547),
(6223,'Vaneo','vaneo',22,'/rossiya/cars/mercedes/vaneo/all/?listing=listing',547),
(6224,'Viano','viano',276,'/rossiya/cars/mercedes/viano/all/?listing=listing',547),
(6226,'W100','w100',0,'/rossiya/cars/mercedes/w100/all/?listing=listing',547),
(6227,'W108','w108',0,'/rossiya/cars/mercedes/w108/all/?listing=listing',547),
(6228,'W110','w110',2,'/rossiya/cars/mercedes/w110/all/?listing=listing',547),
(6229,'W111','w111',0,'/rossiya/cars/mercedes/w111/all/?listing=listing',547),
(6230,'W114','w114',1,'/rossiya/cars/mercedes/w114/all/?listing=listing',547),
(6231,'W115','w115',3,'/rossiya/cars/mercedes/w115/all/?listing=listing',547),
(6232,'W120','w120',0,'/rossiya/cars/mercedes/w120/all/?listing=listing',547),
(6233,'W121','w121',0,'/rossiya/cars/mercedes/w121/all/?listing=listing',547),
(6234,'W123','w123',98,'/rossiya/cars/mercedes/w123/all/?listing=listing',547),
(6235,'W124','w124',296,'/rossiya/cars/mercedes/w124/all/?listing=listing',547),
(6236,'W128','w128',1,'/rossiya/cars/mercedes/w128/all/?listing=listing',547),
(6237,'W136','w136',1,'/rossiya/cars/mercedes/w136/all/?listing=listing',547),
(6238,'W186','w186',1,'/rossiya/cars/mercedes/w186/all/?listing=listing',547),
(6239,'W188','w188',1,'/rossiya/cars/mercedes/w188/all/?listing=listing',547),
(6240,'W189','w189',2,'/rossiya/cars/mercedes/w189/all/?listing=listing',547),
(6241,'Cabrio','cabrio',14,'/rossiya/cars/mini/cabrio/all/?listing=listing',548),
(6242,'Clubman','clubman',64,'/rossiya/cars/mini/clubman/all/?listing=listing',548),
(6243,'Countryman','countryman',167,'/rossiya/cars/mini/countryman/all/?listing=listing',548),
(6245,'Hatch','hatch',383,'/rossiya/cars/mini/hatch/all/?listing=listing',548),
(6246,'Paceman','paceman',15,'/rossiya/cars/mini/paceman/all/?listing=listing',548),
(6248,'ASX','asx',559,'/rossiya/cars/mitsubishi/asx/all/?listing=listing',549),
(6249,'Carisma','carisma',356,'/rossiya/cars/mitsubishi/carisma/all/?listing=listing',549),
(6250,'Colt','colt',333,'/rossiya/cars/mitsubishi/colt/all/?listing=listing',549),
(6251,'Eclipse','eclipse',103,'/rossiya/cars/mitsubishi/eclipse/all/?listing=listing',549),
(6252,'Galant','galant',718,'/rossiya/cars/mitsubishi/galant/all/?listing=listing',549),
(6253,'L200','l200',387,'/rossiya/cars/mitsubishi/l200/all/?listing=listing',549),
(6254,'Lancer','lancer',3191,'/rossiya/cars/mitsubishi/lancer/all/?listing=listing',549),
(6255,'Montero Sport','montero_sport',106,'/rossiya/cars/mitsubishi/montero_sport/all/?listing=listing',549),
(6256,'Outlander','outlander',1842,'/rossiya/cars/mitsubishi/outlander/all/?listing=listing',549),
(6257,'Pajero','pajero',1242,'/rossiya/cars/mitsubishi/pajero/all/?listing=listing',549),
(6258,'Pajero Sport','pajero_sport',695,'/rossiya/cars/mitsubishi/pajero_sport/all/?listing=listing',549),
(6259,'3000 GT','3000_gt',13,'/rossiya/cars/mitsubishi/3000_gt/all/?listing=listing',549),
(6260,'Airtrek','airtrek',54,'/rossiya/cars/mitsubishi/airtrek/all/?listing=listing',549),
(6263,'Attrage','attrage',0,'/rossiya/cars/mitsubishi/attrage/all/?listing=listing',549),
(6265,'Celeste','celeste',0,'/rossiya/cars/mitsubishi/celeste/all/?listing=listing',549),
(6266,'Challenger','challenger',4,'/rossiya/cars/mitsubishi/challenger/all/?listing=listing',549),
(6267,'Chariot','chariot',39,'/rossiya/cars/mitsubishi/chariot/all/?listing=listing',549),
(6269,'Cordia','cordia',0,'/rossiya/cars/mitsubishi/cordia/all/?listing=listing',549),
(6270,'Debonair','debonair',0,'/rossiya/cars/mitsubishi/debonair/all/?listing=listing',549),
(6271,'Delica','delica',89,'/rossiya/cars/mitsubishi/delica/all/?listing=listing',549),
(6272,'Delica D:2','delica_d2',3,'/rossiya/cars/mitsubishi/delica_d2/all/?listing=listing',549),
(6273,'Delica D:3','delica_d3',0,'/rossiya/cars/mitsubishi/delica_d3/all/?listing=listing',549),
(6274,'Diamante','diamante',31,'/rossiya/cars/mitsubishi/diamante/all/?listing=listing',549),
(6275,'Dignity','dignity',0,'/rossiya/cars/mitsubishi/dignity/all/?listing=listing',549),
(6276,'Dingo','dingo',28,'/rossiya/cars/mitsubishi/dingo/all/?listing=listing',549),
(6277,'Dion','dion',23,'/rossiya/cars/mitsubishi/dion/all/?listing=listing',549),
(6279,'eK','ek',9,'/rossiya/cars/mitsubishi/ek/all/?listing=listing',549),
(6280,'Emeraude','emeraude',8,'/rossiya/cars/mitsubishi/emeraude/all/?listing=listing',549),
(6281,'Endeavor','endeavor',7,'/rossiya/cars/mitsubishi/endeavor/all/?listing=listing',549),
(6282,'Eterna','eterna',6,'/rossiya/cars/mitsubishi/eterna/all/?listing=listing',549),
(6283,'FTO','fto',6,'/rossiya/cars/mitsubishi/fto/all/?listing=listing',549),
(6285,'Grandis','grandis',70,'/rossiya/cars/mitsubishi/grandis/all/?listing=listing',549),
(6286,'GTO','gto',3,'/rossiya/cars/mitsubishi/gto/all/?listing=listing',549),
(6288,'i-MiEV','i_miev',11,'/rossiya/cars/mitsubishi/i_miev/all/?listing=listing',549),
(6289,'Jeep J','jeep_j',0,'/rossiya/cars/mitsubishi/jeep_j/all/?listing=listing',549),
(6292,'Lancer Cargo','lancer_cargo',2,'/rossiya/cars/mitsubishi/lancer_cargo/all/?listing=listing',549),
(6293,'Lancer Evolution','lancer_evolution',78,'/rossiya/cars/mitsubishi/lancer_evolution/all/?listing=listing',549),
(6294,'Lancer Ralliart','lancer_ralliart',4,'/rossiya/cars/mitsubishi/lancer_ralliart/all/?listing=listing',549),
(6295,'Legnum','legnum',46,'/rossiya/cars/mitsubishi/legnum/all/?listing=listing',549),
(6296,'Libero','libero',18,'/rossiya/cars/mitsubishi/libero/all/?listing=listing',549),
(6297,'Minica','minica',10,'/rossiya/cars/mitsubishi/minica/all/?listing=listing',549),
(6298,'Minicab','minicab',2,'/rossiya/cars/mitsubishi/minicab/all/?listing=listing',549),
(6299,'Mirage','mirage',58,'/rossiya/cars/mitsubishi/mirage/all/?listing=listing',549),
(6300,'Montero','montero',92,'/rossiya/cars/mitsubishi/montero/all/?listing=listing',549),
(6304,'Pajero iO','pajero_io',36,'/rossiya/cars/mitsubishi/pajero_io/all/?listing=listing',549),
(6305,'Pajero Junior','pajero_junior',7,'/rossiya/cars/mitsubishi/pajero_junior/all/?listing=listing',549),
(6306,'Pajero Mini','pajero_mini',50,'/rossiya/cars/mitsubishi/pajero_mini/all/?listing=listing',549),
(6307,'Pajero Pinin','pajero_pinin',81,'/rossiya/cars/mitsubishi/pajero_pinin/all/?listing=listing',549),
(6309,'Pistachio','pistachio',0,'/rossiya/cars/mitsubishi/pistachio/all/?listing=listing',549),
(6310,'Proudia','proudia',0,'/rossiya/cars/mitsubishi/proudia/all/?listing=listing',549),
(6311,'Raider','raider',1,'/rossiya/cars/mitsubishi/raider/all/?listing=listing',549),
(6312,'RVR','rvr',47,'/rossiya/cars/mitsubishi/rvr/all/?listing=listing',549),
(6313,'Sapporo','sapporo',0,'/rossiya/cars/mitsubishi/sapporo/all/?listing=listing',549),
(6314,'Sigma','sigma',4,'/rossiya/cars/mitsubishi/sigma/all/?listing=listing',549),
(6315,'Space Gear','space_gear',9,'/rossiya/cars/mitsubishi/space_gear/all/?listing=listing',549),
(6316,'Space Runner','space_runner',16,'/rossiya/cars/mitsubishi/space_runner/all/?listing=listing',549),
(6317,'Space Star','space_star',92,'/rossiya/cars/mitsubishi/space_star/all/?listing=listing',549),
(6318,'Space Wagon','space_wagon',55,'/rossiya/cars/mitsubishi/space_wagon/all/?listing=listing',549),
(6319,'Starion','starion',1,'/rossiya/cars/mitsubishi/starion/all/?listing=listing',549),
(6320,'Strada','strada',0,'/rossiya/cars/mitsubishi/strada/all/?listing=listing',549),
(6321,'Toppo','toppo',3,'/rossiya/cars/mitsubishi/toppo/all/?listing=listing',549),
(6322,'Town Box','town_box',0,'/rossiya/cars/mitsubishi/town_box/all/?listing=listing',549),
(6323,'Tredia','tredia',0,'/rossiya/cars/mitsubishi/tredia/all/?listing=listing',549),
(6324,'Almera','almera',1565,'/rossiya/cars/nissan/almera/all/?listing=listing',550),
(6325,'Almera Classic','almera_classic',710,'/rossiya/cars/nissan/almera_classic/all/?listing=listing',550),
(6326,'Juke','juke',890,'/rossiya/cars/nissan/juke/all/?listing=listing',550),
(6327,'Murano','murano',608,'/rossiya/cars/nissan/murano/all/?listing=listing',550),
(6328,'Note','note',566,'/rossiya/cars/nissan/note/all/?listing=listing',550),
(6329,'Pathfinder','pathfinder',512,'/rossiya/cars/nissan/pathfinder/all/?listing=listing',550),
(6330,'Primera','primera',1094,'/rossiya/cars/nissan/primera/all/?listing=listing',550),
(6331,'Qashqai','qashqai',2246,'/rossiya/cars/nissan/qashqai/all/?listing=listing',550),
(6332,'Teana','teana',1216,'/rossiya/cars/nissan/teana/all/?listing=listing',550),
(6333,'Tiida','tiida',572,'/rossiya/cars/nissan/tiida/all/?listing=listing',550),
(6334,'X-Trail','x_trail',1826,'/rossiya/cars/nissan/x_trail/all/?listing=listing',550),
(6335,'100NX','100nx',5,'/rossiya/cars/nissan/100nx/all/?listing=listing',550),
(6336,'180SX','180sx',0,'/rossiya/cars/nissan/180sx/all/?listing=listing',550),
(6337,'200SX','200sx',7,'/rossiya/cars/nissan/200sx/all/?listing=listing',550),
(6338,'240SX','240sx',0,'/rossiya/cars/nissan/240sx/all/?listing=listing',550),
(6339,'280ZX','280zx',1,'/rossiya/cars/nissan/280zx/all/?listing=listing',550),
(6340,'300ZX','300zx',2,'/rossiya/cars/nissan/300zx/all/?listing=listing',550),
(6341,'350Z','350z',32,'/rossiya/cars/nissan/350z/all/?listing=listing',550),
(6342,'370Z','370z',3,'/rossiya/cars/nissan/370z/all/?listing=listing',550),
(6343,'AD','ad',71,'/rossiya/cars/nissan/ad/all/?listing=listing',550),
(6346,'Almera Tino','almera_tino',11,'/rossiya/cars/nissan/almera_tino/all/?listing=listing',550),
(6347,'Altima','altima',15,'/rossiya/cars/nissan/altima/all/?listing=listing',550),
(6348,'Armada','armada',16,'/rossiya/cars/nissan/armada/all/?listing=listing',550),
(6349,'Avenir','avenir',34,'/rossiya/cars/nissan/avenir/all/?listing=listing',550),
(6350,'Bassara','bassara',12,'/rossiya/cars/nissan/bassara/all/?listing=listing',550),
(6351,'BE-1','be_1',0,'/rossiya/cars/nissan/be_1/all/?listing=listing',550),
(6352,'Bluebird','bluebird',78,'/rossiya/cars/nissan/bluebird/all/?listing=listing',550),
(6353,'Bluebird Sylphy','bluebird_sylphy',34,'/rossiya/cars/nissan/bluebird_sylphy/all/?listing=listing',550),
(6354,'Caravan','caravan_coach',14,'/rossiya/cars/nissan/caravan_coach/all/?listing=listing',550),
(6355,'Cedric','cedric',25,'/rossiya/cars/nissan/cedric/all/?listing=listing',550),
(6356,'Cefiro','cefiro',95,'/rossiya/cars/nissan/cefiro/all/?listing=listing',550),
(6357,'Cherry','cherry',0,'/rossiya/cars/nissan/cherry/all/?listing=listing',550),
(6358,'Cima','cima',2,'/rossiya/cars/nissan/cima/all/?listing=listing',550),
(6359,'Clipper','clipper',5,'/rossiya/cars/nissan/clipper/all/?listing=listing',550),
(6360,'Crew','crew',0,'/rossiya/cars/nissan/crew/all/?listing=listing',550),
(6361,'Cube','cube',112,'/rossiya/cars/nissan/cube/all/?listing=listing',550),
(6362,'Datsun','datsun',5,'/rossiya/cars/nissan/datsun/all/?listing=listing',550),
(6363,'Dualis','dualis',2,'/rossiya/cars/nissan/dualis/all/?listing=listing',550),
(6364,'Elgrand','elgrand',24,'/rossiya/cars/nissan/elgrand/all/?listing=listing',550),
(6365,'Expert','expert',21,'/rossiya/cars/nissan/expert/all/?listing=listing',550),
(6366,'Fairlady Z','fairlady_z',2,'/rossiya/cars/nissan/fairlady_z/all/?listing=listing',550),
(6367,'Figaro','figaro',3,'/rossiya/cars/nissan/figaro/all/?listing=listing',550),
(6368,'Fuga','fuga',5,'/rossiya/cars/nissan/fuga/all/?listing=listing',550),
(6369,'Gloria','gloria',21,'/rossiya/cars/nissan/gloria/all/?listing=listing',550),
(6370,'GT-R','gt_r',47,'/rossiya/cars/nissan/gt_r/all/?listing=listing',550),
(6372,'Juke Nismo','juke_nismo',18,'/rossiya/cars/nissan/juke_nismo/all/?listing=listing',550),
(6373,'Kix','kix',1,'/rossiya/cars/nissan/kix/all/?listing=listing',550),
(6374,'Lafesta','lafesta',8,'/rossiya/cars/nissan/lafesta/all/?listing=listing',550),
(6375,'Langley','langley',1,'/rossiya/cars/nissan/langley/all/?listing=listing',550),
(6376,'Largo','largo',16,'/rossiya/cars/nissan/largo/all/?listing=listing',550),
(6377,'Latio','latio',0,'/rossiya/cars/nissan/latio/all/?listing=listing',550),
(6378,'Laurel','laurel',53,'/rossiya/cars/nissan/laurel/all/?listing=listing',550),
(6379,'Leaf','leaf',24,'/rossiya/cars/nissan/leaf/all/?listing=listing',550),
(6380,'Leopard','leopard',1,'/rossiya/cars/nissan/leopard/all/?listing=listing',550),
(6381,'Liberty','liberty',49,'/rossiya/cars/nissan/liberty/all/?listing=listing',550),
(6382,'Lucino','lucino',2,'/rossiya/cars/nissan/lucino/all/?listing=listing',550),
(6383,'March','march',143,'/rossiya/cars/nissan/march/all/?listing=listing',550),
(6384,'Maxima','maxima',409,'/rossiya/cars/nissan/maxima/all/?listing=listing',550),
(6385,'Micra','micra',264,'/rossiya/cars/nissan/micra/all/?listing=listing',550),
(6386,'Mistral','mistral',3,'/rossiya/cars/nissan/mistral/all/?listing=listing',550),
(6387,'Moco','moco',11,'/rossiya/cars/nissan/moco/all/?listing=listing',550),
(6389,'Navara (Frontier)','navara',153,'/rossiya/cars/nissan/navara/all/?listing=listing',550),
(6391,'NP 300','np300',33,'/rossiya/cars/nissan/np300/all/?listing=listing',550),
(6392,'NV200','nv200',18,'/rossiya/cars/nissan/nv200/all/?listing=listing',550),
(6393,'NV350 Caravan','nv350_caravan',0,'/rossiya/cars/nissan/nv350_caravan/all/?listing=listing',550),
(6394,'NX Coupe','nx_coupe',0,'/rossiya/cars/nissan/nx_coupe/all/?listing=listing',550),
(6395,'Otti (Dayz)','otti',8,'/rossiya/cars/nissan/otti/all/?listing=listing',550),
(6396,'Pao','pao',0,'/rossiya/cars/nissan/pao/all/?listing=listing',550),
(6398,'Patrol','patrol',377,'/rossiya/cars/nissan/patrol/all/?listing=listing',550),
(6399,'Pino','pino',1,'/rossiya/cars/nissan/pino/all/?listing=listing',550),
(6400,'Pixo','pixo',3,'/rossiya/cars/nissan/pixo/all/?listing=listing',550),
(6401,'Prairie','prairie',18,'/rossiya/cars/nissan/prairie/all/?listing=listing',550),
(6402,'Presage','presage',28,'/rossiya/cars/nissan/presage/all/?listing=listing',550),
(6403,'Presea','presea',17,'/rossiya/cars/nissan/presea/all/?listing=listing',550),
(6404,'President','president',2,'/rossiya/cars/nissan/president/all/?listing=listing',550),
(6405,'Primastar','primastar',1,'/rossiya/cars/nissan/primastar/all/?listing=listing',550),
(6407,'Pulsar','pulsar',51,'/rossiya/cars/nissan/pulsar/all/?listing=listing',550),
(6409,'Qashqai+2','qashqai_plus_2',145,'/rossiya/cars/nissan/qashqai_plus_2/all/?listing=listing',550),
(6410,'Quest','quest',7,'/rossiya/cars/nissan/quest/all/?listing=listing',550),
(6411,'R\'nessa','rnessa',18,'/rossiya/cars/nissan/rnessa/all/?listing=listing',550),
(6412,'Rasheen','rasheen',1,'/rossiya/cars/nissan/rasheen/all/?listing=listing',550),
(6413,'Rogue','rogue',5,'/rossiya/cars/nissan/rogue/all/?listing=listing',550),
(6414,'Roox','roox',6,'/rossiya/cars/nissan/roox/all/?listing=listing',550),
(6415,'Safari','safari',8,'/rossiya/cars/nissan/safari/all/?listing=listing',550),
(6416,'Sentra','sentra',174,'/rossiya/cars/nissan/sentra/all/?listing=listing',550),
(6417,'Serena','serena',142,'/rossiya/cars/nissan/serena/all/?listing=listing',550),
(6418,'Silvia','silvia',14,'/rossiya/cars/nissan/silvia/all/?listing=listing',550),
(6419,'Skyline','skyline',139,'/rossiya/cars/nissan/skyline/all/?listing=listing',550),
(6420,'Skyline Crossover','skyline_crossover',0,'/rossiya/cars/nissan/skyline_crossover/all/?listing=listing',550),
(6421,'Stagea','stagea',5,'/rossiya/cars/nissan/stagea/all/?listing=listing',550),
(6422,'Stanza','stanza',0,'/rossiya/cars/nissan/stanza/all/?listing=listing',550),
(6423,'Sunny','sunny',204,'/rossiya/cars/nissan/sunny/all/?listing=listing',550),
(6425,'Terrano','terrano',403,'/rossiya/cars/nissan/terrano/all/?listing=listing',550),
(6426,'Terrano Regulus','terrano_regulus',1,'/rossiya/cars/nissan/terrano_regulus/all/?listing=listing',550),
(6428,'Tino','tino',27,'/rossiya/cars/nissan/tino/all/?listing=listing',550),
(6429,'Titan','titan',12,'/rossiya/cars/nissan/titan/all/?listing=listing',550),
(6430,'Urvan','urvan',4,'/rossiya/cars/nissan/urvan/all/?listing=listing',550),
(6431,'Vanette','vanette',53,'/rossiya/cars/nissan/vanette/all/?listing=listing',550),
(6432,'Versa','versa',0,'/rossiya/cars/nissan/versa/all/?listing=listing',550),
(6433,'Wingroad','wingroad',112,'/rossiya/cars/nissan/wingroad/all/?listing=listing',550),
(6434,'X-Terra','xterra',3,'/rossiya/cars/nissan/xterra/all/?listing=listing',550),
(6436,'Antara','antara',432,'/rossiya/cars/opel/antara/all/?listing=listing',551),
(6439,'Frontera','frontera',145,'/rossiya/cars/opel/frontera/all/?listing=listing',551),
(6440,'Insignia','insignia',548,'/rossiya/cars/opel/insignia/all/?listing=listing',551),
(6441,'Kadett','kadett',84,'/rossiya/cars/opel/kadett/all/?listing=listing',551),
(6442,'Meriva','meriva',260,'/rossiya/cars/opel/meriva/all/?listing=listing',551),
(6443,'Mokka','mokka',313,'/rossiya/cars/opel/mokka/all/?listing=listing',551),
(6447,'Adam','adam',0,'/rossiya/cars/opel/adam/all/?listing=listing',551),
(6448,'Admiral','admiral',0,'/rossiya/cars/opel/admiral/all/?listing=listing',551),
(6449,'Agila','agila',26,'/rossiya/cars/opel/agila/all/?listing=listing',551),
(6450,'Ampera','ampera',1,'/rossiya/cars/opel/ampera/all/?listing=listing',551),
(6452,'Ascona','ascona',42,'/rossiya/cars/opel/ascona/all/?listing=listing',551),
(6454,'Astra OPC','astra_opc',46,'/rossiya/cars/opel/astra_opc/all/?listing=listing',551),
(6455,'Calibra','calibra',26,'/rossiya/cars/opel/calibra/all/?listing=listing',551),
(6456,'Campo','campo',0,'/rossiya/cars/opel/campo/all/?listing=listing',551),
(6457,'Cascada','cascada',0,'/rossiya/cars/opel/cascada/all/?listing=listing',551),
(6458,'Combo','combo',54,'/rossiya/cars/opel/combo/all/?listing=listing',551),
(6459,'Commodore','commodore',0,'/rossiya/cars/opel/commodore/all/?listing=listing',551),
(6461,'Corsa OPC','corsa_opc',9,'/rossiya/cars/opel/corsa_opc/all/?listing=listing',551),
(6462,'Diplomat','diplomat',0,'/rossiya/cars/opel/diplomat/all/?listing=listing',551),
(6466,'Insignia OPC','insignia_opc',20,'/rossiya/cars/opel/insignia_opc/all/?listing=listing',551),
(6468,'Kapitan','kapitan',2,'/rossiya/cars/opel/kapitan/all/?listing=listing',551),
(6469,'Karl','karl',0,'/rossiya/cars/opel/karl/all/?listing=listing',551),
(6470,'Manta','manta',3,'/rossiya/cars/opel/manta/all/?listing=listing',551),
(6472,'Meriva OPC','meriva_opc',0,'/rossiya/cars/opel/meriva_opc/all/?listing=listing',551),
(6474,'Monterey','monterey',25,'/rossiya/cars/opel/monterey/all/?listing=listing',551),
(6476,'Olympia','olympia',1,'/rossiya/cars/opel/olympia/all/?listing=listing',551),
(6478,'P4','p4',0,'/rossiya/cars/opel/p4/all/?listing=listing',551),
(6479,'Rekord','rekord',15,'/rossiya/cars/opel/rekord/all/?listing=listing',551),
(6480,'Senator','senator',4,'/rossiya/cars/opel/senator/all/?listing=listing',551),
(6481,'Signum','signum',15,'/rossiya/cars/opel/signum/all/?listing=listing',551),
(6482,'Sintra','sintra',16,'/rossiya/cars/opel/sintra/all/?listing=listing',551),
(6483,'Speedster','speedster',0,'/rossiya/cars/opel/speedster/all/?listing=listing',551),
(6484,'Super Six','super_six',0,'/rossiya/cars/opel/super_six/all/?listing=listing',551),
(6485,'Tigra','tigra',45,'/rossiya/cars/opel/tigra/all/?listing=listing',551),
(6487,'Vectra OPC','vectra_opc',4,'/rossiya/cars/opel/vectra_opc/all/?listing=listing',551),
(6488,'Vita','vita',17,'/rossiya/cars/opel/vita/all/?listing=listing',551),
(6489,'Vivaro','vivaro',70,'/rossiya/cars/opel/vivaro/all/?listing=listing',551),
(6491,'Zafira OPC','zafira_opc',1,'/rossiya/cars/opel/zafira_opc/all/?listing=listing',551),
(6492,'107','107',160,'/rossiya/cars/peugeot/107/all/?listing=listing',552),
(6493,'206','206',752,'/rossiya/cars/peugeot/206/all/?listing=listing',552),
(6494,'207','207',331,'/rossiya/cars/peugeot/207/all/?listing=listing',552),
(6495,'3008','3008',238,'/rossiya/cars/peugeot/3008/all/?listing=listing',552),
(6496,'307','307',555,'/rossiya/cars/peugeot/307/all/?listing=listing',552),
(6497,'308','308',1124,'/rossiya/cars/peugeot/308/all/?listing=listing',552),
(6498,'4007','4007',96,'/rossiya/cars/peugeot/4007/all/?listing=listing',552),
(6499,'406','406',199,'/rossiya/cars/peugeot/406/all/?listing=listing',552),
(6500,'407','407',269,'/rossiya/cars/peugeot/407/all/?listing=listing',552),
(6501,'408','408',445,'/rossiya/cars/peugeot/408/all/?listing=listing',552),
(6503,'1007','1007',16,'/rossiya/cars/peugeot/1007/all/?listing=listing',552),
(6504,'104','104',0,'/rossiya/cars/peugeot/104/all/?listing=listing',552),
(6505,'106','106',10,'/rossiya/cars/peugeot/106/all/?listing=listing',552),
(6507,'108','108',0,'/rossiya/cars/peugeot/108/all/?listing=listing',552),
(6508,'2008','2008',41,'/rossiya/cars/peugeot/2008/all/?listing=listing',552),
(6509,'201','201',0,'/rossiya/cars/peugeot/201/all/?listing=listing',552),
(6510,'202','202',0,'/rossiya/cars/peugeot/202/all/?listing=listing',552),
(6511,'203','203',0,'/rossiya/cars/peugeot/203/all/?listing=listing',552),
(6512,'204','204',0,'/rossiya/cars/peugeot/204/all/?listing=listing',552),
(6513,'205','205',2,'/rossiya/cars/peugeot/205/all/?listing=listing',552),
(6514,'205 GTi','205_gti',0,'/rossiya/cars/peugeot/205_gti/all/?listing=listing',552),
(6517,'208','208',49,'/rossiya/cars/peugeot/208/all/?listing=listing',552),
(6518,'208 GTi','208_gti',0,'/rossiya/cars/peugeot/208_gti/all/?listing=listing',552),
(6520,'301','301',52,'/rossiya/cars/peugeot/301/all/?listing=listing',552),
(6521,'304','304',0,'/rossiya/cars/peugeot/304/all/?listing=listing',552),
(6522,'305','305',0,'/rossiya/cars/peugeot/305/all/?listing=listing',552),
(6523,'306','306',23,'/rossiya/cars/peugeot/306/all/?listing=listing',552),
(6526,'308 GTi','308_gti',0,'/rossiya/cars/peugeot/308_gti/all/?listing=listing',552),
(6527,'309','309',2,'/rossiya/cars/peugeot/309/all/?listing=listing',552),
(6529,'4008','4008',34,'/rossiya/cars/peugeot/4008/all/?listing=listing',552),
(6530,'402','402',0,'/rossiya/cars/peugeot/402/all/?listing=listing',552),
(6531,'403','403',0,'/rossiya/cars/peugeot/403/all/?listing=listing',552),
(6532,'404','404',0,'/rossiya/cars/peugeot/404/all/?listing=listing',552),
(6533,'405','405',28,'/rossiya/cars/peugeot/405/all/?listing=listing',552),
(6537,'5008','5008',7,'/rossiya/cars/peugeot/5008/all/?listing=listing',552),
(6538,'504','504',1,'/rossiya/cars/peugeot/504/all/?listing=listing',552),
(6539,'505','505',2,'/rossiya/cars/peugeot/505/all/?listing=listing',552),
(6540,'508','508',64,'/rossiya/cars/peugeot/508/all/?listing=listing',552),
(6541,'604','604',0,'/rossiya/cars/peugeot/604/all/?listing=listing',552),
(6542,'605','605',23,'/rossiya/cars/peugeot/605/all/?listing=listing',552),
(6543,'607','607',56,'/rossiya/cars/peugeot/607/all/?listing=listing',552),
(6544,'806','806',3,'/rossiya/cars/peugeot/806/all/?listing=listing',552),
(6545,'807','807',9,'/rossiya/cars/peugeot/807/all/?listing=listing',552),
(6546,'Bipper','bipper',4,'/rossiya/cars/peugeot/bipper/all/?listing=listing',552),
(6548,'iOn','ion',0,'/rossiya/cars/peugeot/ion/all/?listing=listing',552),
(6550,'RCZ','rcz',13,'/rossiya/cars/peugeot/rcz/all/?listing=listing',552),
(6551,'Traveller','traveller',0,'/rossiya/cars/peugeot/traveller/all/?listing=listing',552),
(6552,'Duster','duster',2816,'/rossiya/cars/renault/duster/all/?listing=listing',553),
(6553,'Fluence','fluence',605,'/rossiya/cars/renault/fluence/all/?listing=listing',553),
(6554,'Kangoo','kangoo',297,'/rossiya/cars/renault/kangoo/all/?listing=listing',553),
(6555,'Kaptur','kaptur',573,'/rossiya/cars/renault/kaptur/all/?listing=listing',553),
(6556,'Koleos','koleos',182,'/rossiya/cars/renault/koleos/all/?listing=listing',553),
(6557,'Laguna','laguna',399,'/rossiya/cars/renault/laguna/all/?listing=listing',553),
(6558,'Logan','logan',3091,'/rossiya/cars/renault/logan/all/?listing=listing',553),
(6559,'Megane','megane',1560,'/rossiya/cars/renault/megane/all/?listing=listing',553),
(6560,'Sandero','sandero',2009,'/rossiya/cars/renault/sandero/all/?listing=listing',553),
(6561,'Scenic','scenic',570,'/rossiya/cars/renault/scenic/all/?listing=listing',553),
(6562,'Symbol','clio_symbol',457,'/rossiya/cars/renault/clio_symbol/all/?listing=listing',553),
(6563,'10','10',0,'/rossiya/cars/renault/10/all/?listing=listing',553),
(6564,'11','11',3,'/rossiya/cars/renault/11/all/?listing=listing',553),
(6565,'12','12',0,'/rossiya/cars/renault/12/all/?listing=listing',553),
(6566,'14','14',0,'/rossiya/cars/renault/14/all/?listing=listing',553),
(6567,'15','15',0,'/rossiya/cars/renault/15/all/?listing=listing',553),
(6568,'16','16',0,'/rossiya/cars/renault/16/all/?listing=listing',553),
(6569,'17','17',0,'/rossiya/cars/renault/17/all/?listing=listing',553),
(6570,'18','18',0,'/rossiya/cars/renault/18/all/?listing=listing',553),
(6571,'19','19',69,'/rossiya/cars/renault/19/all/?listing=listing',553),
(6572,'20','20',0,'/rossiya/cars/renault/20/all/?listing=listing',553),
(6573,'21','21',3,'/rossiya/cars/renault/21/all/?listing=listing',553),
(6574,'25','25',1,'/rossiya/cars/renault/25/all/?listing=listing',553),
(6575,'30','30',0,'/rossiya/cars/renault/30/all/?listing=listing',553),
(6576,'4','4',0,'/rossiya/cars/renault/4/all/?listing=listing',553),
(6577,'4CV','4cv',0,'/rossiya/cars/renault/4cv/all/?listing=listing',553),
(6580,'8','8',0,'/rossiya/cars/renault/8/all/?listing=listing',553),
(6581,'9','9',1,'/rossiya/cars/renault/9/all/?listing=listing',553),
(6582,'Alaskan','alaskan',0,'/rossiya/cars/renault/alaskan/all/?listing=listing',553),
(6583,'Avantime','avantime',1,'/rossiya/cars/renault/avantime/all/?listing=listing',553),
(6584,'Captur','captur',1,'/rossiya/cars/renault/captur/all/?listing=listing',553),
(6585,'Caravelle','caravelle',0,'/rossiya/cars/renault/caravelle/all/?listing=listing',553),
(6586,'Clio','clio',177,'/rossiya/cars/renault/clio/all/?listing=listing',553),
(6587,'Clio RS','clio_rs',13,'/rossiya/cars/renault/clio_rs/all/?listing=listing',553),
(6588,'Clio V6','clio_v6',0,'/rossiya/cars/renault/clio_v6/all/?listing=listing',553),
(6589,'Dauphine','dauphine',0,'/rossiya/cars/renault/dauphine/all/?listing=listing',553),
(6590,'Dokker','dokker',0,'/rossiya/cars/renault/dokker/all/?listing=listing',553),
(6592,'Espace','espace',51,'/rossiya/cars/renault/espace/all/?listing=listing',553),
(6593,'Floride','floride',0,'/rossiya/cars/renault/floride/all/?listing=listing',553),
(6595,'Fregate','fregate',0,'/rossiya/cars/renault/fregate/all/?listing=listing',553),
(6596,'Fuego','fuego',0,'/rossiya/cars/renault/fuego/all/?listing=listing',553),
(6597,'Kadjar','kadjar',0,'/rossiya/cars/renault/kadjar/all/?listing=listing',553),
(6601,'KWID','kwid',0,'/rossiya/cars/renault/kwid/all/?listing=listing',553),
(6603,'Latitude','latitude',42,'/rossiya/cars/renault/latitude/all/?listing=listing',553),
(6604,'Lodgy','lodgy',0,'/rossiya/cars/renault/lodgy/all/?listing=listing',553),
(6607,'Megane RS','megane_rs',7,'/rossiya/cars/renault/megane_rs/all/?listing=listing',553),
(6608,'Modus','modus',10,'/rossiya/cars/renault/modus/all/?listing=listing',553),
(6609,'Rodeo','rodeo',0,'/rossiya/cars/renault/rodeo/all/?listing=listing',553),
(6610,'Safrane','safrane',30,'/rossiya/cars/renault/safrane/all/?listing=listing',553),
(6612,'Sandero RS','sandero_rs',0,'/rossiya/cars/renault/sandero_rs/all/?listing=listing',553),
(6614,'Sport Spider','sport_spyder',0,'/rossiya/cars/renault/sport_spyder/all/?listing=listing',553),
(6616,'Talisman','talisman',0,'/rossiya/cars/renault/talisman/all/?listing=listing',553),
(6617,'Trafic','trafic',68,'/rossiya/cars/renault/trafic/all/?listing=listing',553),
(6618,'Twingo','twingo',11,'/rossiya/cars/renault/twingo/all/?listing=listing',553),
(6619,'Twizy','twizy',1,'/rossiya/cars/renault/twizy/all/?listing=listing',553),
(6620,'Vel Satis','vel_satis',21,'/rossiya/cars/renault/vel_satis/all/?listing=listing',553),
(6621,'Vivastella','vivastella',0,'/rossiya/cars/renault/vivastella/all/?listing=listing',553),
(6622,'Wind','wind',0,'/rossiya/cars/renault/wind/all/?listing=listing',553),
(6623,'ZOE','zoe',0,'/rossiya/cars/renault/zoe/all/?listing=listing',553),
(6624,'Corniche','corniche',2,'/rossiya/cars/rolls_royce/corniche/all/?listing=listing',554),
(6625,'Dawn','dawn',1,'/rossiya/cars/rolls_royce/dawn/all/?listing=listing',554),
(6626,'Ghost','ghost',25,'/rossiya/cars/rolls_royce/ghost/all/?listing=listing',554),
(6627,'Park Ward','park_ward',0,'/rossiya/cars/rolls_royce/park_ward/all/?listing=listing',554),
(6628,'Phantom','phantom',32,'/rossiya/cars/rolls_royce/phantom/all/?listing=listing',554),
(6629,'Silver Cloud','silver_cloud',2,'/rossiya/cars/rolls_royce/silver_cloud/all/?listing=listing',554),
(6630,'Silver Ghost','silver_ghost',0,'/rossiya/cars/rolls_royce/silver_ghost/all/?listing=listing',554),
(6631,'Silver Seraph','silver_seraph',0,'/rossiya/cars/rolls_royce/silver_seraph/all/?listing=listing',554),
(6632,'Silver Shadow','silver_shadow',0,'/rossiya/cars/rolls_royce/silver_shadow/all/?listing=listing',554),
(6633,'Silver Spur','silver_spur',3,'/rossiya/cars/rolls_royce/silver_spur/all/?listing=listing',554),
(6634,'Wraith','wraith',29,'/rossiya/cars/rolls_royce/wraith/all/?listing=listing',554),
(6635,'Camargue','camargue',0,'/rossiya/cars/rolls_royce/camargue/all/?listing=listing',554),
(6642,'Silver Dawn','silver_dawn',0,'/rossiya/cars/rolls_royce/silver_dawn/all/?listing=listing',554),
(6647,'Silver Wraith','silver_wraith',0,'/rossiya/cars/rolls_royce/silver_wraith/all/?listing=listing',554),
(6649,'100 Series','100_series',1,'/rossiya/cars/skoda/100_series/all/?listing=listing',555),
(6650,'Fabia','fabia',981,'/rossiya/cars/skoda/fabia/all/?listing=listing',555),
(6651,'Fabia RS','fabia_rs',16,'/rossiya/cars/skoda/fabia_rs/all/?listing=listing',555),
(6652,'Favorit','favorit',1,'/rossiya/cars/skoda/favorit/all/?listing=listing',555),
(6653,'Felicia','felicia',154,'/rossiya/cars/skoda/felicia/all/?listing=listing',555),
(6654,'Octavia','octavia',3838,'/rossiya/cars/skoda/octavia/all/?listing=listing',555),
(6655,'Octavia RS','octavia_rs',96,'/rossiya/cars/skoda/octavia_rs/all/?listing=listing',555),
(6656,'Rapid','rapid',1040,'/rossiya/cars/skoda/rapid/all/?listing=listing',555),
(6657,'Roomster','roomster',98,'/rossiya/cars/skoda/roomster/all/?listing=listing',555),
(6658,'Superb','superb',655,'/rossiya/cars/skoda/superb/all/?listing=listing',555),
(6659,'Yeti','yeti',1056,'/rossiya/cars/skoda/yeti/all/?listing=listing',555),
(6661,'Citigo','citigo',0,'/rossiya/cars/skoda/citigo/all/?listing=listing',555),
(6668,'Popular','popular',0,'/rossiya/cars/skoda/popular/all/?listing=listing',555),
(6673,'Actyon','actyon',694,'/rossiya/cars/ssang_yong/actyon/all/?listing=listing',556),
(6674,'Actyon Sports','actyon_sport',142,'/rossiya/cars/ssang_yong/actyon_sport/all/?listing=listing',556),
(6676,'Korando Family','korando_family',4,'/rossiya/cars/ssang_yong/korando_family/all/?listing=listing',556),
(6677,'Korando Sports','korando_sports',4,'/rossiya/cars/ssang_yong/korando_sports/all/?listing=listing',556),
(6678,'Kyron','kyron',665,'/rossiya/cars/ssang_yong/kyron/all/?listing=listing',556),
(6680,'Rexton','rexton',298,'/rossiya/cars/ssang_yong/rexton/all/?listing=listing',556),
(6681,'Rodius','rodius',8,'/rossiya/cars/ssang_yong/rodius/all/?listing=listing',556),
(6682,'Stavic','stavic',13,'/rossiya/cars/ssang_yong/stavic/all/?listing=listing',556),
(6683,'Tivoli','tivoli',2,'/rossiya/cars/ssang_yong/tivoli/all/?listing=listing',556),
(6687,'Kallista','kallista',0,'/rossiya/cars/ssang_yong/kallista/all/?listing=listing',556),
(6693,'Nomad','nomad',0,'/rossiya/cars/ssang_yong/nomad/all/?listing=listing',556),
(6698,'Forester','forester',1074,'/rossiya/cars/subaru/forester/all/?listing=listing',557),
(6699,'Impreza','impreza',512,'/rossiya/cars/subaru/impreza/all/?listing=listing',557),
(6700,'Impreza WRX','impreza_wrx',140,'/rossiya/cars/subaru/impreza_wrx/all/?listing=listing',557),
(6701,'Impreza WRX STi','impreza_wrx_sti',121,'/rossiya/cars/subaru/impreza_wrx_sti/all/?listing=listing',557),
(6702,'Legacy','legacy',469,'/rossiya/cars/subaru/legacy/all/?listing=listing',557),
(6703,'Leone','leone',11,'/rossiya/cars/subaru/leone/all/?listing=listing',557),
(6704,'Outback','outback',401,'/rossiya/cars/subaru/outback/all/?listing=listing',557),
(6705,'Pleo','pleo',9,'/rossiya/cars/subaru/pleo/all/?listing=listing',557),
(6706,'R2','r2',15,'/rossiya/cars/subaru/r2/all/?listing=listing',557),
(6707,'Tribeca','b9_tribeca',125,'/rossiya/cars/subaru/b9_tribeca/all/?listing=listing',557),
(6708,'XV','xv',100,'/rossiya/cars/subaru/xv/all/?listing=listing',557),
(6709,'360','360',0,'/rossiya/cars/subaru/360/all/?listing=listing',557),
(6710,'Alcyone','alcyone',0,'/rossiya/cars/subaru/alcyone/all/?listing=listing',557),
(6711,'Baja','baja',5,'/rossiya/cars/subaru/baja/all/?listing=listing',557),
(6712,'BRZ','brz',6,'/rossiya/cars/subaru/brz/all/?listing=listing',557),
(6713,'Dex','dex',0,'/rossiya/cars/subaru/dex/all/?listing=listing',557),
(6714,'Domingo','domingo',0,'/rossiya/cars/subaru/domingo/all/?listing=listing',557),
(6715,'Exiga','exiga',3,'/rossiya/cars/subaru/exiga/all/?listing=listing',557),
(6720,'Justy','justy',8,'/rossiya/cars/subaru/justy/all/?listing=listing',557),
(6723,'Levorg','levorg',0,'/rossiya/cars/subaru/levorg/all/?listing=listing',557),
(6725,'Lucra','lucra',0,'/rossiya/cars/subaru/lucra/all/?listing=listing',557),
(6728,'R1','r1',2,'/rossiya/cars/subaru/r1/all/?listing=listing',557),
(6730,'Sambar','sambar',2,'/rossiya/cars/subaru/sambar/all/?listing=listing',557),
(6731,'Stella','stella',3,'/rossiya/cars/subaru/stella/all/?listing=listing',557),
(6732,'SVX','svx',2,'/rossiya/cars/subaru/svx/all/?listing=listing',557),
(6733,'Traviq','traviq',1,'/rossiya/cars/subaru/traviq/all/?listing=listing',557),
(6734,'Trezia','trezia',1,'/rossiya/cars/subaru/trezia/all/?listing=listing',557),
(6736,'Vivio','vivio',3,'/rossiya/cars/subaru/vivio/all/?listing=listing',557),
(6737,'WRX','wrx',4,'/rossiya/cars/subaru/wrx/all/?listing=listing',557),
(6738,'WRX STi','wrx_sti',5,'/rossiya/cars/subaru/wrx_sti/all/?listing=listing',557),
(6739,'XT','xt',0,'/rossiya/cars/subaru/xt/all/?listing=listing',557),
(6741,'Baleno','baleno',64,'/rossiya/cars/suzuki/baleno/all/?listing=listing',558),
(6742,'Escudo','escudo',27,'/rossiya/cars/suzuki/escudo/all/?listing=listing',558),
(6743,'Grand Vitara','grand_vitara',1122,'/rossiya/cars/suzuki/grand_vitara/all/?listing=listing',558),
(6744,'Ignis','ignis',64,'/rossiya/cars/suzuki/ignis/all/?listing=listing',558),
(6745,'Jimny','jimny',155,'/rossiya/cars/suzuki/jimny/all/?listing=listing',558),
(6746,'Liana','liana',212,'/rossiya/cars/suzuki/liana/all/?listing=listing',558),
(6747,'SX4','sx4',465,'/rossiya/cars/suzuki/sx4/all/?listing=listing',558),
(6748,'Splash','splash',27,'/rossiya/cars/suzuki/splash/all/?listing=listing',558),
(6749,'Swift','swift',221,'/rossiya/cars/suzuki/swift/all/?listing=listing',558),
(6750,'Vitara','vitara',307,'/rossiya/cars/suzuki/vitara/all/?listing=listing',558),
(6751,'Wagon R','wagon_r',39,'/rossiya/cars/suzuki/wagon_r/all/?listing=listing',558),
(6752,'Aerio','aerio',19,'/rossiya/cars/suzuki/aerio/all/?listing=listing',558),
(6753,'Alto','alto',23,'/rossiya/cars/suzuki/alto/all/?listing=listing',558),
(6755,'Cappuccino','cappuccino',0,'/rossiya/cars/suzuki/cappuccino/all/?listing=listing',558),
(6756,'Celerio','celerio',0,'/rossiya/cars/suzuki/celerio/all/?listing=listing',558),
(6757,'Cervo','cervo_classic',1,'/rossiya/cars/suzuki/cervo_classic/all/?listing=listing',558),
(6758,'Ertiga','ertiga',0,'/rossiya/cars/suzuki/ertiga/all/?listing=listing',558),
(6760,'Every','every',4,'/rossiya/cars/suzuki/every/all/?listing=listing',558),
(6761,'Forenza','forenza',4,'/rossiya/cars/suzuki/forenza/all/?listing=listing',558),
(6765,'Kei','kei',6,'/rossiya/cars/suzuki/kei/all/?listing=listing',558),
(6766,'Kizashi','kizashi',13,'/rossiya/cars/suzuki/kizashi/all/?listing=listing',558),
(6767,'Landy','landy',4,'/rossiya/cars/suzuki/landy/all/?listing=listing',558),
(6769,'MR Wagon','mr_wagon',2,'/rossiya/cars/suzuki/mr_wagon/all/?listing=listing',558),
(6770,'Palette','palette',5,'/rossiya/cars/suzuki/palette/all/?listing=listing',558),
(6771,'Reno','reno',2,'/rossiya/cars/suzuki/reno/all/?listing=listing',558),
(6772,'Samurai','samurai',14,'/rossiya/cars/suzuki/samurai/all/?listing=listing',558),
(6773,'Solio','solio',4,'/rossiya/cars/suzuki/solio/all/?listing=listing',558),
(6774,'Spacia','spacia',0,'/rossiya/cars/suzuki/spacia/all/?listing=listing',558),
(6778,'Twin','twin',0,'/rossiya/cars/suzuki/twin/all/?listing=listing',558),
(6779,'Verona','verona',0,'/rossiya/cars/suzuki/verona/all/?listing=listing',558),
(6782,'Wagon R+','wagon_r_plus',0,'/rossiya/cars/suzuki/wagon_r_plus/all/?listing=listing',558),
(6783,'X-90','x_90',0,'/rossiya/cars/suzuki/x_90/all/?listing=listing',558),
(6784,'XL7','xl_7',2,'/rossiya/cars/suzuki/xl_7/all/?listing=listing',558),
(6785,'Model S','model_s',30,'/rossiya/cars/tesla/model_s/all/?listing=listing',559),
(6786,'Model X','model_x',10,'/rossiya/cars/tesla/model_x/all/?listing=listing',559),
(6788,'Auris','auris',356,'/rossiya/cars/toyota/auris/all/?listing=listing',560),
(6789,'Avensis','avensis',865,'/rossiya/cars/toyota/avensis/all/?listing=listing',560),
(6790,'Camry','camry',3207,'/rossiya/cars/toyota/camry/all/?listing=listing',560),
(6791,'Carina','carina',330,'/rossiya/cars/toyota/carina/all/?listing=listing',560),
(6792,'Corolla','corolla',2403,'/rossiya/cars/toyota/corolla/all/?listing=listing',560),
(6793,'Highlander','highlander',374,'/rossiya/cars/toyota/highlander/all/?listing=listing',560),
(6794,'Hilux','hilux',289,'/rossiya/cars/toyota/hilux/all/?listing=listing',560),
(6795,'Land Cruiser','land_cruiser',2259,'/rossiya/cars/toyota/land_cruiser/all/?listing=listing',560),
(6796,'Land Cruiser Prado','land_cruiser_prado',1678,'/rossiya/cars/toyota/land_cruiser_prado/all/?listing=listing',560),
(6797,'Mark II','mark_ii',303,'/rossiya/cars/toyota/mark_ii/all/?listing=listing',560),
(6798,'RAV 4','rav_4',2476,'/rossiya/cars/toyota/rav_4/all/?listing=listing',560),
(6799,'2000GT','2000gt',0,'/rossiya/cars/toyota/2000gt/all/?listing=listing',560),
(6800,'4Runner','4runner',95,'/rossiya/cars/toyota/4runner/all/?listing=listing',560),
(6801,'Allex','allex',22,'/rossiya/cars/toyota/allex/all/?listing=listing',560),
(6802,'Allion','allion',44,'/rossiya/cars/toyota/allion/all/?listing=listing',560),
(6803,'Alphard','alphard',81,'/rossiya/cars/toyota/alphard/all/?listing=listing',560),
(6804,'Altezza','altezza',44,'/rossiya/cars/toyota/altezza/all/?listing=listing',560),
(6805,'Aqua','aqua',11,'/rossiya/cars/toyota/aqua/all/?listing=listing',560),
(6806,'Aristo','aristo',14,'/rossiya/cars/toyota/aristo/all/?listing=listing',560),
(6807,'Aurion','aurion',0,'/rossiya/cars/toyota/aurion/all/?listing=listing',560),
(6809,'Avalon','avalon',14,'/rossiya/cars/toyota/avalon/all/?listing=listing',560),
(6811,'Avensis Verso','avensis_verso',14,'/rossiya/cars/toyota/avensis_verso/all/?listing=listing',560),
(6812,'Aygo','aygo',21,'/rossiya/cars/toyota/aygo/all/?listing=listing',560),
(6813,'bB','bb',35,'/rossiya/cars/toyota/bb/all/?listing=listing',560),
(6814,'Belta','belta',10,'/rossiya/cars/toyota/belta/all/?listing=listing',560),
(6815,'Blade','blade',1,'/rossiya/cars/toyota/blade/all/?listing=listing',560),
(6816,'Blizzard','blizzard',0,'/rossiya/cars/toyota/blizzard/all/?listing=listing',560),
(6817,'Brevis','brevis',8,'/rossiya/cars/toyota/brevis/all/?listing=listing',560),
(6818,'Caldina','caldina',123,'/rossiya/cars/toyota/caldina/all/?listing=listing',560),
(6819,'Cami','cami',9,'/rossiya/cars/toyota/cami/all/?listing=listing',560),
(6821,'Camry (Japan)','camry_japan',166,'/rossiya/cars/toyota/camry_japan/all/?listing=listing',560),
(6822,'Camry Solara','camry_solara',27,'/rossiya/cars/toyota/camry_solara/all/?listing=listing',560),
(6824,'Carina ED','carina_ed',27,'/rossiya/cars/toyota/carina_ed/all/?listing=listing',560),
(6826,'Celica','celica',194,'/rossiya/cars/toyota/celica/all/?listing=listing',560),
(6827,'Celsior','celsior',3,'/rossiya/cars/toyota/celsior/all/?listing=listing',560),
(6828,'Century','century',0,'/rossiya/cars/toyota/century/all/?listing=listing',560),
(6829,'Chaser','chaser',91,'/rossiya/cars/toyota/chaser/all/?listing=listing',560),
(6831,'Corolla Rumion','corolla_rumion',5,'/rossiya/cars/toyota/corolla_rumion/all/?listing=listing',560),
(6832,'Corolla Spacio','corolla_spacio',42,'/rossiya/cars/toyota/corolla_spacio/all/?listing=listing',560),
(6833,'Corolla Verso','corolla_verso',47,'/rossiya/cars/toyota/corolla_verso/all/?listing=listing',560),
(6834,'Corona','corona',152,'/rossiya/cars/toyota/corona/all/?listing=listing',560),
(6836,'Cressida','cressida',0,'/rossiya/cars/toyota/cressida/all/?listing=listing',560),
(6837,'Cresta','cresta',76,'/rossiya/cars/toyota/cresta/all/?listing=listing',560),
(6838,'Crown','crown',121,'/rossiya/cars/toyota/crown/all/?listing=listing',560),
(6839,'Crown Majesta','crown_majesta',7,'/rossiya/cars/toyota/crown_majesta/all/?listing=listing',560),
(6840,'Curren','curren',10,'/rossiya/cars/toyota/curren/all/?listing=listing',560),
(6841,'Cynos','cynos',21,'/rossiya/cars/toyota/cynos/all/?listing=listing',560),
(6842,'Duet','duet',19,'/rossiya/cars/toyota/duet/all/?listing=listing',560),
(6843,'Echo','echo',37,'/rossiya/cars/toyota/echo/all/?listing=listing',560),
(6844,'Estima','estima',77,'/rossiya/cars/toyota/estima/all/?listing=listing',560),
(6845,'FJ Cruiser','fj_cruiser',31,'/rossiya/cars/toyota/fj_cruiser/all/?listing=listing',560),
(6846,'Fortuner','fortuner',27,'/rossiya/cars/toyota/fortuner/all/?listing=listing',560),
(6847,'FunCargo','funcargo',28,'/rossiya/cars/toyota/funcargo/all/?listing=listing',560),
(6848,'Gaia','gaia',13,'/rossiya/cars/toyota/gaia/all/?listing=listing',560),
(6849,'Granvia','granvia',0,'/rossiya/cars/toyota/granvia/all/?listing=listing',560),
(6850,'GT86','gt86',17,'/rossiya/cars/toyota/gt86/all/?listing=listing',560),
(6851,'Harrier','harrier',66,'/rossiya/cars/toyota/harrier/all/?listing=listing',560),
(6852,'HiAce','hiace',118,'/rossiya/cars/toyota/hiace/all/?listing=listing',560),
(6855,'Hilux Surf','hilux_surf',50,'/rossiya/cars/toyota/hilux_surf/all/?listing=listing',560),
(6856,'Innova','innova',0,'/rossiya/cars/toyota/innova/all/?listing=listing',560),
(6857,'Ipsum','ipsum',47,'/rossiya/cars/toyota/ipsum/all/?listing=listing',560),
(6858,'iQ','iq',9,'/rossiya/cars/toyota/iq/all/?listing=listing',560),
(6859,'ISis','isis',26,'/rossiya/cars/toyota/isis/all/?listing=listing',560),
(6860,'Ist','ist',35,'/rossiya/cars/toyota/ist/all/?listing=listing',560),
(6861,'Kluger','kluger',9,'/rossiya/cars/toyota/kluger/all/?listing=listing',560),
(6864,'LiteAce','lite_ace',24,'/rossiya/cars/toyota/lite_ace/all/?listing=listing',560),
(6866,'Mark X','mark_x',28,'/rossiya/cars/toyota/mark_x/all/?listing=listing',560),
(6867,'Mark X ZiO','mark_x_zio',1,'/rossiya/cars/toyota/mark_x_zio/all/?listing=listing',560),
(6868,'MasterAce Surf','master_ace_surf',8,'/rossiya/cars/toyota/master_ace_surf/all/?listing=listing',560),
(6870,'Mega Cruiser','mega_cruiser',3,'/rossiya/cars/toyota/mega_cruiser/all/?listing=listing',560),
(6871,'Mirai','mirai',0,'/rossiya/cars/toyota/mirai/all/?listing=listing',560),
(6872,'MR2','mr2',17,'/rossiya/cars/toyota/mr2/all/?listing=listing',560),
(6873,'Nadia','nadia',21,'/rossiya/cars/toyota/nadia/all/?listing=listing',560),
(6874,'Noah','noah',39,'/rossiya/cars/toyota/noah/all/?listing=listing',560),
(6875,'Opa','opa',23,'/rossiya/cars/toyota/opa/all/?listing=listing',560),
(6876,'Origin','origin',0,'/rossiya/cars/toyota/origin/all/?listing=listing',560),
(6877,'Paseo','paseo',3,'/rossiya/cars/toyota/paseo/all/?listing=listing',560),
(6878,'Passo','passo',55,'/rossiya/cars/toyota/passo/all/?listing=listing',560),
(6879,'Passo Sette','passo_sette',2,'/rossiya/cars/toyota/passo_sette/all/?listing=listing',560),
(6880,'Picnic','picnic',13,'/rossiya/cars/toyota/picnic/all/?listing=listing',560),
(6881,'Pixis Epoch','pixis_epoch',0,'/rossiya/cars/toyota/pixis_epoch/all/?listing=listing',560),
(6882,'Pixis Mega','pixis_mega',0,'/rossiya/cars/toyota/pixis_mega/all/?listing=listing',560),
(6883,'Pixis Space','pixis_space',0,'/rossiya/cars/toyota/pixis_space/all/?listing=listing',560),
(6884,'Platz','platz',53,'/rossiya/cars/toyota/platz/all/?listing=listing',560),
(6885,'Porte','porte',9,'/rossiya/cars/toyota/porte/all/?listing=listing',560),
(6886,'Premio','premio',27,'/rossiya/cars/toyota/premio/all/?listing=listing',560),
(6887,'Previa','previa',20,'/rossiya/cars/toyota/previa/all/?listing=listing',560),
(6888,'Prius','prius',236,'/rossiya/cars/toyota/prius/all/?listing=listing',560),
(6889,'Prius c','prius_c',2,'/rossiya/cars/toyota/prius_c/all/?listing=listing',560),
(6890,'Prius v (+)','priusplus',6,'/rossiya/cars/toyota/priusplus/all/?listing=listing',560),
(6891,'ProAce','proace',0,'/rossiya/cars/toyota/proace/all/?listing=listing',560),
(6892,'Probox','probox',25,'/rossiya/cars/toyota/probox/all/?listing=listing',560),
(6893,'Progres','progres',5,'/rossiya/cars/toyota/progres/all/?listing=listing',560),
(6894,'Pronard','pronard',1,'/rossiya/cars/toyota/pronard/all/?listing=listing',560),
(6895,'Publica','publica',0,'/rossiya/cars/toyota/publica/all/?listing=listing',560),
(6896,'Ractis','ractis',31,'/rossiya/cars/toyota/ractis/all/?listing=listing',560),
(6897,'Raum','raum',17,'/rossiya/cars/toyota/raum/all/?listing=listing',560),
(6899,'Regius','regius',12,'/rossiya/cars/toyota/regius/all/?listing=listing',560),
(6900,'RegiusAce','regiusace',0,'/rossiya/cars/toyota/regiusace/all/?listing=listing',560),
(6901,'Rush','rush',7,'/rossiya/cars/toyota/rush/all/?listing=listing',560),
(6902,'Sai','sai',0,'/rossiya/cars/toyota/sai/all/?listing=listing',560),
(6903,'Scepter','scepter_sedan',2,'/rossiya/cars/toyota/scepter_sedan/all/?listing=listing',560),
(6904,'Sequoia','sequoia',80,'/rossiya/cars/toyota/sequoia/all/?listing=listing',560),
(6905,'Sera','sera',4,'/rossiya/cars/toyota/sera/all/?listing=listing',560),
(6906,'Sienna','sienna',58,'/rossiya/cars/toyota/sienna/all/?listing=listing',560),
(6907,'Sienta','sienta',10,'/rossiya/cars/toyota/sienta/all/?listing=listing',560),
(6908,'Soarer','soarer',13,'/rossiya/cars/toyota/soarer/all/?listing=listing',560),
(6909,'Soluna','soluna',0,'/rossiya/cars/toyota/soluna/all/?listing=listing',560),
(6910,'Sparky','sparky',3,'/rossiya/cars/toyota/sparky/all/?listing=listing',560),
(6911,'Sprinter','sprinter',72,'/rossiya/cars/toyota/sprinter/all/?listing=listing',560),
(6912,'Sprinter Carib','sprinter_carib',20,'/rossiya/cars/toyota/sprinter_carib/all/?listing=listing',560),
(6913,'Sprinter Marino','sprinter_marino',20,'/rossiya/cars/toyota/sprinter_marino/all/?listing=listing',560),
(6914,'Sprinter Trueno','sprinter_trueno',12,'/rossiya/cars/toyota/sprinter_trueno/all/?listing=listing',560),
(6915,'Starlet','starlet',26,'/rossiya/cars/toyota/starlet/all/?listing=listing',560),
(6916,'Succeed','succeed',20,'/rossiya/cars/toyota/succeed/all/?listing=listing',560),
(6917,'Supra','supra',12,'/rossiya/cars/toyota/supra/all/?listing=listing',560),
(6918,'Tacoma','tacoma',38,'/rossiya/cars/toyota/tacoma/all/?listing=listing',560),
(6919,'Tercel','tercel',20,'/rossiya/cars/toyota/tercel/all/?listing=listing',560),
(6920,'TownAce','town_ace',78,'/rossiya/cars/toyota/town_ace/all/?listing=listing',560),
(6921,'Tundra','tundra',256,'/rossiya/cars/toyota/tundra/all/?listing=listing',560),
(6922,'Urban Cruiser','urban_cruiser',3,'/rossiya/cars/toyota/urban_cruiser/all/?listing=listing',560),
(6923,'Vanguard','vanguard',2,'/rossiya/cars/toyota/vanguard/all/?listing=listing',560),
(6924,'Vellfire','vellfire',11,'/rossiya/cars/toyota/vellfire/all/?listing=listing',560),
(6925,'Venza','venza',120,'/rossiya/cars/toyota/venza/all/?listing=listing',560),
(6926,'Verossa','verossa',10,'/rossiya/cars/toyota/verossa/all/?listing=listing',560),
(6927,'Verso','verso',86,'/rossiya/cars/toyota/verso/all/?listing=listing',560),
(6928,'Verso-S','verso_s',0,'/rossiya/cars/toyota/verso_s/all/?listing=listing',560),
(6929,'Vios','vios',2,'/rossiya/cars/toyota/vios/all/?listing=listing',560),
(6930,'Vista','vista',111,'/rossiya/cars/toyota/vista/all/?listing=listing',560),
(6931,'Vitz','vitz',179,'/rossiya/cars/toyota/vitz/all/?listing=listing',560),
(6932,'Voltz','voltz',6,'/rossiya/cars/toyota/voltz/all/?listing=listing',560),
(6933,'Voxy','voxy',40,'/rossiya/cars/toyota/voxy/all/?listing=listing',560),
(6934,'WiLL','will',21,'/rossiya/cars/toyota/will/all/?listing=listing',560),
(6935,'WiLL Cypha','will_cypha',6,'/rossiya/cars/toyota/will_cypha/all/?listing=listing',560),
(6936,'Windom','windom',19,'/rossiya/cars/toyota/windom/all/?listing=listing',560),
(6937,'Wish','wish',41,'/rossiya/cars/toyota/wish/all/?listing=listing',560),
(6938,'Yaris','yaris',265,'/rossiya/cars/toyota/yaris/all/?listing=listing',560),
(6939,'Yaris Verso','yaris_verso',6,'/rossiya/cars/toyota/yaris_verso/all/?listing=listing',560),
(6940,'Caddy','caddy',328,'/rossiya/cars/volkswagen/caddy/all/?listing=listing',561),
(6942,'Golf','golf',2359,'/rossiya/cars/volkswagen/golf/all/?listing=listing',561),
(6943,'Jetta','jetta',1884,'/rossiya/cars/volkswagen/jetta/all/?listing=listing',561),
(6944,'Multivan','multivan',374,'/rossiya/cars/volkswagen/multivan/all/?listing=listing',561),
(6945,'Passat','passat',4901,'/rossiya/cars/volkswagen/passat/all/?listing=listing',561),
(6946,'Passat CC','passat_cc',739,'/rossiya/cars/volkswagen/passat_cc/all/?listing=listing',561),
(6947,'Polo','polo',3242,'/rossiya/cars/volkswagen/polo/all/?listing=listing',561),
(6948,'Tiguan','tiguan',1965,'/rossiya/cars/volkswagen/tiguan/all/?listing=listing',561),
(6949,'Touareg','touareg',1899,'/rossiya/cars/volkswagen/touareg/all/?listing=listing',561),
(6950,'Transporter','transporter',735,'/rossiya/cars/volkswagen/transporter/all/?listing=listing',561),
(6951,'181','181',0,'/rossiya/cars/volkswagen/181/all/?listing=listing',561),
(6952,'Amarok','amarok',234,'/rossiya/cars/volkswagen/amarok/all/?listing=listing',561),
(6953,'Beetle','beetle',99,'/rossiya/cars/volkswagen/beetle/all/?listing=listing',561),
(6954,'Bora','bora',170,'/rossiya/cars/volkswagen/bora/all/?listing=listing',561),
(6958,'Corrado','corrado',2,'/rossiya/cars/volkswagen/corrado/all/?listing=listing',561),
(6959,'Derby','derby',0,'/rossiya/cars/volkswagen/derby/all/?listing=listing',561),
(6960,'Eos','eos',12,'/rossiya/cars/volkswagen/eos/all/?listing=listing',561),
(6961,'Fox','fox',5,'/rossiya/cars/volkswagen/fox/all/?listing=listing',561),
(6963,'Golf Country','golf_country',1,'/rossiya/cars/volkswagen/golf_country/all/?listing=listing',561),
(6964,'Golf GTI','golf_gti',102,'/rossiya/cars/volkswagen/golf_gti/all/?listing=listing',561),
(6965,'Golf Plus','golf_plus',257,'/rossiya/cars/volkswagen/golf_plus/all/?listing=listing',561),
(6966,'Golf R','golf_r',23,'/rossiya/cars/volkswagen/golf_r/all/?listing=listing',561),
(6967,'Golf R32','golf_r32',3,'/rossiya/cars/volkswagen/golf_r32/all/?listing=listing',561),
(6968,'Golf Sportsvan','golf_sportsvan',0,'/rossiya/cars/volkswagen/golf_sportsvan/all/?listing=listing',561),
(6969,'Iltis','iltis',1,'/rossiya/cars/volkswagen/iltis/all/?listing=listing',561),
(6971,'K70','k70',0,'/rossiya/cars/volkswagen/k70/all/?listing=listing',561),
(6972,'Karmann-Ghia','karmann_ghia',1,'/rossiya/cars/volkswagen/karmann_ghia/all/?listing=listing',561),
(6973,'Lupo','lupo',30,'/rossiya/cars/volkswagen/lupo/all/?listing=listing',561),
(6974,'Lupo GTI','lupo_gti',0,'/rossiya/cars/volkswagen/lupo_gti/all/?listing=listing',561),
(6977,'Passat (North America)','passat_na',1,'/rossiya/cars/volkswagen/passat_na/all/?listing=listing',561),
(6979,'Phaeton','phaeton',43,'/rossiya/cars/volkswagen/phaeton/all/?listing=listing',561),
(6980,'Pointer','pointer',79,'/rossiya/cars/volkswagen/pointer/all/?listing=listing',561),
(6982,'Polo GTI','polo_gti',1,'/rossiya/cars/volkswagen/polo_gti/all/?listing=listing',561),
(6983,'Polo R WRC','polo_r_wrc',0,'/rossiya/cars/volkswagen/polo_r_wrc/all/?listing=listing',561),
(6984,'Routan','routan',0,'/rossiya/cars/volkswagen/routan/all/?listing=listing',561),
(6985,'Santana','santana',2,'/rossiya/cars/volkswagen/santana/all/?listing=listing',561),
(6986,'Scirocco','scirocco',110,'/rossiya/cars/volkswagen/scirocco/all/?listing=listing',561),
(6987,'Scirocco R','scirocco_r',1,'/rossiya/cars/volkswagen/scirocco_r/all/?listing=listing',561),
(6988,'Sharan','sharan',166,'/rossiya/cars/volkswagen/sharan/all/?listing=listing',561),
(6989,'Taro','taro',1,'/rossiya/cars/volkswagen/taro/all/?listing=listing',561),
(6992,'Touran','touran',292,'/rossiya/cars/volkswagen/touran/all/?listing=listing',561),
(6994,'Type 1','type_1',5,'/rossiya/cars/volkswagen/type_1/all/?listing=listing',561),
(6995,'Type 2','type_2',1,'/rossiya/cars/volkswagen/type_2/all/?listing=listing',561),
(6996,'Type 4','type_4',0,'/rossiya/cars/volkswagen/type_4/all/?listing=listing',561),
(6997,'up!','up',1,'/rossiya/cars/volkswagen/up/all/?listing=listing',561),
(6998,'Vento','vento',143,'/rossiya/cars/volkswagen/vento/all/?listing=listing',561),
(6999,'XL1','xl1',0,'/rossiya/cars/volkswagen/xl1/all/?listing=listing',561),
(7000,'740','740',54,'/rossiya/cars/volvo/740/all/?listing=listing',562),
(7001,'850','850',95,'/rossiya/cars/volvo/850/all/?listing=listing',562),
(7002,'940','940',68,'/rossiya/cars/volvo/940/all/?listing=listing',562),
(7003,'C30','c30',98,'/rossiya/cars/volvo/c30/all/?listing=listing',562),
(7004,'S40','s40',430,'/rossiya/cars/volvo/s40/all/?listing=listing',562),
(7005,'S60','s60',614,'/rossiya/cars/volvo/s60/all/?listing=listing',562),
(7006,'S80','s80',557,'/rossiya/cars/volvo/s80/all/?listing=listing',562),
(7007,'V70','v70',51,'/rossiya/cars/volvo/v70/all/?listing=listing',562),
(7008,'XC60','xc60',406,'/rossiya/cars/volvo/xc60/all/?listing=listing',562),
(7009,'XC70','xc70',485,'/rossiya/cars/volvo/xc70/all/?listing=listing',562),
(7010,'XC90','xc90',885,'/rossiya/cars/volvo/xc90/all/?listing=listing',562),
(7011,'120 Series','120_series',1,'/rossiya/cars/volvo/120_series/all/?listing=listing',562),
(7012,'140 Series','140',0,'/rossiya/cars/volvo/140/all/?listing=listing',562),
(7013,'164','164',0,'/rossiya/cars/volvo/164/all/?listing=listing',562),
(7014,'240 Series','240_series',12,'/rossiya/cars/volvo/240_series/all/?listing=listing',562),
(7015,'260 Series','260',0,'/rossiya/cars/volvo/260/all/?listing=listing',562),
(7016,'300 Series','300_series',7,'/rossiya/cars/volvo/300_series/all/?listing=listing',562),
(7017,'440','440',22,'/rossiya/cars/volvo/440/all/?listing=listing',562),
(7018,'460','460',33,'/rossiya/cars/volvo/460/all/?listing=listing',562),
(7019,'480','480',2,'/rossiya/cars/volvo/480/all/?listing=listing',562),
(7020,'66','66',2,'/rossiya/cars/volvo/66/all/?listing=listing',562),
(7022,'760','760',9,'/rossiya/cars/volvo/760/all/?listing=listing',562),
(7023,'780','780',0,'/rossiya/cars/volvo/780/all/?listing=listing',562),
(7026,'960','960',39,'/rossiya/cars/volvo/960/all/?listing=listing',562),
(7028,'C70','c70',27,'/rossiya/cars/volvo/c70/all/?listing=listing',562),
(7029,'Laplander','laplander',3,'/rossiya/cars/volvo/laplander/all/?listing=listing',562),
(7030,'P1800','p1800',0,'/rossiya/cars/volvo/p1800/all/?listing=listing',562),
(7031,'P1900','p1900',0,'/rossiya/cars/volvo/p1900/all/?listing=listing',562),
(7034,'S60 Cross Country','s60_cross_country',1,'/rossiya/cars/volvo/s60_cross_country/all/?listing=listing',562),
(7035,'S70','s70',39,'/rossiya/cars/volvo/s70/all/?listing=listing',562),
(7037,'S90','s90',13,'/rossiya/cars/volvo/s90/all/?listing=listing',562),
(7038,'V40','v40',32,'/rossiya/cars/volvo/v40/all/?listing=listing',562),
(7039,'V40 Cross Country','v40_cc',43,'/rossiya/cars/volvo/v40_cc/all/?listing=listing',562),
(7040,'V50','v50',37,'/rossiya/cars/volvo/v50/all/?listing=listing',562),
(7041,'V60','v60',15,'/rossiya/cars/volvo/v60/all/?listing=listing',562),
(7042,'V60 Cross Country','v60_cross_country',8,'/rossiya/cars/volvo/v60_cross_country/all/?listing=listing',562),
(7044,'V90','v90',1,'/rossiya/cars/volvo/v90/all/?listing=listing',562),
(7048,'12 ЗИМ','12',14,'/rossiya/cars/gaz/12/all/?listing=listing',563),
(7049,'14 «Чайка»','14',14,'/rossiya/cars/gaz/14/all/?listing=listing',563),
(7050,'21 «Волга»','21',366,'/rossiya/cars/gaz/21/all/?listing=listing',563),
(7051,'24 «Волга»','24',276,'/rossiya/cars/gaz/24/all/?listing=listing',563),
(7052,'3102 «Волга»','3102',247,'/rossiya/cars/gaz/3102/all/?listing=listing',563),
(7053,'31029 «Волга»','31029',226,'/rossiya/cars/gaz/31029/all/?listing=listing',563),
(7054,'3110 «Волга»','3110',773,'/rossiya/cars/gaz/3110/all/?listing=listing',563),
(7055,'31105 «Волга»','31105',588,'/rossiya/cars/gaz/31105/all/?listing=listing',563),
(7056,'69','69',172,'/rossiya/cars/gaz/69/all/?listing=listing',563),
(7057,'Volga Siber','volga_siber',85,'/rossiya/cars/gaz/volga_siber/all/?listing=listing',563),
(7058,'М-20 «Победа»','m_20',60,'/rossiya/cars/gaz/m_20/all/?listing=listing',563),
(7060,'13 «Чайка»','13',9,'/rossiya/cars/gaz/13/all/?listing=listing',563),
(7063,'22 «Волга»','22',8,'/rossiya/cars/gaz/22/all/?listing=listing',563),
(7064,'2308 «Атаман»','2308_ataman',0,'/rossiya/cars/gaz/2308_ataman/all/?listing=listing',563),
(7065,'2330 «Тигр»','2330_tigr',1,'/rossiya/cars/gaz/2330_tigr/all/?listing=listing',563),
(7069,'3103 «Волга»','3103_volga',0,'/rossiya/cars/gaz/3103_volga/all/?listing=listing',563),
(7070,'3105 «Волга»','3105',0,'/rossiya/cars/gaz/3105/all/?listing=listing',563),
(7073,'3111 «Волга»','3111',2,'/rossiya/cars/gaz/3111/all/?listing=listing',563),
(7074,'67','67',14,'/rossiya/cars/gaz/67/all/?listing=listing',563),
(7078,'М-72','m_72',3,'/rossiya/cars/gaz/m_72/all/?listing=listing',563),
(7079,'М1','m1',2,'/rossiya/cars/gaz/m1/all/?listing=listing',563),
(7080,'3151','3151',519,'/rossiya/cars/uaz/3151/all/?listing=listing',564),
(7081,'3153','3153',24,'/rossiya/cars/uaz/3153/all/?listing=listing',564),
(7082,'3159','3159',18,'/rossiya/cars/uaz/3159/all/?listing=listing',564),
(7083,'3160','3160',12,'/rossiya/cars/uaz/3160/all/?listing=listing',564),
(7084,'3162 Simbir','3162',38,'/rossiya/cars/uaz/3162/all/?listing=listing',564),
(7085,'469','469',206,'/rossiya/cars/uaz/469/all/?listing=listing',564),
(7086,'Hunter','hunter',551,'/rossiya/cars/uaz/hunter/all/?listing=listing',564),
(7087,'Patriot','patriot',1438,'/rossiya/cars/uaz/patriot/all/?listing=listing',564),
(7088,'Pickup','pickup',187,'/rossiya/cars/uaz/pickup/all/?listing=listing',564),
(7089,'378 GT Zagato','378_gt',0,'/rossiya/cars/ac/378_gt/all/?listing=listing',565),
(7090,'Ace','ace',0,'/rossiya/cars/ac/ace/all/?listing=listing',565),
(7091,'Aceca','aceca',0,'/rossiya/cars/ac/aceca/all/?listing=listing',565),
(7092,'Cobra','cobra',2,'/rossiya/cars/ac/cobra/all/?listing=listing',565),
(7096,'RDX','rdx',28,'/rossiya/cars/acura/rdx/all/?listing=listing',566),
(7097,'RL','rl',3,'/rossiya/cars/acura/rl/all/?listing=listing',566),
(7098,'RLX','rlx',4,'/rossiya/cars/acura/rlx/all/?listing=listing',566),
(7099,'RSX','rsx',6,'/rossiya/cars/acura/rsx/all/?listing=listing',566),
(7100,'TL','tl',2,'/rossiya/cars/acura/tl/all/?listing=listing',566),
(7101,'TLX','tlx',1,'/rossiya/cars/acura/tlx/all/?listing=listing',566),
(7102,'TSX','tsx',4,'/rossiya/cars/acura/tsx/all/?listing=listing',566),
(7103,'ZDX','zdx',14,'/rossiya/cars/acura/zdx/all/?listing=listing',566),
(7104,'CL','cl',0,'/rossiya/cars/acura/cl/all/?listing=listing',566),
(7105,'CSX','csx',0,'/rossiya/cars/acura/csx/all/?listing=listing',566),
(7106,'EL','el',0,'/rossiya/cars/acura/el/all/?listing=listing',566),
(7107,'ILX','ilx',0,'/rossiya/cars/acura/ilx/all/?listing=listing',566),
(7116,'SLX','slx',0,'/rossiya/cars/acura/slx/all/?listing=listing',566),
(7121,'Trumpf Junior','trumpf_junior',1,'/rossiya/cars/adler/trumpf_junior/all/?listing=listing',567),
(7122,'146','146',7,'/rossiya/cars/alfa_romeo/146/all/?listing=listing',568),
(7123,'147','147',22,'/rossiya/cars/alfa_romeo/147/all/?listing=listing',568),
(7124,'156','156',92,'/rossiya/cars/alfa_romeo/156/all/?listing=listing',568),
(7125,'159','159',31,'/rossiya/cars/alfa_romeo/159/all/?listing=listing',568),
(7127,'166','166',10,'/rossiya/cars/alfa_romeo/166/all/?listing=listing',568),
(7128,'Brera','brera',9,'/rossiya/cars/alfa_romeo/brera/all/?listing=listing',568),
(7129,'GTV','gtv',7,'/rossiya/cars/alfa_romeo/gtv/all/?listing=listing',568),
(7130,'Giulietta','giulietta',31,'/rossiya/cars/alfa_romeo/giulietta/all/?listing=listing',568),
(7131,'MiTo','mito',16,'/rossiya/cars/alfa_romeo/mito/all/?listing=listing',568),
(7132,'Spider','spider',8,'/rossiya/cars/alfa_romeo/spider/all/?listing=listing',568),
(7133,'105/115','105_115',0,'/rossiya/cars/alfa_romeo/105_115/all/?listing=listing',568),
(7134,'145','145',3,'/rossiya/cars/alfa_romeo/145/all/?listing=listing',568),
(7137,'155','155',4,'/rossiya/cars/alfa_romeo/155/all/?listing=listing',568),
(7142,'1900','1900',0,'/rossiya/cars/alfa_romeo/1900/all/?listing=listing',568),
(7143,'2600','2600',0,'/rossiya/cars/alfa_romeo/2600/all/?listing=listing',568),
(7144,'33','33',3,'/rossiya/cars/alfa_romeo/33/all/?listing=listing',568),
(7145,'4C','4c',1,'/rossiya/cars/alfa_romeo/4c/all/?listing=listing',568),
(7147,'6C','6c',0,'/rossiya/cars/alfa_romeo/6c/all/?listing=listing',568),
(7148,'75','75',2,'/rossiya/cars/alfa_romeo/75/all/?listing=listing',568),
(7149,'8C Competizione','8c_competizione',0,'/rossiya/cars/alfa_romeo/8c_competizione/all/?listing=listing',568),
(7151,'Alfasud','alfasud',0,'/rossiya/cars/alfa_romeo/alfasud/all/?listing=listing',568),
(7152,'Alfetta','alfetta',0,'/rossiya/cars/alfa_romeo/alfetta/all/?listing=listing',568),
(7153,'Arna','arna',0,'/rossiya/cars/alfa_romeo/arna/all/?listing=listing',568),
(7155,'Disco Volante','disco_volante',0,'/rossiya/cars/alfa_romeo/disco_volante/all/?listing=listing',568),
(7156,'Giulia','giulia',0,'/rossiya/cars/alfa_romeo/giulia/all/?listing=listing',568),
(7159,'GTA Coupe','gta',0,'/rossiya/cars/alfa_romeo/gta/all/?listing=listing',568),
(7162,'Montreal','montreal',0,'/rossiya/cars/alfa_romeo/montreal/all/?listing=listing',568),
(7163,'RZ','rz',0,'/rossiya/cars/alfa_romeo/rz/all/?listing=listing',568),
(7165,'Sprint','sprint',0,'/rossiya/cars/alfa_romeo/sprint/all/?listing=listing',568),
(7166,'SZ','sz',0,'/rossiya/cars/alfa_romeo/sz/all/?listing=listing',568),
(7167,'B10','b10',1,'/rossiya/cars/alpina/b10/all/?listing=listing',569),
(7168,'B11','b11',0,'/rossiya/cars/alpina/b11/all/?listing=listing',569),
(7169,'B3','b3',2,'/rossiya/cars/alpina/b3/all/?listing=listing',569),
(7170,'B5','b5',1,'/rossiya/cars/alpina/b5/all/?listing=listing',569),
(7171,'B6','b6',6,'/rossiya/cars/alpina/b6/all/?listing=listing',569),
(7172,'B7','b7',1,'/rossiya/cars/alpina/b7/all/?listing=listing',569),
(7173,'B9','b9',0,'/rossiya/cars/alpina/b9/all/?listing=listing',569),
(7176,'D10','d10',0,'/rossiya/cars/alpina/d10/all/?listing=listing',569),
(7177,'D5','d5',1,'/rossiya/cars/alpina/d5/all/?listing=listing',569),
(7180,'B12','b12',0,'/rossiya/cars/alpina/b12/all/?listing=listing',569),
(7182,'B4','b4',0,'/rossiya/cars/alpina/b4/all/?listing=listing',569),
(7186,'B8','b8',0,'/rossiya/cars/alpina/b8/all/?listing=listing',569),
(7191,'D3','d3',0,'/rossiya/cars/alpina/d3/all/?listing=listing',569),
(7194,'XD3','xd3',0,'/rossiya/cars/alpina/xd3/all/?listing=listing',569),
(7195,'A110','a110',0,'/rossiya/cars/alpine/a110/all/?listing=listing',570),
(7196,'A310','a310',0,'/rossiya/cars/alpine/a310/all/?listing=listing',570),
(7197,'A610','a610',0,'/rossiya/cars/alpine/a610/all/?listing=listing',570),
(7198,'GTA','gta',0,'/rossiya/cars/alpine/gta/all/?listing=listing',570),
(7199,'HMMWV (Humvee)','humvee',0,'/rossiya/cars/am_general/humvee/all/?listing=listing',571),
(7200,'Hornet','hornet',0,'/rossiya/cars/amc/hornet/all/?listing=listing',572),
(7201,'Atom','atom',0,'/rossiya/cars/ariel/atom/all/?listing=listing',573),
(7203,'24','24',1,'/rossiya/cars/aro/24/all/?listing=listing',574),
(7205,'Rocsta','rocsta',1,'/rossiya/cars/asia/rocsta/all/?listing=listing',575),
(7206,'Topic','topic',2,'/rossiya/cars/asia/topic/all/?listing=listing',575),
(7207,'Cygnet','cygnet',0,'/rossiya/cars/aston_martin/cygnet/all/?listing=listing',576),
(7208,'DB7','db7',1,'/rossiya/cars/aston_martin/db7/all/?listing=listing',576),
(7209,'DB9','db9',13,'/rossiya/cars/aston_martin/db9/all/?listing=listing',576),
(7210,'DBS','dbs',2,'/rossiya/cars/aston_martin/dbs/all/?listing=listing',576),
(7211,'Lagonda','lagonda',0,'/rossiya/cars/aston_martin/lagonda/all/?listing=listing',576),
(7212,'Rapide','rapide',4,'/rossiya/cars/aston_martin/rapide/all/?listing=listing',576),
(7213,'Tickford Capri','tickford_capri',0,'/rossiya/cars/aston_martin/tickford_capri/all/?listing=listing',576),
(7214,'V12 Vanquish','v12',6,'/rossiya/cars/aston_martin/v12/all/?listing=listing',576),
(7215,'V12 Vantage','v12_vantage',1,'/rossiya/cars/aston_martin/v12_vantage/all/?listing=listing',576),
(7216,'V8 Vantage','v8',8,'/rossiya/cars/aston_martin/v8/all/?listing=listing',576),
(7217,'Virage','virage',3,'/rossiya/cars/aston_martin/virage/all/?listing=listing',576),
(7218,'Bulldog','bulldog',0,'/rossiya/cars/aston_martin/bulldog/all/?listing=listing',576),
(7220,'DB11','db11',0,'/rossiya/cars/aston_martin/db11/all/?listing=listing',576),
(7221,'DB5','db5',0,'/rossiya/cars/aston_martin/db5/all/?listing=listing',576),
(7226,'One-77','one_77',0,'/rossiya/cars/aston_martin/one_77/all/?listing=listing',576),
(7231,'V12 Zagato','v12_zagato',0,'/rossiya/cars/aston_martin/v12_zagato/all/?listing=listing',576),
(7233,'V8 Zagato','v8_zagato',0,'/rossiya/cars/aston_martin/v8_zagato/all/?listing=listing',576),
(7293,'Allegro','allegro',0,'/rossiya/cars/austin/allegro/all/?listing=listing',578),
(7294,'Ambassador','ambassador',0,'/rossiya/cars/austin/ambassador/all/?listing=listing',578),
(7295,'Maestro','maestro',0,'/rossiya/cars/austin/maestro/all/?listing=listing',578),
(7296,'Maxi','maxi',0,'/rossiya/cars/austin/maxi/all/?listing=listing',578),
(7298,'Mini','mini',0,'/rossiya/cars/austin/mini/all/?listing=listing',578),
(7299,'Montego','montego',0,'/rossiya/cars/austin/montego/all/?listing=listing',578),
(7300,'Princess','princess',0,'/rossiya/cars/austin/princess/all/?listing=listing',578),
(7301,'A 112','a_112',0,'/rossiya/cars/autobianchi/a_112/all/?listing=listing',579),
(7302,'BD-1322','bd_1322',0,'/rossiya/cars/baltijas_dzips/bd_1322/all/?listing=listing',580),
(7303,'1989','1989',1,'/rossiya/cars/batmobile/1989/all/?listing=listing',581),
(7304,'BJ2020','bj_2020',0,'/rossiya/cars/beijing/bj_2020/all/?listing=listing',582),
(7305,'BJ212','bj_212',0,'/rossiya/cars/beijing/bj_212/all/?listing=listing',582),
(7332,'Freeclimber','freeclimber',1,'/rossiya/cars/bertone/freeclimber/all/?listing=listing',584),
(7333,'Type 3','type_3',0,'/rossiya/cars/bitter/type_3/all/?listing=listing',585),
(7394,'2000','2000',1,'/rossiya/cars/borgward/2000/all/?listing=listing',587),
(7395,'7.3S','73_s',0,'/rossiya/cars/brabus/73_s/all/?listing=listing',588),
(7396,'M V12','m_v12',0,'/rossiya/cars/brabus/m_v12/all/?listing=listing',588),
(7397,'SV12','sv12',2,'/rossiya/cars/brabus/sv12/all/?listing=listing',588),
(7398,'FRV (BS2)','frv_bs2',0,'/rossiya/cars/brilliance/frv_bs2/all/?listing=listing',589),
(7399,'H230','h230',0,'/rossiya/cars/brilliance/h230/all/?listing=listing',589),
(7400,'H530','h530',4,'/rossiya/cars/brilliance/h530/all/?listing=listing',589),
(7401,'M1 (BS6)','m1',1,'/rossiya/cars/brilliance/m1/all/?listing=listing',589),
(7402,'M2 (BS4)','m2_bs4',21,'/rossiya/cars/brilliance/m2_bs4/all/?listing=listing',589),
(7403,'M3 (BC3)','m3_bc3',1,'/rossiya/cars/brilliance/m3_bc3/all/?listing=listing',589),
(7404,'V5','v5',13,'/rossiya/cars/brilliance/v5/all/?listing=listing',589),
(7405,'Blenheim','blenheim',0,'/rossiya/cars/bristol/blenheim/all/?listing=listing',590),
(7406,'Blenheim Speedster','blenheim_speedster',0,'/rossiya/cars/bristol/blenheim_speedster/all/?listing=listing',590),
(7407,'Fighter','fighter',0,'/rossiya/cars/bristol/fighter/all/?listing=listing',590),
(7408,'Geneva','geneva',0,'/rossiya/cars/bufori/geneva/all/?listing=listing',591),
(7409,'La Joya','la_joya',0,'/rossiya/cars/bufori/la_joya/all/?listing=listing',591),
(7410,'Chiron','chiron',0,'/rossiya/cars/bugatti/chiron/all/?listing=listing',592),
(7411,'EB 110','eb_110',0,'/rossiya/cars/bugatti/eb_110/all/?listing=listing',592),
(7412,'EB 112','eb_112',0,'/rossiya/cars/bugatti/eb_112/all/?listing=listing',592),
(7413,'EB Veyron 16.4','eb_veyron',2,'/rossiya/cars/bugatti/eb_veyron/all/?listing=listing',592),
(7415,'Electra','electra',1,'/rossiya/cars/buick/electra/all/?listing=listing',593),
(7416,'LeSabre','le_sabre',1,'/rossiya/cars/buick/le_sabre/all/?listing=listing',593),
(7417,'Limited','limited',1,'/rossiya/cars/buick/limited/all/?listing=listing',593),
(7418,'Park Avenue','park_avenue',1,'/rossiya/cars/buick/park_avenue/all/?listing=listing',593),
(7419,'Reatta','reatta',1,'/rossiya/cars/buick/reatta/all/?listing=listing',593),
(7420,'Regal','regal',2,'/rossiya/cars/buick/regal/all/?listing=listing',593),
(7421,'Rendezvous','rendezvous',1,'/rossiya/cars/buick/rendezvous/all/?listing=listing',593),
(7422,'Riviera','riviera',6,'/rossiya/cars/buick/riviera/all/?listing=listing',593),
(7423,'Roadmaster','roadmaster',1,'/rossiya/cars/buick/roadmaster/all/?listing=listing',593),
(7424,'Special','special',2,'/rossiya/cars/buick/special/all/?listing=listing',593),
(7427,'Enclave','enclave',1,'/rossiya/cars/buick/enclave/all/?listing=listing',593),
(7428,'Encore','encore',0,'/rossiya/cars/buick/encore/all/?listing=listing',593),
(7429,'Envision','envision',0,'/rossiya/cars/buick/envision/all/?listing=listing',593),
(7430,'Estate Wagon','estate_wagon',0,'/rossiya/cars/buick/estate_wagon/all/?listing=listing',593),
(7431,'Excelle','excelle',0,'/rossiya/cars/buick/excelle/all/?listing=listing',593),
(7432,'GL8','gl8',0,'/rossiya/cars/buick/gl8/all/?listing=listing',593),
(7433,'LaCrosse','la_crosse',0,'/rossiya/cars/buick/la_crosse/all/?listing=listing',593),
(7436,'Lucerne','lucerne',0,'/rossiya/cars/buick/lucerne/all/?listing=listing',593),
(7438,'Rainer','rainer',0,'/rossiya/cars/buick/rainer/all/?listing=listing',593),
(7444,'Skyhawk','skyhawk',0,'/rossiya/cars/buick/skyhawk/all/?listing=listing',593),
(7445,'Skylark','skylark',0,'/rossiya/cars/buick/skylark/all/?listing=listing',593),
(7447,'Super','super',0,'/rossiya/cars/buick/super/all/?listing=listing',593),
(7448,'Terraza','terraza',0,'/rossiya/cars/buick/terraza/all/?listing=listing',593),
(7449,'Verano','verano',0,'/rossiya/cars/buick/verano/all/?listing=listing',593),
(7450,'E6','e6',0,'/rossiya/cars/byd/e6/all/?listing=listing',594),
(7451,'F0','f0',1,'/rossiya/cars/byd/f0/all/?listing=listing',594),
(7452,'F3','f3',88,'/rossiya/cars/byd/f3/all/?listing=listing',594),
(7453,'F5','f5',0,'/rossiya/cars/byd/f5/all/?listing=listing',594),
(7454,'F6','f6',1,'/rossiya/cars/byd/f6/all/?listing=listing',594),
(7455,'F8','f8',0,'/rossiya/cars/byd/f8/all/?listing=listing',594),
(7456,'Flyer','flyer',29,'/rossiya/cars/byd/flyer/all/?listing=listing',594),
(7457,'G3','g3',0,'/rossiya/cars/byd/g3/all/?listing=listing',594),
(7458,'G6','g6',0,'/rossiya/cars/byd/g6/all/?listing=listing',594),
(7459,'L3','l3',0,'/rossiya/cars/byd/l3/all/?listing=listing',594),
(7462,'BD132J (CoCo)','bd_132j',0,'/rossiya/cars/byvin/bd_132j/all/?listing=listing',595),
(7463,'BD326J (Moca)','bd_326j',0,'/rossiya/cars/byvin/bd_326j/all/?listing=listing',595),
(7464,'ATS','ats',15,'/rossiya/cars/cadillac/ats/all/?listing=listing',596),
(7465,'BLS','bls',21,'/rossiya/cars/cadillac/bls/all/?listing=listing',596),
(7466,'CTS','cts',117,'/rossiya/cars/cadillac/cts/all/?listing=listing',596),
(7467,'CTS-V','cts_v',6,'/rossiya/cars/cadillac/cts_v/all/?listing=listing',596),
(7468,'De Ville','de_ville',19,'/rossiya/cars/cadillac/de_ville/all/?listing=listing',596),
(7469,'Eldorado','eldorado',8,'/rossiya/cars/cadillac/eldorado/all/?listing=listing',596),
(7470,'Escalade','escalade',327,'/rossiya/cars/cadillac/escalade/all/?listing=listing',596),
(7471,'Fleetwood','fleetwood',9,'/rossiya/cars/cadillac/fleetwood/all/?listing=listing',596),
(7472,'SRX','srx',226,'/rossiya/cars/cadillac/srx/all/?listing=listing',596),
(7473,'STS','sts',19,'/rossiya/cars/cadillac/sts/all/?listing=listing',596),
(7474,'Seville','seville',18,'/rossiya/cars/cadillac/seville/all/?listing=listing',596),
(7475,'Allante','allante',0,'/rossiya/cars/cadillac/allante/all/?listing=listing',596),
(7477,'ATS-V','ats_v',5,'/rossiya/cars/cadillac/ats_v/all/?listing=listing',596),
(7479,'Brougham','brougham',2,'/rossiya/cars/cadillac/brougham/all/?listing=listing',596),
(7480,'Catera','catera',0,'/rossiya/cars/cadillac/catera/all/?listing=listing',596),
(7481,'CT6','ct6',0,'/rossiya/cars/cadillac/ct6/all/?listing=listing',596),
(7485,'DTS','dts',2,'/rossiya/cars/cadillac/dts/all/?listing=listing',596),
(7487,'ELR','elr',1,'/rossiya/cars/cadillac/elr/all/?listing=listing',596),
(7490,'LSE','lse',0,'/rossiya/cars/cadillac/lse/all/?listing=listing',596),
(7491,'Series 62','series_62',2,'/rossiya/cars/cadillac/series_62/all/?listing=listing',596),
(7493,'Sixty Special','sixty_special',0,'/rossiya/cars/cadillac/sixty_special/all/?listing=listing',596),
(7496,'XLR','xlr',0,'/rossiya/cars/cadillac/xlr/all/?listing=listing',596),
(7497,'XT5','xt5',2,'/rossiya/cars/cadillac/xt5/all/?listing=listing',596),
(7498,'XTS','xts',0,'/rossiya/cars/cadillac/xts/all/?listing=listing',596),
(7499,'C12','c12',0,'/rossiya/cars/callaway/c12/all/?listing=listing',597),
(7500,'FX4','fx4',0,'/rossiya/cars/carbodies/fx4/all/?listing=listing',598),
(7501,'21','c21',0,'/rossiya/cars/caterham/c21/all/?listing=listing',599),
(7502,'CSR','csr',0,'/rossiya/cars/caterham/csr/all/?listing=listing',599),
(7503,'Seven','seven',0,'/rossiya/cars/caterham/seven/all/?listing=listing',599),
(7504,'Benni','benni',1,'/rossiya/cars/changan/benni/all/?listing=listing',600),
(7505,'CM-8','cm_8',0,'/rossiya/cars/changan/cm_8/all/?listing=listing',600),
(7506,'CS35','cs35',20,'/rossiya/cars/changan/cs35/all/?listing=listing',600),
(7507,'CS75','cs75',0,'/rossiya/cars/changan/cs75/all/?listing=listing',600),
(7508,'Eado','eado',7,'/rossiya/cars/changan/eado/all/?listing=listing',600),
(7509,'Raeton','raeton',0,'/rossiya/cars/changan/raeton/all/?listing=listing',600),
(7510,'Z-Shine','z_shine',0,'/rossiya/cars/changan/z_shine/all/?listing=listing',600),
(7511,'Flying','flying',1,'/rossiya/cars/changfeng/flying/all/?listing=listing',601),
(7512,'SUV (CS6)','cs6',0,'/rossiya/cars/changfeng/cs6/all/?listing=listing',601),
(7629,'300C','300c',167,'/rossiya/cars/chrysler/300c/all/?listing=listing',604),
(7630,'300M','300m',40,'/rossiya/cars/chrysler/300m/all/?listing=listing',604),
(7631,'Cirrus','cirrus',13,'/rossiya/cars/chrysler/cirrus/all/?listing=listing',604),
(7632,'Concorde','concorde',19,'/rossiya/cars/chrysler/concorde/all/?listing=listing',604),
(7633,'Crossfire','crossfire',23,'/rossiya/cars/chrysler/crossfire/all/?listing=listing',604),
(7634,'Neon','neon',14,'/rossiya/cars/chrysler/neon/all/?listing=listing',604),
(7635,'PT Cruiser','pt_cruiser',118,'/rossiya/cars/chrysler/pt_cruiser/all/?listing=listing',604),
(7636,'Pacifica','pacifica',54,'/rossiya/cars/chrysler/pacifica/all/?listing=listing',604),
(7637,'Sebring','sebring',173,'/rossiya/cars/chrysler/sebring/all/?listing=listing',604),
(7638,'Town & Country','town_and_country',49,'/rossiya/cars/chrysler/town_and_country/all/?listing=listing',604),
(7639,'Voyager','voyager',166,'/rossiya/cars/chrysler/voyager/all/?listing=listing',604),
(7640,'180','180',0,'/rossiya/cars/chrysler/180/all/?listing=listing',604),
(7642,'300','300',0,'/rossiya/cars/chrysler/300/all/?listing=listing',604),
(7644,'300C SRT8','300_srt8',4,'/rossiya/cars/chrysler/300_srt8/all/?listing=listing',604),
(7646,'Aspen','aspen',0,'/rossiya/cars/chrysler/aspen/all/?listing=listing',604),
(7649,'Cordoba','cordoba',0,'/rossiya/cars/chrysler/cordoba/all/?listing=listing',604),
(7652,'Fifth Avenue','fifth_avenue',1,'/rossiya/cars/chrysler/fifth_avenue/all/?listing=listing',604),
(7653,'Imperial','imperial',1,'/rossiya/cars/chrysler/imperial/all/?listing=listing',604),
(7654,'Imperial Crown','imperial_crown',0,'/rossiya/cars/chrysler/imperial_crown/all/?listing=listing',604),
(7655,'Intrepid','intrepid',10,'/rossiya/cars/chrysler/intrepid/all/?listing=listing',604),
(7656,'Le Baron','le_baron',6,'/rossiya/cars/chrysler/le_baron/all/?listing=listing',604),
(7657,'LHS','lhs',7,'/rossiya/cars/chrysler/lhs/all/?listing=listing',604),
(7658,'Nassau','nassau',0,'/rossiya/cars/chrysler/nassau/all/?listing=listing',604),
(7660,'New Yorker','new_yorker',8,'/rossiya/cars/chrysler/new_yorker/all/?listing=listing',604),
(7662,'Prowler','prowler',2,'/rossiya/cars/chrysler/prowler/all/?listing=listing',604),
(7664,'Saratoga','saratoga',9,'/rossiya/cars/chrysler/saratoga/all/?listing=listing',604),
(7666,'Stratus','stratus',12,'/rossiya/cars/chrysler/stratus/all/?listing=listing',604),
(7667,'TC by Maserati','tc_by_maserati',1,'/rossiya/cars/chrysler/tc_by_maserati/all/?listing=listing',604),
(7669,'Viper','viper',0,'/rossiya/cars/chrysler/viper/all/?listing=listing',604),
(7670,'Vision','vision',4,'/rossiya/cars/chrysler/vision/all/?listing=listing',604),
(7672,'Windsor','windsor',0,'/rossiya/cars/chrysler/windsor/all/?listing=listing',604),
(7724,'V16t','v16t',0,'/rossiya/cars/cizeta/v16t/all/?listing=listing',606),
(7725,'T Rex','t_rex',0,'/rossiya/cars/coggiola/t_rex/all/?listing=listing',607),
(7727,'1310','1310',1,'/rossiya/cars/dacia/1310/all/?listing=listing',608),
(7728,'1410','1410',0,'/rossiya/cars/dacia/1410/all/?listing=listing',608),
(7733,'Nova','nova',0,'/rossiya/cars/dacia/nova/all/?listing=listing',608),
(7735,'Solenza','solenza',0,'/rossiya/cars/dacia/solenza/all/?listing=listing',608),
(7736,'City Leading','city_leading',0,'/rossiya/cars/dadi/city_leading/all/?listing=listing',609),
(7738,'Smoothing','smoothing',0,'/rossiya/cars/dadi/smoothing/all/?listing=listing',609),
(7776,'Applause','applause',3,'/rossiya/cars/daihatsu/applause/all/?listing=listing',611),
(7777,'Atrai','atrai',16,'/rossiya/cars/daihatsu/atrai/all/?listing=listing',611),
(7778,'Charade','charade',4,'/rossiya/cars/daihatsu/charade/all/?listing=listing',611),
(7779,'Copen','copen',5,'/rossiya/cars/daihatsu/copen/all/?listing=listing',611),
(7780,'Cuore','cuore',10,'/rossiya/cars/daihatsu/cuore/all/?listing=listing',611),
(7781,'Mira','mira',12,'/rossiya/cars/daihatsu/mira/all/?listing=listing',611),
(7782,'Move','move',15,'/rossiya/cars/daihatsu/move/all/?listing=listing',611),
(7783,'Sirion','sirion',19,'/rossiya/cars/daihatsu/sirion/all/?listing=listing',611),
(7784,'Storia','storia',5,'/rossiya/cars/daihatsu/storia/all/?listing=listing',611),
(7785,'Terios','terios',53,'/rossiya/cars/daihatsu/terios/all/?listing=listing',611),
(7786,'YRV','yrv',21,'/rossiya/cars/daihatsu/yrv/all/?listing=listing',611),
(7787,'Altis','altis',0,'/rossiya/cars/daihatsu/altis/all/?listing=listing',611),
(7790,'Be-go','be_go',0,'/rossiya/cars/daihatsu/be_go/all/?listing=listing',611),
(7791,'Boon','boon',2,'/rossiya/cars/daihatsu/boon/all/?listing=listing',611),
(7792,'Ceria','ceria',0,'/rossiya/cars/daihatsu/ceria/all/?listing=listing',611),
(7794,'Charmant','charmant',0,'/rossiya/cars/daihatsu/charmant/all/?listing=listing',611),
(7795,'Coo','coo',2,'/rossiya/cars/daihatsu/coo/all/?listing=listing',611),
(7798,'Delta Wagon','delta_wagon',0,'/rossiya/cars/daihatsu/delta_wagon/all/?listing=listing',611),
(7799,'Esse','esse',3,'/rossiya/cars/daihatsu/esse/all/?listing=listing',611),
(7800,'Feroza','feroza',0,'/rossiya/cars/daihatsu/feroza/all/?listing=listing',611),
(7801,'Gran Move','gran_move',0,'/rossiya/cars/daihatsu/gran_move/all/?listing=listing',611),
(7802,'Leeza','leeza',0,'/rossiya/cars/daihatsu/leeza/all/?listing=listing',611),
(7803,'Materia','materia',3,'/rossiya/cars/daihatsu/materia/all/?listing=listing',611),
(7804,'MAX','max',1,'/rossiya/cars/daihatsu/max/all/?listing=listing',611),
(7805,'Midget','midget_ii',0,'/rossiya/cars/daihatsu/midget_ii/all/?listing=listing',611),
(7807,'Mira Gino','mira_gino',1,'/rossiya/cars/daihatsu/mira_gino/all/?listing=listing',611),
(7809,'Move Latte','move_latte',0,'/rossiya/cars/daihatsu/move_latte/all/?listing=listing',611),
(7810,'Naked','naked',0,'/rossiya/cars/daihatsu/naked/all/?listing=listing',611),
(7811,'Opti','opti',0,'/rossiya/cars/daihatsu/opti/all/?listing=listing',611),
(7812,'Pyzar','pyzar',3,'/rossiya/cars/daihatsu/pyzar/all/?listing=listing',611),
(7813,'Rocky','rocky',2,'/rossiya/cars/daihatsu/rocky/all/?listing=listing',611),
(7814,'Rugger','rugger',0,'/rossiya/cars/daihatsu/rugger/all/?listing=listing',611),
(7816,'Sonica','sonica',2,'/rossiya/cars/daihatsu/sonica/all/?listing=listing',611),
(7818,'Taft','taft',0,'/rossiya/cars/daihatsu/taft/all/?listing=listing',611),
(7819,'Tanto','tanto',1,'/rossiya/cars/daihatsu/tanto/all/?listing=listing',611),
(7821,'Trevis','trevis',0,'/rossiya/cars/daihatsu/trevis/all/?listing=listing',611),
(7822,'Wildcat','wildcat',0,'/rossiya/cars/daihatsu/wildcat/all/?listing=listing',611),
(7823,'Xenia','xenia',2,'/rossiya/cars/daihatsu/xenia/all/?listing=listing',611),
(7825,'DS420','ds_420',2,'/rossiya/cars/daimler/ds_420/all/?listing=listing',612),
(7826,'Sovereign (XJ6)','sovereign',0,'/rossiya/cars/daimler/sovereign/all/?listing=listing',612),
(7827,'X300','x_300',1,'/rossiya/cars/daimler/x_300/all/?listing=listing',612),
(7828,'X308','x_308',1,'/rossiya/cars/daimler/x_308/all/?listing=listing',612),
(7829,'X350','x_350',0,'/rossiya/cars/daimler/x_350/all/?listing=listing',612),
(7830,'XJ40','xj40',0,'/rossiya/cars/daimler/xj40/all/?listing=listing',612),
(7831,'XJS','xjs',0,'/rossiya/cars/daimler/xjs/all/?listing=listing',612),
(7833,'720','720',0,'/rossiya/cars/datsun/720/all/?listing=listing',613),
(7836,'GO','go',0,'/rossiya/cars/datsun/go/all/?listing=listing',613),
(7837,'GO+','go_plus',0,'/rossiya/cars/datsun/go_plus/all/?listing=listing',613),
(7838,'mi-DO','mi_do',255,'/rossiya/cars/datsun/mi_do/all/?listing=listing',613),
(7839,'on-DO','on_do',512,'/rossiya/cars/datsun/on_do/all/?listing=listing',613),
(7843,'Violet','violet',0,'/rossiya/cars/datsun/violet/all/?listing=listing',613),
(7844,'Bigua','bigua',0,'/rossiya/cars/de_tomaso/bigua/all/?listing=listing',614),
(7845,'Guara','guara',0,'/rossiya/cars/de_tomaso/guara/all/?listing=listing',614),
(7846,'Mangusta','mangusta',0,'/rossiya/cars/de_tomaso/mangusta/all/?listing=listing',614),
(7847,'Pantera','pantera',1,'/rossiya/cars/de_tomaso/pantera/all/?listing=listing',614),
(7848,'Vallelunga','vallelunga',0,'/rossiya/cars/de_tomaso/vallelunga/all/?listing=listing',614),
(7849,'DMC-12','dmc_12',1,'/rossiya/cars/delorean/dmc_12/all/?listing=listing',615),
(7850,'Antelope','antelope',1,'/rossiya/cars/derways/antelope/all/?listing=listing',616),
(7851,'Aurora','aurora',12,'/rossiya/cars/derways/aurora/all/?listing=listing',616),
(7852,'Cowboy','cowboy',2,'/rossiya/cars/derways/cowboy/all/?listing=listing',616),
(7853,'Land Crown','land_crown',0,'/rossiya/cars/derways/land_crown/all/?listing=listing',616),
(7854,'Plutus','plutus',2,'/rossiya/cars/derways/plutus/all/?listing=listing',616),
(7855,'Saladin','saladin',0,'/rossiya/cars/derways/saladin/all/?listing=listing',616),
(7857,'Firedome','firedome',0,'/rossiya/cars/desoto/firedome/all/?listing=listing',617),
(7858,'Fireflite','fireflite',1,'/rossiya/cars/desoto/fireflite/all/?listing=listing',617),
(7859,'Caliber','caliber',176,'/rossiya/cars/dodge/caliber/all/?listing=listing',618),
(7860,'Caravan','caravan',275,'/rossiya/cars/dodge/caravan/all/?listing=listing',618),
(7862,'Charger','charger',24,'/rossiya/cars/dodge/charger/all/?listing=listing',618),
(7863,'Durango','durango',22,'/rossiya/cars/dodge/durango/all/?listing=listing',618),
(7865,'Journey','journey',44,'/rossiya/cars/dodge/journey/all/?listing=listing',618),
(7867,'Nitro','nitro',26,'/rossiya/cars/dodge/nitro/all/?listing=listing',618),
(7868,'RAM','ram',322,'/rossiya/cars/dodge/ram/all/?listing=listing',618),
(7871,'Aries','aries',0,'/rossiya/cars/dodge/aries/all/?listing=listing',618),
(7872,'Avenger','avenger',17,'/rossiya/cars/dodge/avenger/all/?listing=listing',618),
(7877,'Dakota','dakota',6,'/rossiya/cars/dodge/dakota/all/?listing=listing',618),
(7878,'Dart','dart',2,'/rossiya/cars/dodge/dart/all/?listing=listing',618),
(7879,'Daytona','daytona',0,'/rossiya/cars/dodge/daytona/all/?listing=listing',618),
(7885,'Magnum','magnum',9,'/rossiya/cars/dodge/magnum/all/?listing=listing',618),
(7886,'Mayfair','mayfair',0,'/rossiya/cars/dodge/mayfair/all/?listing=listing',618),
(7887,'Monaco','monaco',0,'/rossiya/cars/dodge/monaco/all/?listing=listing',618),
(7890,'Omni','omni',0,'/rossiya/cars/dodge/omni/all/?listing=listing',618),
(7892,'Ramcharger','ramcharger',0,'/rossiya/cars/dodge/ramcharger/all/?listing=listing',618),
(7893,'Shadow','shadow',0,'/rossiya/cars/dodge/shadow/all/?listing=listing',618),
(7894,'Spirit','spirit',2,'/rossiya/cars/dodge/spirit/all/?listing=listing',618),
(7895,'Stealth','stealth',8,'/rossiya/cars/dodge/stealth/all/?listing=listing',618),
(7898,'WC','wc',1,'/rossiya/cars/dodge/wc/all/?listing=listing',618),
(7899,'AX7','ax7',0,'/rossiya/cars/dongfeng/ax7/all/?listing=listing',619),
(7900,'H30 Cross','h30_cross',16,'/rossiya/cars/dongfeng/h30_cross/all/?listing=listing',619),
(7902,'Oting','oting',0,'/rossiya/cars/dongfeng/oting/all/?listing=listing',619),
(7903,'Rich','rich',0,'/rossiya/cars/dongfeng/rich/all/?listing=listing',619),
(7904,'S30','s30',8,'/rossiya/cars/dongfeng/s30/all/?listing=listing',619),
(7905,'Assol','assol',5,'/rossiya/cars/doninvest/assol/all/?listing=listing',620),
(7906,'Kondor','kondor',3,'/rossiya/cars/doninvest/kondor/all/?listing=listing',620),
(7908,'D8','d8',0,'/rossiya/cars/donkervoort/d8/all/?listing=listing',621),
(7909,'D8 GTO','d8_gto',0,'/rossiya/cars/donkervoort/d8_gto/all/?listing=listing',621),
(7913,'GD04B','gd04b',0,'/rossiya/cars/e_car/gd04b/all/?listing=listing',623),
(7914,'Premier','premier',0,'/rossiya/cars/eagle/premier/all/?listing=listing',624),
(7915,'Summit','summit',1,'/rossiya/cars/eagle/summit/all/?listing=listing',624),
(7916,'Talon','talon',0,'/rossiya/cars/eagle/talon/all/?listing=listing',624),
(7919,'Estrima Biro','estrima_biro',0,'/rossiya/cars/ecomotors/estrima_biro/all/?listing=listing',626),
(7920,'Besturn B50','besturn_b50',39,'/rossiya/cars/faw/besturn_b50/all/?listing=listing',627),
(7921,'Besturn B70','besturn_b70',1,'/rossiya/cars/faw/besturn_b70/all/?listing=listing',627),
(7922,'Besturn X80','x80',1,'/rossiya/cars/faw/x80/all/?listing=listing',627),
(7923,'Jinn','jinn',6,'/rossiya/cars/faw/jinn/all/?listing=listing',627),
(7924,'Oley','oley',6,'/rossiya/cars/faw/oley/all/?listing=listing',627),
(7925,'V2','v2',0,'/rossiya/cars/faw/v2/all/?listing=listing',627),
(7968,'500','500',41,'/rossiya/cars/fiat/500/all/?listing=listing',629),
(7969,'Albea','albea',197,'/rossiya/cars/fiat/albea/all/?listing=listing',629),
(7970,'Brava','brava',43,'/rossiya/cars/fiat/brava/all/?listing=listing',629),
(7971,'Bravo','bravo',38,'/rossiya/cars/fiat/bravo/all/?listing=listing',629),
(7972,'Doblo','doblo',191,'/rossiya/cars/fiat/doblo/all/?listing=listing',629),
(7973,'Linea','linea',35,'/rossiya/cars/fiat/linea/all/?listing=listing',629),
(7974,'Marea','marea',28,'/rossiya/cars/fiat/marea/all/?listing=listing',629),
(7975,'Panda','panda',35,'/rossiya/cars/fiat/panda/all/?listing=listing',629),
(7976,'Punto','punto',197,'/rossiya/cars/fiat/punto/all/?listing=listing',629),
(7977,'Scudo','scudo',25,'/rossiya/cars/fiat/scudo/all/?listing=listing',629),
(7978,'Stilo','stilo',30,'/rossiya/cars/fiat/stilo/all/?listing=listing',629),
(7979,'124','124',1,'/rossiya/cars/fiat/124/all/?listing=listing',629),
(7980,'124 Spider','124_spider',0,'/rossiya/cars/fiat/124_spider/all/?listing=listing',629),
(7981,'124 Sport Spider','124_sport_spider',0,'/rossiya/cars/fiat/124_sport_spider/all/?listing=listing',629),
(7982,'126','126',0,'/rossiya/cars/fiat/126/all/?listing=listing',629),
(7983,'127','127',0,'/rossiya/cars/fiat/127/all/?listing=listing',629),
(7984,'128','128',0,'/rossiya/cars/fiat/128/all/?listing=listing',629),
(7985,'130','130',0,'/rossiya/cars/fiat/130/all/?listing=listing',629),
(7986,'131','131',0,'/rossiya/cars/fiat/131/all/?listing=listing',629),
(7987,'132','132',0,'/rossiya/cars/fiat/132/all/?listing=listing',629),
(7988,'238','238',0,'/rossiya/cars/fiat/238/all/?listing=listing',629),
(7990,'500L','500l',0,'/rossiya/cars/fiat/500l/all/?listing=listing',629),
(7991,'500X','500x',0,'/rossiya/cars/fiat/500x/all/?listing=listing',629),
(7994,'900T','900t',0,'/rossiya/cars/fiat/900t/all/?listing=listing',629),
(7996,'Argenta','argenta',0,'/rossiya/cars/fiat/argenta/all/?listing=listing',629),
(7997,'Barchetta','barchetta',0,'/rossiya/cars/fiat/barchetta/all/?listing=listing',629),
(8000,'Cinquecento','cinquecento',0,'/rossiya/cars/fiat/cinquecento/all/?listing=listing',629),
(8002,'Croma','croma',5,'/rossiya/cars/fiat/croma/all/?listing=listing',629),
(8004,'Duna','duna',0,'/rossiya/cars/fiat/duna/all/?listing=listing',629),
(8005,'Fiorino','fiorino',3,'/rossiya/cars/fiat/fiorino/all/?listing=listing',629),
(8006,'Freemont','freemont',9,'/rossiya/cars/fiat/freemont/all/?listing=listing',629),
(8007,'Fullback','fullback',0,'/rossiya/cars/fiat/fullback/all/?listing=listing',629),
(8008,'Idea','idea',0,'/rossiya/cars/fiat/idea/all/?listing=listing',629),
(8011,'Multipla','multipla',4,'/rossiya/cars/fiat/multipla/all/?listing=listing',629),
(8012,'Palio','palio',18,'/rossiya/cars/fiat/palio/all/?listing=listing',629),
(8015,'Qubo','qubo',3,'/rossiya/cars/fiat/qubo/all/?listing=listing',629),
(8016,'Regata','regata',2,'/rossiya/cars/fiat/regata/all/?listing=listing',629),
(8017,'Ritmo','ritmo',1,'/rossiya/cars/fiat/ritmo/all/?listing=listing',629),
(8019,'Sedici','sedici',4,'/rossiya/cars/fiat/sedici/all/?listing=listing',629),
(8020,'Seicento','seicento',1,'/rossiya/cars/fiat/seicento/all/?listing=listing',629),
(8021,'Siena','siena',0,'/rossiya/cars/fiat/siena/all/?listing=listing',629),
(8024,'Tempra','tempra',14,'/rossiya/cars/fiat/tempra/all/?listing=listing',629),
(8025,'Tipo','tipo',14,'/rossiya/cars/fiat/tipo/all/?listing=listing',629),
(8026,'Ulysse','ulysse',2,'/rossiya/cars/fiat/ulysse/all/?listing=listing',629),
(8027,'Uno','uno',3,'/rossiya/cars/fiat/uno/all/?listing=listing',629),
(8028,'X 1/9','x_1_9',0,'/rossiya/cars/fiat/x_1_9/all/?listing=listing',629),
(8029,'Karma','karma',1,'/rossiya/cars/fisker/karma/all/?listing=listing',630),
(8121,'Midi','midi',0,'/rossiya/cars/foton/midi/all/?listing=listing',632),
(8122,'Sauvana','sauvana',0,'/rossiya/cars/foton/sauvana/all/?listing=listing',632),
(8123,'Tunland','tunland',6,'/rossiya/cars/foton/tunland/all/?listing=listing',632),
(8124,'125p','125_p',0,'/rossiya/cars/fso/125_p/all/?listing=listing',633),
(8125,'126p','126p',0,'/rossiya/cars/fso/126p/all/?listing=listing',633),
(8126,'127p','127p',0,'/rossiya/cars/fso/127p/all/?listing=listing',633),
(8127,'132p','132p',0,'/rossiya/cars/fso/132p/all/?listing=listing',633),
(8128,'Polonez','polonez',0,'/rossiya/cars/fso/polonez/all/?listing=listing',633),
(8129,'6500 (Land King)','land_king',0,'/rossiya/cars/fuqi/land_king/all/?listing=listing',634),
(8130,'CK (Otaka)','ck',48,'/rossiya/cars/geely/ck/all/?listing=listing',635),
(8131,'Emgrand 7','emgrand_7',1,'/rossiya/cars/geely/emgrand_7/all/?listing=listing',635),
(8132,'Emgrand EC7','emgrand',219,'/rossiya/cars/geely/emgrand/all/?listing=listing',635),
(8133,'Emgrand X7','emgrand_x7',54,'/rossiya/cars/geely/emgrand_x7/all/?listing=listing',635),
(8134,'FC (Vision)','vision',23,'/rossiya/cars/geely/vision/all/?listing=listing',635),
(8135,'GC6','gc6',21,'/rossiya/cars/geely/gc6/all/?listing=listing',635),
(8136,'Haoqing','haoqing',0,'/rossiya/cars/geely/haoqing/all/?listing=listing',635),
(8137,'LC (Panda)','lc',0,'/rossiya/cars/geely/lc/all/?listing=listing',635),
(8138,'MK','mk',220,'/rossiya/cars/geely/mk/all/?listing=listing',635),
(8139,'MK Cross','mk_cross',141,'/rossiya/cars/geely/mk_cross/all/?listing=listing',635),
(8140,'SC7','sc7',1,'/rossiya/cars/geely/sc7/all/?listing=listing',635),
(8141,'Beauty Leopard','beauty_leopard',0,'/rossiya/cars/geely/beauty_leopard/all/?listing=listing',635),
(8145,'Emgrand EC8','emgrand_8',0,'/rossiya/cars/geely/emgrand_8/all/?listing=listing',635),
(8149,'GC9','gc9',0,'/rossiya/cars/geely/gc9/all/?listing=listing',635),
(8152,'LC (Panda) Cross','lc_cross',0,'/rossiya/cars/geely/lc_cross/all/?listing=listing',635),
(8155,'MR','mr',0,'/rossiya/cars/geely/mr/all/?listing=listing',635),
(8159,'Spectrum','spectrum',0,'/rossiya/cars/geo/spectrum/all/?listing=listing',636),
(8160,'Storm','storm',1,'/rossiya/cars/geo/storm/all/?listing=listing',636),
(8162,'Acadia','acadia',3,'/rossiya/cars/gmc/acadia/all/?listing=listing',637),
(8163,'Canyon','canyon',5,'/rossiya/cars/gmc/canyon/all/?listing=listing',637),
(8164,'Envoy','envoy',7,'/rossiya/cars/gmc/envoy/all/?listing=listing',637),
(8165,'Jimmy','jimmy',11,'/rossiya/cars/gmc/jimmy/all/?listing=listing',637),
(8166,'Savana','savana',30,'/rossiya/cars/gmc/savana/all/?listing=listing',637),
(8169,'Terrain','terrain',8,'/rossiya/cars/gmc/terrain/all/?listing=listing',637),
(8170,'Typhoon','typhoon',1,'/rossiya/cars/gmc/typhoon/all/?listing=listing',637),
(8171,'Vandura','vandura',1,'/rossiya/cars/gmc/vandura/all/?listing=listing',637),
(8172,'Yukon','yukon',29,'/rossiya/cars/gmc/yukon/all/?listing=listing',637),
(8180,'Sonoma','sonoma',0,'/rossiya/cars/gmc/sonoma/all/?listing=listing',637),
(8182,'Syclone','syclone',0,'/rossiya/cars/gmc/syclone/all/?listing=listing',637),
(8187,'Troy','troy',0,'/rossiya/cars/gonow/troy/all/?listing=listing',638),
(8189,'Deer','deer',54,'/rossiya/cars/great_wall/deer/all/?listing=listing',640),
(8190,'Hover','hover_2005',192,'/rossiya/cars/great_wall/hover_2005/all/?listing=listing',640),
(8191,'Hover H3','hoverh3',260,'/rossiya/cars/great_wall/hoverh3/all/?listing=listing',640),
(8192,'Hover H5','hoverh5',252,'/rossiya/cars/great_wall/hoverh5/all/?listing=listing',640),
(8193,'Hover H6','hoverh6',19,'/rossiya/cars/great_wall/hoverh6/all/?listing=listing',640),
(8194,'Hover M2','hover_m2',6,'/rossiya/cars/great_wall/hover_m2/all/?listing=listing',640),
(8195,'Hover M4','hover_m4',23,'/rossiya/cars/great_wall/hover_m4/all/?listing=listing',640),
(8196,'Peri','peri',6,'/rossiya/cars/great_wall/peri/all/?listing=listing',640),
(8197,'Safe','safe',115,'/rossiya/cars/great_wall/safe/all/?listing=listing',640),
(8198,'Sailor','sailor',19,'/rossiya/cars/great_wall/sailor/all/?listing=listing',640),
(8199,'Wingle','wingle_up',49,'/rossiya/cars/great_wall/wingle_up/all/?listing=listing',640),
(8200,'Coolbear','coolbear',2,'/rossiya/cars/great_wall/coolbear/all/?listing=listing',640),
(8201,'Cowry (V80)','cowry',0,'/rossiya/cars/great_wall/cowry/all/?listing=listing',640),
(8203,'Florid','florid',3,'/rossiya/cars/great_wall/florid/all/?listing=listing',640),
(8208,'Hover M1 (Peri 4x4)','hover_m1',0,'/rossiya/cars/great_wall/hover_m1/all/?listing=listing',640),
(8211,'Pegasus','pegasus',0,'/rossiya/cars/great_wall/pegasus/all/?listing=listing',640),
(8215,'Sing RUV','sing',1,'/rossiya/cars/great_wall/sing/all/?listing=listing',640),
(8216,'Socool','socool',0,'/rossiya/cars/great_wall/socool/all/?listing=listing',640),
(8217,'Voleex C10 (Phenom)','voleexc10',0,'/rossiya/cars/great_wall/voleexc10/all/?listing=listing',640),
(8218,'Voleex C30','voleexc30',1,'/rossiya/cars/great_wall/voleexc30/all/?listing=listing',640),
(8220,'Brio','brio',19,'/rossiya/cars/hafei/brio/all/?listing=listing',641),
(8221,'Princip','princip',3,'/rossiya/cars/hafei/princip/all/?listing=listing',641),
(8222,'Saibao','saibao',0,'/rossiya/cars/hafei/saibao/all/?listing=listing',641),
(8224,'Simbo','simbo',5,'/rossiya/cars/hafei/simbo/all/?listing=listing',641),
(8226,'7','7',10,'/rossiya/cars/haima/7/all/?listing=listing',642),
(8229,'H2','h2',10,'/rossiya/cars/haval/h2/all/?listing=listing',643),
(8230,'H6','h6',10,'/rossiya/cars/haval/h6/all/?listing=listing',643),
(8231,'H8','h8',4,'/rossiya/cars/haval/h8/all/?listing=listing',643),
(8232,'H9','h9',2,'/rossiya/cars/haval/h9/all/?listing=listing',643),
(8233,'Boliger','boliger',5,'/rossiya/cars/hawtai/boliger/all/?listing=listing',644),
(8235,'Contessa','contessa',0,'/rossiya/cars/hindustan/contessa/all/?listing=listing',645),
(8240,'Jackaroo','jackaroo',0,'/rossiya/cars/holden/jackaroo/all/?listing=listing',646),
(8241,'Monaro','monaro',0,'/rossiya/cars/holden/monaro/all/?listing=listing',646),
(8243,'Statesman','statesman',0,'/rossiya/cars/holden/statesman/all/?listing=listing',646),
(8244,'UTE','ute',0,'/rossiya/cars/holden/ute/all/?listing=listing',646),
(8247,'Apollo','apollo',0,'/rossiya/cars/holden/apollo/all/?listing=listing',646),
(8249,'Barina','barina',0,'/rossiya/cars/holden/barina/all/?listing=listing',646),
(8250,'Calais','calais',0,'/rossiya/cars/holden/calais/all/?listing=listing',646),
(8338,'Landscape','landscape',2,'/rossiya/cars/huanghai/landscape/all/?listing=listing',648),
(8340,'Deluxe Eight','deluxe_eight',1,'/rossiya/cars/hudson/deluxe_eight/all/?listing=listing',649),
(8341,'H1','h1',14,'/rossiya/cars/hummer/h1/all/?listing=listing',650),
(8343,'H3','h3',72,'/rossiya/cars/hummer/h3/all/?listing=listing',650),
(8433,'Elba','elba',0,'/rossiya/cars/innocenti/elba/all/?listing=listing',653),
(8434,'Mille','mille',0,'/rossiya/cars/innocenti/mille/all/?listing=listing',653),
(8437,'Paykan','paykan',0,'/rossiya/cars/iran_khodro/paykan/all/?listing=listing',655),
(8438,'Samand','samand',93,'/rossiya/cars/iran_khodro/samand/all/?listing=listing',655),
(8439,'Soren','soren',0,'/rossiya/cars/iran_khodro/soren/all/?listing=listing',655),
(8440,'Commendatore 112i','commendatore_112i',0,'/rossiya/cars/isdera/commendatore_112i/all/?listing=listing',656),
(8441,'Imperator 108i','imperator_108i',0,'/rossiya/cars/isdera/imperator_108i/all/?listing=listing',656),
(8442,'Spyder','spyder',0,'/rossiya/cars/isdera/spyder/all/?listing=listing',656),
(8443,'Amigo','amigo',1,'/rossiya/cars/isuzu/amigo/all/?listing=listing',657),
(8444,'Axiom','axiom',7,'/rossiya/cars/isuzu/axiom/all/?listing=listing',657),
(8445,'Bighorn','bighorn',10,'/rossiya/cars/isuzu/bighorn/all/?listing=listing',657),
(8446,'D-Max','d_max',4,'/rossiya/cars/isuzu/d_max/all/?listing=listing',657),
(8447,'Gemini','gemini',5,'/rossiya/cars/isuzu/gemini/all/?listing=listing',657),
(8448,'MU','mu',2,'/rossiya/cars/isuzu/mu/all/?listing=listing',657),
(8450,'TF (Pickup)','tf',1,'/rossiya/cars/isuzu/tf/all/?listing=listing',657),
(8451,'Trooper','trooper',27,'/rossiya/cars/isuzu/trooper/all/?listing=listing',657),
(8452,'VehiCross','vehi_cross',8,'/rossiya/cars/isuzu/vehi_cross/all/?listing=listing',657),
(8453,'Wizard','wizard',5,'/rossiya/cars/isuzu/wizard/all/?listing=listing',657),
(8455,'Ascender','ascender',0,'/rossiya/cars/isuzu/ascender/all/?listing=listing',657),
(8456,'Aska','aska',0,'/rossiya/cars/isuzu/aska/all/?listing=listing',657),
(8461,'Hombre','hombre',0,'/rossiya/cars/isuzu/hombre/all/?listing=listing',657),
(8462,'Impulse','impulse',0,'/rossiya/cars/isuzu/impulse/all/?listing=listing',657),
(8463,'KB','kb',0,'/rossiya/cars/isuzu/kb/all/?listing=listing',657),
(8465,'MU-7','mu-7',0,'/rossiya/cars/isuzu/mu-7/all/?listing=listing',657),
(8466,'MU-X','mu_x',0,'/rossiya/cars/isuzu/mu_x/all/?listing=listing',657),
(8467,'Piazza','piazza',0,'/rossiya/cars/isuzu/piazza/all/?listing=listing',657),
(8469,'Stylus','stylus',0,'/rossiya/cars/isuzu/stylus/all/?listing=listing',657),
(8474,'J2 (Yueyue)','j2',0,'/rossiya/cars/jac/j2/all/?listing=listing',658),
(8475,'J3 (Tongyue,Tojoy)','j3',0,'/rossiya/cars/jac/j3/all/?listing=listing',658),
(8476,'J4 (Heyue A30)','j4',0,'/rossiya/cars/jac/j4/all/?listing=listing',658),
(8477,'J5 (Heyue)','j5',5,'/rossiya/cars/jac/j5/all/?listing=listing',658),
(8478,'J6 (Heyue RS)','j6',0,'/rossiya/cars/jac/j6/all/?listing=listing',658),
(8479,'J7 (Binyue)','j7',0,'/rossiya/cars/jac/j7/all/?listing=listing',658),
(8480,'M1 (Refine)','refine',3,'/rossiya/cars/jac/refine/all/?listing=listing',658),
(8482,'S1 (Rein)','s1_rein',1,'/rossiya/cars/jac/s1_rein/all/?listing=listing',658),
(8483,'S3','s_3',0,'/rossiya/cars/jac/s_3/all/?listing=listing',658),
(8484,'S5 (Eagle)','s5',17,'/rossiya/cars/jac/s5/all/?listing=listing',658),
(8485,'F-Pace','f_pace',126,'/rossiya/cars/jaguar/f_pace/all/?listing=listing',659),
(8486,'F-Type','f_type',31,'/rossiya/cars/jaguar/f_type/all/?listing=listing',659),
(8487,'S-Type','s_type',70,'/rossiya/cars/jaguar/s_type/all/?listing=listing',659),
(8488,'X-Type','x_type',94,'/rossiya/cars/jaguar/x_type/all/?listing=listing',659),
(8489,'XE','xe',87,'/rossiya/cars/jaguar/xe/all/?listing=listing',659),
(8490,'XF','xf',363,'/rossiya/cars/jaguar/xf/all/?listing=listing',659),
(8491,'XFR','xfr',14,'/rossiya/cars/jaguar/xfr/all/?listing=listing',659),
(8492,'XJ','xj',192,'/rossiya/cars/jaguar/xj/all/?listing=listing',659),
(8493,'XJR','xjr',10,'/rossiya/cars/jaguar/xjr/all/?listing=listing',659),
(8494,'XK','xk',17,'/rossiya/cars/jaguar/xk/all/?listing=listing',659),
(8495,'XKR','xkr',23,'/rossiya/cars/jaguar/xkr/all/?listing=listing',659),
(8496,'E-type','e_type',1,'/rossiya/cars/jaguar/e_type/all/?listing=listing',659),
(8499,'F-Type SVR','f_type_svr',0,'/rossiya/cars/jaguar/f_type_svr/all/?listing=listing',659),
(8500,'Mark 2','mark_2',0,'/rossiya/cars/jaguar/mark_2/all/?listing=listing',659),
(8507,'XJ220','xj220',0,'/rossiya/cars/jaguar/xj220/all/?listing=listing',659),
(8512,'Cherokee','cherokee',206,'/rossiya/cars/jeep/cherokee/all/?listing=listing',660),
(8513,'CJ','cj',2,'/rossiya/cars/jeep/cj/all/?listing=listing',660),
(8514,'Commander','commander',18,'/rossiya/cars/jeep/commander/all/?listing=listing',660),
(8515,'Compass','compass',61,'/rossiya/cars/jeep/compass/all/?listing=listing',660),
(8516,'Grand Cherokee','grand_cherokee',768,'/rossiya/cars/jeep/grand_cherokee/all/?listing=listing',660),
(8517,'Grand Cherokee SRT8','grand_cherokee_srt8',70,'/rossiya/cars/jeep/grand_cherokee_srt8/all/?listing=listing',660),
(8518,'Grand Wagoneer','grand_wagoneer',0,'/rossiya/cars/jeep/grand_wagoneer/all/?listing=listing',660),
(8519,'Liberty (North America)','libety_na',23,'/rossiya/cars/jeep/libety_na/all/?listing=listing',660),
(8520,'Liberty (Patriot)','patriot',33,'/rossiya/cars/jeep/patriot/all/?listing=listing',660),
(8521,'Renegade','renegade',53,'/rossiya/cars/jeep/renegade/all/?listing=listing',660),
(8522,'Wrangler','wrangler',137,'/rossiya/cars/jeep/wrangler/all/?listing=listing',660),
(8523,'Interceptor','interceptor',0,'/rossiya/cars/jensen/interceptor/all/?listing=listing',661),
(8524,'S-V8','s_v8',0,'/rossiya/cars/jensen/s_v8/all/?listing=listing',661),
(8525,'Baodian','baodian',14,'/rossiya/cars/jmc/baodian/all/?listing=listing',662),
(8572,'Agera','agera',0,'/rossiya/cars/koenigsegg/agera/all/?listing=listing',664),
(8573,'CC8S','cc8s',0,'/rossiya/cars/koenigsegg/cc8s/all/?listing=listing',664),
(8574,'CCR','ccr',0,'/rossiya/cars/koenigsegg/ccr/all/?listing=listing',664),
(8575,'CCX','ccx',0,'/rossiya/cars/koenigsegg/ccx/all/?listing=listing',664),
(8576,'One:1','one_1',0,'/rossiya/cars/koenigsegg/one_1/all/?listing=listing',664),
(8577,'Regera','regera',0,'/rossiya/cars/koenigsegg/regera/all/?listing=listing',664),
(8578,'X-Bow','xbow',0,'/rossiya/cars/ktm/xbow/all/?listing=listing',665),
(8610,'Dedra','dedra',7,'/rossiya/cars/lancia/dedra/all/?listing=listing',667),
(8611,'Delta','delta',8,'/rossiya/cars/lancia/delta/all/?listing=listing',667),
(8612,'Kappa','kappa',7,'/rossiya/cars/lancia/kappa/all/?listing=listing',667),
(8613,'Lybra','lybra',4,'/rossiya/cars/lancia/lybra/all/?listing=listing',667),
(8614,'Musa','musa',1,'/rossiya/cars/lancia/musa/all/?listing=listing',667),
(8615,'Phedra','phedra',1,'/rossiya/cars/lancia/phedra/all/?listing=listing',667),
(8616,'Prisma','prisma',0,'/rossiya/cars/lancia/prisma/all/?listing=listing',667),
(8617,'Rally 037','rally_037',0,'/rossiya/cars/lancia/rally_037/all/?listing=listing',667),
(8618,'Thema','thema',4,'/rossiya/cars/lancia/thema/all/?listing=listing',667),
(8619,'Thesis','thesis',5,'/rossiya/cars/lancia/thesis/all/?listing=listing',667),
(8620,'Ypsilon','ypsilon',2,'/rossiya/cars/lancia/ypsilon/all/?listing=listing',667),
(8622,'Appia','appia',0,'/rossiya/cars/lancia/appia/all/?listing=listing',667),
(8623,'Aurelia','aurelia',0,'/rossiya/cars/lancia/aurelia/all/?listing=listing',667),
(8624,'Beta','beta',0,'/rossiya/cars/lancia/beta/all/?listing=listing',667),
(8627,'Flaminia','flaminia',0,'/rossiya/cars/lancia/flaminia/all/?listing=listing',667),
(8628,'Flavia','flavia',0,'/rossiya/cars/lancia/flavia/all/?listing=listing',667),
(8629,'Fulvia','fulvia',0,'/rossiya/cars/lancia/fulvia/all/?listing=listing',667),
(8630,'Gamma','gamma',0,'/rossiya/cars/lancia/gamma/all/?listing=listing',667),
(8631,'Hyena','hyuena',0,'/rossiya/cars/lancia/hyuena/all/?listing=listing',667),
(8633,'Lambda','lambda',0,'/rossiya/cars/lancia/lambda/all/?listing=listing',667),
(8635,'Monte Carlo','montecarlo',0,'/rossiya/cars/lancia/montecarlo/all/?listing=listing',667),
(8640,'Stratos','stratos',0,'/rossiya/cars/lancia/stratos/all/?listing=listing',667),
(8643,'Trevi','trevi',0,'/rossiya/cars/lancia/trevi/all/?listing=listing',667),
(8645,'Y10','y10',0,'/rossiya/cars/lancia/y10/all/?listing=listing',667),
(8647,'Zeta','zeta',0,'/rossiya/cars/lancia/zeta/all/?listing=listing',667),
(8658,'Fashion (CV9)','cv9',0,'/rossiya/cars/landwind/cv9/all/?listing=listing',669),
(8659,'Forward','forward',0,'/rossiya/cars/landwind/forward/all/?listing=listing',669),
(8662,'Х9','lw_x9',0,'/rossiya/cars/landwind/lw_x9/all/?listing=listing',669),
(8692,'Breez (520)','breez',146,'/rossiya/cars/lifan/breez/all/?listing=listing',672),
(8693,'Cebrium (720)','cebrium',31,'/rossiya/cars/lifan/cebrium/all/?listing=listing',672),
(8694,'Celliya (530)','celliya',5,'/rossiya/cars/lifan/celliya/all/?listing=listing',672),
(8695,'Smily','smily',189,'/rossiya/cars/lifan/smily/all/?listing=listing',672),
(8696,'Solano','solano',286,'/rossiya/cars/lifan/solano/all/?listing=listing',672),
(8697,'X50','x50',141,'/rossiya/cars/lifan/x50/all/?listing=listing',672),
(8698,'X60','x60',324,'/rossiya/cars/lifan/x60/all/?listing=listing',672),
(8699,'Aviator','aviator',4,'/rossiya/cars/lincoln/aviator/all/?listing=listing',673),
(8702,'MKC','mkc',4,'/rossiya/cars/lincoln/mkc/all/?listing=listing',673),
(8703,'MKS','mks',1,'/rossiya/cars/lincoln/mks/all/?listing=listing',673),
(8704,'MKX','mkx',6,'/rossiya/cars/lincoln/mkx/all/?listing=listing',673),
(8705,'MKZ','mkz',7,'/rossiya/cars/lincoln/mkz/all/?listing=listing',673),
(8706,'Mark VII','mark_vii',2,'/rossiya/cars/lincoln/mark_vii/all/?listing=listing',673),
(8707,'Mark VIII','mark_viii',3,'/rossiya/cars/lincoln/mark_viii/all/?listing=listing',673),
(8708,'Navigator','navigator',60,'/rossiya/cars/lincoln/navigator/all/?listing=listing',673),
(8709,'Town Car','town_car',98,'/rossiya/cars/lincoln/town_car/all/?listing=listing',673),
(8714,'Mark LT','mark_lt',1,'/rossiya/cars/lincoln/mark_lt/all/?listing=listing',673),
(8719,'MKT','mkt',0,'/rossiya/cars/lincoln/mkt/all/?listing=listing',673),
(8723,'Premiere','premiere',0,'/rossiya/cars/lincoln/premiere/all/?listing=listing',673),
(8725,'340R','340r',0,'/rossiya/cars/lotus/340r/all/?listing=listing',674),
(8726,'Eclat','eclat',0,'/rossiya/cars/lotus/eclat/all/?listing=listing',674),
(8728,'Elise','elise',1,'/rossiya/cars/lotus/elise/all/?listing=listing',674),
(8729,'Elite','elite',0,'/rossiya/cars/lotus/elite/all/?listing=listing',674),
(8730,'Esprit','esprit',0,'/rossiya/cars/lotus/esprit/all/?listing=listing',674),
(8731,'Europa','europa',0,'/rossiya/cars/lotus/europa/all/?listing=listing',674),
(8732,'Europa S','europa-s',0,'/rossiya/cars/lotus/europa-s/all/?listing=listing',674),
(8733,'Evora','evora',1,'/rossiya/cars/lotus/evora/all/?listing=listing',674),
(8735,'Exige','exige',3,'/rossiya/cars/lotus/exige/all/?listing=listing',674),
(8736,'TX','tx',1,'/rossiya/cars/lti/tx/all/?listing=listing',675),
(8737,'Luxgen5','luxgen5',0,'/rossiya/cars/luxgen/luxgen5/all/?listing=listing',676),
(8738,'Luxgen7 MPV','7_mpv',1,'/rossiya/cars/luxgen/7_mpv/all/?listing=listing',676),
(8739,'Luxgen7 SUV','7_suv',13,'/rossiya/cars/luxgen/7_suv/all/?listing=listing',676),
(8740,'U6 Turbo','u6_turbo',0,'/rossiya/cars/luxgen/u6_turbo/all/?listing=listing',676),
(8741,'U7 Turbo','u7_turbo',3,'/rossiya/cars/luxgen/u7_turbo/all/?listing=listing',676),
(8743,'Bolero','bolero',0,'/rossiya/cars/mahindra/bolero/all/?listing=listing',677),
(8744,'CJ-3','cj_3',0,'/rossiya/cars/mahindra/cj_3/all/?listing=listing',677),
(8747,'Marshal','marshal',0,'/rossiya/cars/mahindra/marshal/all/?listing=listing',677),
(8748,'MM','mm',0,'/rossiya/cars/mahindra/mm/all/?listing=listing',677),
(8749,'NC 640 DP','nc_640_dp',0,'/rossiya/cars/mahindra/nc_640_dp/all/?listing=listing',677),
(8751,'Verito','verito',0,'/rossiya/cars/mahindra/verito/all/?listing=listing',677),
(8753,'Xylo','xylo',0,'/rossiya/cars/mahindra/xylo/all/?listing=listing',677),
(8754,'GTS','gts',1,'/rossiya/cars/marcos/gts/all/?listing=listing',678),
(8755,'LM 400','lm400',0,'/rossiya/cars/marcos/lm400/all/?listing=listing',678),
(8756,'LM 500','lm500',0,'/rossiya/cars/marcos/lm500/all/?listing=listing',678),
(8757,'Mantis','mantis_gt',0,'/rossiya/cars/marcos/mantis_gt/all/?listing=listing',678),
(8758,'Marcasite','marcasite',0,'/rossiya/cars/marcos/marcasite/all/?listing=listing',678),
(8759,'5EXi','5exi',0,'/rossiya/cars/marlin/5exi/all/?listing=listing',679),
(8760,'Sportster','sportster',0,'/rossiya/cars/marlin/sportster/all/?listing=listing',679),
(8761,'B1','b1',0,'/rossiya/cars/marussia/b1/all/?listing=listing',680),
(8762,'B2','b2',0,'/rossiya/cars/marussia/b2/all/?listing=listing',680),
(8764,'800','800',0,'/rossiya/cars/maruti/800/all/?listing=listing',681),
(8767,'Esteem','esteem',0,'/rossiya/cars/maruti/esteem/all/?listing=listing',681),
(8768,'Gypsy','gypsy',0,'/rossiya/cars/maruti/gypsy/all/?listing=listing',681),
(8772,'Zen','zen',0,'/rossiya/cars/maruti/zen/all/?listing=listing',681),
(8773,'3200 GT','3200gt',6,'/rossiya/cars/maserati/3200gt/all/?listing=listing',682),
(8774,'4200 GT','4200_gt',7,'/rossiya/cars/maserati/4200_gt/all/?listing=listing',682),
(8775,'Ghibli','ghibli',16,'/rossiya/cars/maserati/ghibli/all/?listing=listing',682),
(8776,'GranTurismo','gran_turismo',23,'/rossiya/cars/maserati/gran_turismo/all/?listing=listing',682),
(8777,'Karif','karif',0,'/rossiya/cars/maserati/karif/all/?listing=listing',682),
(8778,'Khamsin','khamsin',0,'/rossiya/cars/maserati/khamsin/all/?listing=listing',682),
(8779,'Kyalami','kyalami',0,'/rossiya/cars/maserati/kyalami/all/?listing=listing',682),
(8780,'Levante','levante',0,'/rossiya/cars/maserati/levante/all/?listing=listing',682),
(8781,'MC12','mc12',0,'/rossiya/cars/maserati/mc12/all/?listing=listing',682),
(8782,'Quattroporte','quattroporte',35,'/rossiya/cars/maserati/quattroporte/all/?listing=listing',682),
(8783,'Shamal','shamal',0,'/rossiya/cars/maserati/shamal/all/?listing=listing',682),
(8784,'228','228',0,'/rossiya/cars/maserati/228/all/?listing=listing',682),
(8786,'420','420',0,'/rossiya/cars/maserati/420/all/?listing=listing',682),
(8788,'Barchetta Stradale','barchetta_stradale',0,'/rossiya/cars/maserati/barchetta_stradale/all/?listing=listing',682),
(8789,'Biturbo','biturbo',0,'/rossiya/cars/maserati/biturbo/all/?listing=listing',682),
(8791,'Chubasco','chubasco',0,'/rossiya/cars/maserati/chubasco/all/?listing=listing',682),
(8794,'Indy','indy',0,'/rossiya/cars/maserati/indy/all/?listing=listing',682),
(8800,'Merak','merak',0,'/rossiya/cars/maserati/merak/all/?listing=listing',682),
(8801,'Mexico','mexico',0,'/rossiya/cars/maserati/mexico/all/?listing=listing',682),
(8803,'Royale','royale',0,'/rossiya/cars/maserati/royale/all/?listing=listing',682),
(8805,'57','57',13,'/rossiya/cars/maybach/57/all/?listing=listing',683),
(8806,'62','62',6,'/rossiya/cars/maybach/62/all/?listing=listing',683);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
